/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
  // Define changes to default configuration here. For example:
  // config.language = 'fr';
  // config.uiColor = '#AADC6E';
  config.removeDialogTabs = "link:advanced";
  config.entities = true;
  config.allowedContent = true;
  config.extraPlugins = 'filebrowser';
  config.extraPlugins = 'popup';
  config.extraPlugins = 'filetools';
  config.extraPlugins = 'richcombo,placeholder_select';
  // config.extraAllowedContent = "span;ul;li;table;td;style;*[id];*(*);*{*}";
  config.filebrowserBrowseUrl = 'http://localhost/blogProject/kcfinder/browse.php?type=files';
  config.filebrowserImageBrowseUrl = 'http://localhost/blogProject/kcfinder/browse.php?type=images';
  config.filebrowserFlashBrowseUrl = 'http://localhost/blogProject/kcfinder/browse.php?type=flash';
  config.filebrowserUploadUrl = 'http://localhost/blogProject/kcfinder/upload.php?type=files';
  config.filebrowserImageUploadUrl = 'http://localhost/blogProject/kcfinder/upload.php?type=images';
  config.filebrowserFlashUploadUrl = 'http://localhost/blogProject/kcfinder/upload.php?type=flash';

  // config.toolbar = [
  //     ['Bold', 'Italic', 'Underline'],
  //     ['placeholder_select']
  // ];

  config.placeholder_select = {
      // placeholders: ['FirstName', 'LastName', 'EmailAddress'],
      placeholders: ['id_order', 'nama_pemesan', 'no_whatsapp','email', 'kota', 'datetime_order', 'total_order', 'kode_unik', 'status_pembayaran', 'status_order', 'diskon', 'nama_produk', 'qty'],
      format: '[[%placeholder%]]',
  }
};

CKEDITOR.dtd.$removeEmpty.span = 0;
