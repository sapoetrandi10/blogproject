<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
  /**
  * Ipaymu payment gateway
  *
  * This is a wrapper class/library by Andi Saputra
  *
  * @package    CodeIgniter
  * @subpackage libraries
  * @category   library
  * @version    2.0
  * @author     Andi Saputra <sapoetrandi10@gmail.com>
  */
  class Ipaymu {

    public function test(){
        return "Library OK";
    }

    // public function requestPatternIpaymu(){
    // // SAMPLE HIT API iPaymu v2 PHP //

    //     $va           = 'yout_va'; //get on iPaymu dashboard
    //     $secret       = 'yout_api_key'; //get on iPaymu dashboard

    //     $url          = 'https://my.ipaymu.com/api/v2/payment'; //url
    //     $method       = 'POST'; //method

    //     //Request Body//
    //     $body['product']    = array('headset', 'softcase');
    //     $body['qty']        = array('1', '3');
    //     $body['price']      = array('100000', '20000');
    //     $body['returnUrl']  = 'https://mywebsite.com/thankyou';
    //     $body['cancelUrl']  = 'https://mywebsite.com/cancel';
    //     $body['notifyUrl']  = 'https://mywebsite.com/notify';
    //     //End Request Body//

    //     //Generate Signature
    //     // *Don't change this
    //     $jsonBody     = json_encode($body, JSON_UNESCAPED_SLASHES);
    //     $requestBody  = strtolower(hash('sha256', $jsonBody));
    //     $stringToSign = strtoupper($method) . ':' . $va . ':' . $requestBody . ':' . $secret;
    //     $signature    = hash_hmac('sha256', $stringToSign, $secret);
    //     $timestamp    = Date('YmdHis');
    //     //End Generate Signature


    //     $ch = curl_init($url);

    //     $headers = array(
    //         'Accept: application/json',
    //         'Content-Type: application/json',
    //         'va: ' . $va,
    //         'signature: ' . $signature,
    //         'timestamp: ' . $timestamp
    //     );

    //     curl_setopt($ch, CURLOPT_HEADER, false);
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    //     curl_setopt($ch, CURLOPT_POST, count($body));
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);

    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    //     $err = curl_error($ch);
    //     $ret = curl_exec($ch);
    //     curl_close($ch);
    //     if($err) {
    //         echo $err;
    //     } else {

    //         //Response
    //         $ret = json_decode($ret);
    //         if($ret->Status == 200) {
    //             $sessionId  = $ret->Data->SessionID;
    //             $url        =  $ret->Data->Url;
    //             header('Location:' . $url);
    //         } else {
    //             echo $ret;
    //         }
    //         //End Response
    //     }
    // }

    public function checkBalance($va, $secret, $type){

        // checker type
        if($type == 'production'){
            $url          = 'https://my.ipaymu.com/api/v2/balance'; //url
            $method       = 'POST'; //method
        }else{
            $url          = 'https://sandbox.ipaymu.com/api/v2/balance'; //url sandbox
            $method       = 'POST'; //method
        }

        //Request Body//
        $body['account']    = $va;
        //End Request Body//

        //Generate Signature
        // *Don't change this
        $jsonBody     = json_encode($body, JSON_UNESCAPED_SLASHES);
        $requestBody  = strtolower(hash('sha256', $jsonBody));
        $stringToSign = strtoupper($method) . ':' . $va . ':' . $requestBody . ':' . $secret;
        $signature    = hash_hmac('sha256', $stringToSign, $secret);
        $timestamp    = Date('YmdHis');
        //End Generate Signature


        $ch = curl_init($url);

        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'va: ' . $va,
            'signature: ' . $signature,
            'timestamp: ' . $timestamp
        );

        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_POST, count($body));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $err = curl_error($ch);
        $ret = curl_exec($ch);
        
        curl_close($ch);
        if($err) {
            $err = json_decode($err);
            return $err;
        } else {

            //Response
            $ret = json_decode($ret);
            if($ret->Status == 200) {
                // $balance  = $ret->Data;
                return $ret;
            } else {
                return $ret;
            }
            //End Response
        }
    }

    public function checkTransaction($transactionId, $va, $secret, $type){

        // checker type
        if($type == 'production'){
            $url          = 'https://my.ipaymu.com/api/v2/transaction'; //url
            $method       = 'POST'; //method
        }else{
            $url          = 'https://sandbox.ipaymu.com/api/v2/transaction'; //url sandbox
            $method       = 'POST'; //method
        }

        //Request Body//
        $body['transactionId']    = $transactionId;
        //End Request Body//

        //Generate Signature
        // *Don't change this
        $jsonBody     = json_encode($body, JSON_UNESCAPED_SLASHES);
        $requestBody  = strtolower(hash('sha256', $jsonBody));
        $stringToSign = strtoupper($method) . ':' . $va . ':' . $requestBody . ':' . $secret;
        $signature    = hash_hmac('sha256', $stringToSign, $secret);
        $timestamp    = Date('YmdHis');
        //End Generate Signature


        $ch = curl_init($url);

        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'va: ' . $va,
            'signature: ' . $signature,
            'timestamp: ' . $timestamp
        );

        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_POST, count($body));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $err = curl_error($ch);
        $ret = curl_exec($ch);
        
        curl_close($ch);
        if($err) {
            $err = json_decode($err);
            return $err;
        } else {

            //Response
            $ret = json_decode($ret);
            if($ret->Status == 200) {
                // $balance  = $ret->Data;
                return $ret;
            } else {
                return $ret;
            }
            //End Response
        }
    }

    //$body['product'] = array();
    //$body['product'][0] = barang yang belum ditambahkan ongkir dan lain lain (harga barang mentah)
    //contoh Request Body//
    // $body['product']    = array('headset', 'softcase');
    // $body['qty']        = array('1', '3');
    // $body['price']      = array('100000', '20000');
    // $body['returnUrl']  = 'https://mywebsite.com/thankyou';
    // $body['cancelUrl']  = 'https://mywebsite.com/cancel';
    // $body['notifyUrl']  = 'https://mywebsite.com/notify';
    //End Request Body//
    public function requestPayment($va, $secret, $body, $type = 'sandbox'){

        // checker type
        if($type == 'production'){
            $url          = 'https://my.ipaymu.com/api/v2/payment'; //url
            $method       = 'POST'; //method
        }else{
            $url          = 'https://sandbox.ipaymu.com/api/v2/payment'; //url sandbox
            $method       = 'POST'; //method
        }

        // Set metode dan fee pembayaran
        $body['paymentMethod'] = "";
        $total_price = '';

        // if ($body['paymentMethod']
        if ($body['paymentChannel'] == "indomaret" || $body['paymentChannel'] == "alfamart")
        {
            $body['paymentMethod'] = "cstore";
            $fee = 4000;
            array_push($body['product'],'Fee Ipaymu');
            array_push($body['qty'], '1');
            array_push($body['price'], ceil($fee));
            $total_price = array_sum($body['price']);
        }
        else if ($body['paymentChannel'] == "akulaku")
        {
            $body['paymentMethod'] = "akulaku";
            $harga = array_sum($body['price']);
            $fee = 5000 + ($harga*1.5)/100;
            array_push($body['product'],'Fee Ipaymu');
            array_push($body['qty'], '1');
            array_push($body['price'], ceil($fee));
            $total_price = array_sum($body['price']);
        }
        else if($body['paymentChannel'] == "qris")
        {
            $body['paymentMethod'] = "qris";
            $harga = array_sum($body['price']);
            $fee = $harga*0.7/100;
            array_push($body['product'],'Fee Ipaymu');
            array_push($body['qty'], '1');
            array_push($body['price'], ceil($fee));
            $total_price = array_sum($body['price']);
        }

        //Generate Signature
        // *Don't change this
        $jsonBody     = json_encode($body, JSON_UNESCAPED_SLASHES);
        $requestBody  = strtolower(hash('sha256', $jsonBody));
        $stringToSign = strtoupper($method) . ':' . $va . ':' . $requestBody . ':' . $secret;
        $signature    = hash_hmac('sha256', $stringToSign, $secret);
        $timestamp    = Date('YmdHis');
        //End Generate Signature


        $ch = curl_init($url);

        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'va: ' . $va,
            'signature: ' . $signature,
            'timestamp: ' . $timestamp
        );

        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_POST, count($body));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $err = curl_error($ch);
        $ret = curl_exec($ch);
        curl_close($ch);

        if($err) {
            $err = json_decode($err);
            return $err;
        } else {

            //Response
            $ret = json_decode($ret);
            // var_dump($ret);
            // exit;
            
            if($ret->Status == 200) {
                $ret->Data->total_price = $total_price;
                // $sessionId  = $ret->Data->SessionID;
                // $url        =  $ret->Data->Url;
                // $response = [
                //     "sessionId" => $sessionId,
                //     "url" => $url
                // ];
                return $ret;
                // header('Location:' . $url);
            }else{
                return $ret;
            }
            //End Response
        }
    }
}