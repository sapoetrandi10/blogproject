<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Woowa  
{
    public function sendWa($data,$fu_num)
    {
        // $id_p = [35,54,48,56]; // DA
        date_default_timezone_set('Asia/Jakarta');
        $nomor = "";
        $datas = [];
        $data['to'] = ltrim($data['to'],"+");
    	if (strlen($data['to']) >= 10) {
    		if (substr($data['to'],0,1) == "0") {
			$nomor = "62" .substr($data['to'], 1);
    		} else if (substr($data['to'],0,2) == "62") {
			$nomor = $data['to'];
    		} else if(substr($data['to'],0,1) == "8") {
			$nomor = "62" .$data['to'];
            }

            $key=$data['key'];
            $url='http://116.203.191.58/api/send_message';
            // $url='http://116.203.191.58/api/async_send_message';
            
            // if (!in_array($data['id_product'],$id_p)) {
            //     $key='04c9db4b679f606f54ea5b4aab38a9b1e93baa2ef47fc1bb';
            //     $url='http://116.203.92.59/api/send_message';
            // }else{
            //     $key='8181ce18826726d2d4ae39a68b3a85875f0c6a19eb5da71e';
            //     $url='http://116.203.92.59/api/send_message';
            // }

            $datas = array(
                "phone_no"=> $nomor,
                "key"		=>$key,
                "message"	=> trim($data['content']),
            );

            $data_string = json_encode($datas);
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_VERBOSE, 0);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
                curl_setopt($ch, CURLOPT_TIMEOUT, 360);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
                );
                $res=curl_exec($ch);
                $err = curl_error($ch);
                curl_close ($ch);
                $response = $res;
                if ($response != 'phone_offline' && $response != 'Other error') {
                    $datas = [
                        'phone_number'      => $data['to'],
                        'status'            => $response,
                        'id_order'          => $data['id_order'],
                        'follow_up_number'  => $nomor,
                        'datetime_exe'      => date('Y-m-d H:i:s'),
                    ]; 

                    if (isset($data['rotator'])) {
                       $datas['use_rotator'] = "YES";
                    }
                }else{
                    $datas = [
                        'phone_number'      => $data['to'],
                        'status'            => $response,
                        'id_order'          => $data['id_order'],
                        'follow_up_number'  => $nomor,
                        'datetime_exe'      => date('Y-m-d H:i:s'),
                    ]; 

                    if (isset($data['rotator'])) {
                       $datas['use_rotator'] = "YES";
                    }
                }
        }else{
            $datas = [
                'phone_number'      => $data['to'],
                'status'            => 'Not Valid Number',
                'id_order'          => $data['id_order'],
                'follow_up_number'  => $nomor,
                'datetime_exe'      => date('Y-m-d H:i:s'),
            ]; 

            if (isset($data['rotator'])) {
                $datas['use_rotator'] = "YES";
            }
        }
      return $datas;
    }

    public function checkNumber($phone_number, $woowa_key){
        $phone_no = trim($phone_number);
		$key = trim($woowa_key);
		$url='http://116.203.191.58/api/check_number';
		$data = array(
		  "phone_no" =>$phone_no,
		  "key"    =>$key
		);
		$data_string = json_encode($data);
		// dd($data_string);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 360);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  'Content-Type: application/json',
		  'Content-Length: ' . strlen($data_string))
		);
		$res=curl_exec($ch);
        $response = [];
        if($res == 'exists'){
            $response = [
                "status" => "success",
                "message" => $res
            ];
        }else if($res == 'not_exists'){
            $response = [
                "status" => "success",
                "message" => $res
            ];
        }else{
            $response = [
                "status" => "failed",
                "message" => "Akun tidak valid!"
            ];
        }
		curl_close($ch);
        // return $response;
        echo json_encode($response);
    }
}
