<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$whitelist = array(
    '127.0.0.1',
    '::1'
);
if (!in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
    //production
    if (!is_cli()){
        $a = explode('.',$_SERVER['HTTP_HOST']);
        if(count($a) == 3) {
            $ur =  $a[0].'.'.$a[1].'.'.$a[2];
            if ($ur == 'blog.dropshipaja.com' ) {
                if (empty($this->uri->segment(1))) {
                    $route['default_controller'] = 'katalog';
                }
            }else if ($ur == 'adminblog.dropshipaja.com' ) {
                if (empty($this->uri->segment(1))) {
                    $route['default_controller'] = 'dashboard';
                }else{
                    $route['register'] = 'login/vRegistration';
                }
            }
        }
    }
}else{
    //local
    if (!is_cli()){
        $a = explode('.',$_SERVER['HTTP_HOST']);
        if(count($a) == 3) {
            $ur =  $a[0].'.'.$a[1].'.'.$a[2];
            if ($ur == 'order.blogproject.com' ) {
                if (empty($this->uri->segment(1))) {
                    $route['default_controller'] = 'katalog';
                }
            }
        }else if(count($a) == 2){
            $ur =  $a[0].'.'.$a[1];
            if ($ur == 'blogproject.test' ) {
                if (empty($this->uri->segment(1))) {
                    $route['default_controller'] = 'dashboard';
                }else{
                    $route['register'] = 'login/vRegistration';
                }
            }
        }
    }
}


// $route['default_controller'] = 'blog';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
