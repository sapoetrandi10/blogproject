<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StudyCaseApi extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_blog');
		$this->load->model('M_kategori');
		$this->load->model('M_studycaseapi');

        if (!$this->session->userdata('email_administrator')) {
			redirect(base_url() . 'login');
        }
	}

    public function index()
	{
        $data['page_title'] = "Study Case Api";
        $this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('studycaseapi/V_studycaseapi', $data);
        $this->load->view('parts/v_footer1', $data);
	}

    public function getBarang()
	{
		header('Content-Type: application/json');
		$result = $this->M_studycaseapi->getBarang();
        // var_dump($result);
        // return $result;
        echo $result;
	}

    public function vTest2()
	{        
        $data['page_title'] = "Study Case";
        $this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('studycaseapi/V_test2', $data);
        $this->load->view('parts/v_footer1', $data);
	}

    public function getTest2()
	{
		$tb_barang = array(
            array(
                'kategori' => 1,
                'stok' => 1000,
            ),
            array(
                'kategori' => 2,
                'stok' => 1000,
            ),
            array(
                'kategori' => 3,
                'stok' => 1000,
            ),
            array(
                'kategori' => 7,
                'stok' => 1000,
            ),
        );
        
        $tb_kategori = array(
            [
                'id_kategori' => 4,
                'nama_kategori' => "kaos",
            ],
            [
                'id_kategori' => 2,
                'nama_kategori' => "baju"
            ],
            [
                'id_kategori' => 1,
                'nama_kategori' => "hoodie",
            ],
            [
                'id_kategori' => 3,
                'nama_kategori' => "casing",
            ],
        );

        sort($tb_kategori);

        $data_array = [];
        foreach ($tb_kategori as $key => $value) {
            if($tb_barang[$key]['kategori'] == $value['id_kategori']){
                $arr = [
                    "id_kategori" => $tb_barang[$key]['kategori'],
                    "nama_kategori" => $value['nama_kategori'],
                    "stok" => $tb_barang[$key]['stok']
                ];
                array_push($data_array,$arr);
            }

        }
        echo json_encode($data_array);
	}
}