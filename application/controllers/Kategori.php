<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_kategori');
		$this->load->library('Datatables');
		$this->load->library('upload');

		if (!$this->session->userdata('email_administrator')) {
			redirect(base_url() . 'login');
        }
	}
	
	public function index()
	{
		$data['allKategori'] = $this->M_kategori->getAllKategori();
        $data['page_title'] = "Kategori Blog";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('kategori_blog/v_kategori_blog', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function vAddKategori()
	{
		$data['page_title'] = "Add Kategori Blog";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('kategori_blog/v_add_kategori_blog');
		$this->load->view('parts/v_footer1', $data);
	}

	public function addKategori()
	{
		$nama_kategori = $this->input->post('nama_kategori', TRUE);
		$status_kategori = $this->input->post('status_kategori', TRUE);

		$this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'trim|required|min_length[2]|max_length[20]');
		$this->form_validation->set_rules('status_kategori', 'Status Kategori', 'trim|required|min_length[2]');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b>Kategori gagal disimpan!! '.validation_errors().'
			</div></div>');
			redirect(base_url() . "Kategori/vAddkategori");
			exit;
		}else{
			$allowed_types_thumbnail_kategori = "gif|jpg|jpeg|png";
			$upload_path_thumbnail_kategori = "assets/uploaded_images";
			$resize_path_thumbnail_kategori = "assets/resize_images";
			$field_thumbnail_kategori = "thumbnail_kategori";
			$thumbnail_kategori = upload($allowed_types_thumbnail_kategori, $upload_path_thumbnail_kategori, $resize_path_thumbnail_kategori, $field_thumbnail_kategori);

			$data = [
				"nama_kategori" => $nama_kategori,
				"status_kategori" => $status_kategori
			];
	
			if($thumbnail_kategori['status'] == 'true'){
				if(isset($thumbnail_kategori['thumbnail'])){
					$data["thumbnail_kategori"] =  $thumbnail_kategori['thumbnail'];
				}
			}else if($thumbnail_kategori['status'] == 'false'){
				$massage = $thumbnail_kategori['message'];
				$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>'.$massage.'!!</strong></div>');
				redirect(base_url("Kategori/vAddkategori"));
				exit;
			}
	
			$insertKategori = $this->M_kategori->addKategori($data);
			if($insertKategori){
				$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Kategori berhasil disimpan!!</strong></div>');
				redirect(base_url('Kategori'));
			}else{
				$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Kategori gagal disimpan!! silahkan coba lagi!!</strong></div>');
				redirect(base_url("Kategori/vAddkategori"));
			}
		}

	}

	public function vEditKategori($id_kategori)
	{
		$data['kategoriById'] = $this->M_kategori->getKategoriById($id_kategori);
        $data['page_title'] = "Edit Kategori Blog";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('kategori_blog/v_edit_kategori_blog', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function editKategori()
	{
		$nama_kategori = $this->input->post('nama_kategori', TRUE);
		$status_kategori = $this->input->post('status_kategori', TRUE);
		$id_kategori = $this->input->post('id_kategori', TRUE);	
		$latest_img = $this->input->post('latest_img', TRUE);
		$thumbnail_kategori = $this->input->post('thumbnail_Kategori', TRUE);
		$data = [];

		$this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'trim|required|min_length[2]|max_length[20]');
		$this->form_validation->set_rules('status_kategori', 'Status Kategori', 'trim|required|min_length[2]');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Kategori gagal disimpan!! '.validation_errors().'
			</div></div>');
			redirect(base_url() . "Kategori/vEditKategori/$id_kategori");
			exit;
		}else{

			$allowed_types_thumbnail_kategori = "gif|jpg|jpeg|png";
			$upload_path_thumbnail_kategori = "assets/uploaded_images";
			$resize_path_thumbnail_kategori = "assets/resize_images";
			$field_thumbnail_kategori = "thumbnail_kategori";
			$thumbnail_kategori = upload($allowed_types_thumbnail_kategori, $upload_path_thumbnail_kategori, $resize_path_thumbnail_kategori, $field_thumbnail_kategori);
			// exit;
			$data = [
				"nama_kategori" => $nama_kategori,
				"status_kategori" => $status_kategori
			];

			if($thumbnail_kategori['status'] == 'true'){
				if(isset($thumbnail_kategori['thumbnail'])){
					$data["thumbnail_kategori"] =  $thumbnail_kategori['thumbnail'];
				}
			}else if($thumbnail_kategori['status'] == 'false'){
				$massage = $thumbnail_kategori['message'];
				$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>'.$massage.'!!</strong></div>');
				redirect(base_url("Kategori/vEditKategori/$id_kategori"));
				exit;
			}
	
			
			$editKategori = $this->M_kategori->editKategori($id_kategori, $data);
	
			if($editKategori){
				$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Kategori berhasil diupdate!!</strong></div>');
				redirect(base_url('Kategori'));
			}else{
				$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Kategori gagal diupdate!! silahkan coba lagi!!</strong></div>');
				redirect(base_url("Kategori/vEditKategori/$id_kategori"));
			}
		}
	}

	public function deleteKategori($id_kategori)
	{
		$deleteKategori = $this->M_kategori->deleteKategori($id_kategori);

		if($deleteKategori){
			$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Kategori berhasil didelete!!</strong></div>');
			redirect(base_url('Kategori'));
		}else{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Kategori gagal didelete!! silahkan coba lagi!!</strong></div>');
			redirect(base_url('Kategori'));
		}
	}

	function getAllKategoriJson()
	{
		header('Content-Type: application/json');
		echo $this->M_kategori->getAllKategoriJson();
	}

	public function upload($allowed_types, $upload_path, $resize_path, $field){
		$config['upload_path']= $upload_path;
		$config['allowed_types']= $allowed_types;
		$config['encrypt_name'] = TRUE;
		$config['max_size']     = 134217728;
		$this->upload->initialize($config);
		if ($_FILES[$field]['size'] != 0) {
			if ( ! $this->upload->do_upload($field)){
				$this->session->set_flashdata('error','Upload failed!');
				$data['error'] = $this->upload->display_errors();
				// redirect(base_url().'acara/' .$post['id_order'] .'/edit_acara#calon');
				// exit;
				return false;
				// return $data['error'];
				// if($data['error'] === "<p>You did not select a file to upload.</p>"){
				// 	return false;
				// }
			} else {
				$uploaded_data = $this->upload->data();
				$this->resizeImage($uploaded_data['file_name'], $upload_path, $resize_path);
				$dataimage = $this->upload->data();
				$thumbnail = $dataimage['raw_name'].'_thumb'.$dataimage['file_ext'];
				return $uploaded_data['file_name'];
			}
		}
	}
	
	public function resizeImage($filename, $upload_path, $resize_path) {
		$source_path = $upload_path ."/" .$filename;
		$target_path = $resize_path ."/";
		$config_manip = array(
			'image_library' => 'gd2',
			'source_image' => $source_path,
			'new_image' => $target_path,
			'maintain_ratio' => TRUE,
			'create_thumb' => TRUE,
			'thumb_marker' => '_thumb',
			'quality' => '80%',
			'master_dim' => 'auto',
			'width' => '800',
			'height' => '800'
		);
		if (is_object(@$this->image_lib) ? TRUE : FALSE) {
		$this->image_lib->initialize($config_manip);      	
		if (!$this->image_lib->resize()) {
			echo $this->image_lib->display_errors();
		}
	  } else {
		$this->load->library('image_lib', $config_manip);
		  if (!$this->image_lib->resize()) {
			  echo $this->image_lib->display_errors();
		  }
	  }
		$this->image_lib->clear();
		unlink($source_path);
	}
}
