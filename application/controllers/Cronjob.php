<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cronjob extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
        $this->load->model('M_cronjob');
        // $today = date("Y-m-d H:i:s");
    }

    public function index()
    {
        echo "404 not found";
    }
    
    public function getFollowUpByOrder()
    {
        $fuwa = $this->M_cronjob->getFollowUpByOrder();
        // var_dump(count($fuwa));
        // exit;
       
        if($fuwa){
            $tamplate_fuwa = '';
            $updateFU = [];
            $interval_datetime_order = -1;
            foreach ($fuwa as $key => $value) {
                $data = [
                    'to' => $value['no_whatsapp'],
                    'key' => $value['woowa_key'],
                    'id_order' => $value['id_order'],
                    'follow_up' => 'none'
                ];

                
                $datetime_order = strtotime($value['datetime_order']);
                
                if($value['auto_fu'] === 'yes'){
                    if($value['follow_up_1'] === 'no' && $value['status_pembayaran'] === 'belum' && $value['tamplate_fuwa1'] !== ''){
                        $tamplate_fuwa = $value['tamplate_fuwa1'];
                        $data['follow_up'] = 'follow up 1';
                        $day_fu1 = $value['day_fu1'];
                        $hour_fu1 = $value['hour_fu1'];
                        $minute_fu1 = $value['minute_fu1'];
                        $interval_datetime_order = strtotime("+$day_fu1 days $hour_fu1 hours $minute_fu1 minutes", $datetime_order);
                        $updateFU['follow_up_1'] = 'yes';
                    }else if($value['follow_up_2'] === 'no' && $value['status_pembayaran'] === 'belum' && $value['tamplate_fuwa2'] !== ''){
                        $tamplate_fuwa = $value['tamplate_fuwa2'];
                        $data['follow_up'] = 'follow up 2';
                        $day_fu2 = $value['day_fu2'];
                        $hour_fu2 = $value['hour_fu2'];
                        $minute_fu2 = $value['minute_fu2'];
                        $interval_datetime_order = strtotime("+$day_fu2 days $hour_fu2 hours $minute_fu2 minutes", $datetime_order);
                        $updateFU['follow_up_2'] = 'yes';
                    }else if($value['follow_up_3'] === 'no' && $value['status_pembayaran'] === 'belum' && $value['tamplate_fuwa3'] !== ''){
                        $tamplate_fuwa = $value['tamplate_fuwa3'];
                        $data['follow_up'] = 'follow up 3';
                        $day_fu3 = $value['day_fu3'];
                        $hour_fu3 = $value['hour_fu3'];
                        $minute_fu3 = $value['minute_fu3'];
                        $interval_datetime_order = strtotime("+$day_fu3 days $hour_fu3 hours $minute_fu3 minutes", $datetime_order);
                        $updateFU['follow_up_3'] = 'yes';
                    }else if($value['follow_up_4'] === 'no' && $value['status_pembayaran'] === 'belum' && $value['tamplate_fuwa4'] !== ''){
                        $tamplate_fuwa = $value['tamplate_fuwa4'];
                        $data['follow_up'] = 'follow up 4';
                        $day_fu4 = $value['day_fu4'];
                        $hour_fu4 = $value['hour_fu4'];
                        $minute_fu4 = $value['minute_fu4'];
                        $interval_datetime_order = strtotime("+$day_fu4 days $hour_fu4 hours $minute_fu4 minutes", $datetime_order);
                        $updateFU['follow_up_4'] = 'yes';
                    }else if($value['follow_up_process'] === 'no' && $value['status_order'] === 'process' && $value['tamplate_fuwa_process'] !== ''){
                        $tamplate_fuwa = $value['tamplate_fuwa_process'];
                        $data['follow_up'] = 'follow up process';
                        $day_fu_process = $value['day_fu_process'];
                        $hour_fu_process = $value['hour_fu_process'];
                        $minute_fu_process = $value['minute_fu_process'];
                        $interval_datetime_order = strtotime("+$day_fu_process days $hour_fu_process hours $minute_fu_process minutes", $datetime_order);
                        $updateFU['follow_up_process'] = 'yes';
                    }else if($value['follow_up_cancel'] === 'no' && $value['status_cancel'] === 'yes' && $value['auto_fu_cancel'] === 'yes' && $value['tamplate_fuwa_cancel'] !== ''){
                        $tamplate_fuwa = $value['tamplate_fuwa_cancel'];
                        $data['follow_up'] = 'follow up cancel';
                        $updateFU['follow_up_cancel'] = 'yes';
                    }
                    preg_match_all("/\[\[[^\]]*\]\]/", $value['tamplate_fuwa1'], $variabels);
                    if (!empty($variabels[0])){
                        $variabels = array_unique($variabels[0]);
                        foreach ($variabels as $variabel) {
                            $var = str_replace("[[", "", $variabel);
                            $var = str_replace("]]", "", $var);
                            $tamplate_fuwa = str_replace($variabel, $value[$var], $tamplate_fuwa);
                        }
                        $data['content'] = $tamplate_fuwa;
                    }

                    if($interval_datetime_order < strtotime("today 08:00:00") && $interval_datetime_order !== -1){
                        // kirim follow up wa
                        $woowa = $this->woowa->sendWa($data, $value['no_whatsapp']);
                        if($woowa['status'] === 'Success'){
                            $this->M_cronjob->updateFollowUpStatus($value['id_order'], $updateFU);
                        }
                        var_dump($woowa);
                    }
                }
            }
        }
    }
}