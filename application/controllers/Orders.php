<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('M_orders');
		$this->load->library('Datatables');
		$this->load->library('upload');

		if (!$this->session->userdata('email_administrator')) {
			redirect(base_url() . 'login');
        }
	}
	
	public function index()
	{
        $data['page_title'] = "Orders";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('orders/v_orders', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function vProgressOrder()
	{
        $data['page_title'] = "Orders";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('orders/v_orders_progress', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	function getAllOrderJson()
	{
		header('Content-Type: application/json');
		echo $this->M_orders->getAllOrderJson();
	}

	function getProgressOrderJson()
	{
		header('Content-Type: application/json');
		echo $this->M_orders->getProgressOrderJson();
	}

	
	public function updateStatusOrder($status_order, $id_order){
		$updateStatusOrder = $this->M_orders->updateStatusOrder($status_order, $id_order);

		$redirectPage = "orders";
		$status = array('process', 'complete', 'refund', 'cancel');
		
		if(in_array($status_order ,$status)){
			$redirectPage = "orders/vProgressOrder/";
		}
		
		if($updateStatusOrder){
			$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Status Order berhasil diupdate!!</strong></div>');
			redirect(base_url($redirectPage));
		}else{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Status Order gagal diupdate!! silahkan coba lagi!!</strong></div>');
			redirect(base_url($redirectPage));
		}
    }

	public function getOrderById($id_order)
	{
		echo $this->M_orders->getOrderById($id_order);
	}

	public function confirmPayment()
	{
		$today = date('Y-m-d h:i:s', time());
		$post = $this->input->post();
		$id_order = $this->input->post('id_order', TRUE);
		$nama_bank = $this->input->post('nama_bank', TRUE);
		$no_rek = $this->input->post('no_rek', TRUE);
		$atas_nama = $this->input->post('atas_nama', TRUE);
		$rekening_pembayaran = $this->input->post('rekening_pembayaran', TRUE);
		$tgl_transfer = $this->input->post('tgl_transfer', TRUE);
		$encodeIdOrder = base64_encode($id_order);

		

		$this->form_validation->set_rules('nama_bank', 'Nama Bank', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('no_rek', 'No rekening', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('atas_nama', 'Nama pemilik rekening', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('rekening_pembayaran', 'Rekening Tujuan', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('tgl_transfer', 'Tanggal transfer', 'trim|required');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Konfirmasi Pembayaran anda belum berhasil!! '.validation_errors().'
			</div></div>');
			redirect(base_url() . "store/katalog/paymentConfirmation/?cp=$encodeIdOrder");
			exit;
		}else{
			$allowed_types = "jpg|jpeg|png";
			$upload_path = "assets/uploaded_images";
			$resize_path = "assets/resize_images";
			$field = "bukti_transfer";
			$bukti_transfer = upload($allowed_types, $upload_path, $resize_path, $field);

			$data = [
				'tgl_transfer' => $tgl_transfer,
				'datetime_konfirmasi_pembayaran' => $today,
				'no_rek' => $no_rek,
				'atas_nama' => $atas_nama,
				'dari_bank' => $nama_bank,
				'ke_bank' => $rekening_pembayaran,
				'bukti_transfer' => $bukti_transfer
			];

			if($bukti_transfer == null){
				$data = [
					'tgl_transfer' => $tgl_transfer,
					'datetime_konfirmasi_pembayaran' => $today,
					'no_rek' => $no_rek,
					'atas_nama' => $atas_nama,
					'dari_bank' => $nama_bank,
					'ke_bank' => $rekening_pembayaran,
				];
			}

			$result = $this->M_orders->confirmPayment($id_order, $data);
			// $id_order = $this->db->insert_id();
			if($result){
				redirect(base_url("store/katalog/thankYouPage/?cp=$encodeIdOrder"));
			}else{
				$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Maaf Konfirmasi pembayaran anda belum berhasil. silahkan ulangi lagi!</strong></div>');
				redirect(base_url("store/katalog/paymentConfirmation/?cp=$encodeIdOrder"));
			}
		}

	}
	
	public function createOrder()
	{
		$today = date('Y-m-d h:i:s', time());
		$name = $this->input->post('name', TRUE);
		$no_whatsapp = $this->input->post('no_whatsapp', TRUE);
		$email = $this->input->post('email', TRUE);
		$kota = $this->input->post('kota', TRUE);
		$kode_unik = $this->input->post('kode_unik', TRUE);
		$total_order = $this->input->post('total_order', TRUE);
		$nama_produk = $this->input->post('nama_produk', TRUE);
		$id_produk = $this->input->post('id_produk', TRUE);
		// $post = $this->input->post();

		$this->form_validation->set_rules('name', 'Nama', 'trim|required');
		$this->form_validation->set_rules('no_whatsapp', 'No Whatsapp', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('kota', 'Kota', 'trim|required');
		$this->form_validation->set_rules('kode_unik', 'Kode unik', 'trim');
		$this->form_validation->set_rules('total_order', 'Total order', 'trim');
		$this->form_validation->set_rules('nama_produk', 'Nama produk', 'trim');
		$this->form_validation->set_rules('id_produk', 'Id produk', 'trim');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b>Pembelian anda belum berhasil!! '.validation_errors().'
			</div></div>');
			redirect(base_url() . "store/katalog/checkoutPage/$id_produk");
			exit;
		}else{
			
				$data = [
					'nama_pemesan' => $name,
					'no_whatsapp' => $no_whatsapp,
					'email' => $email,
					'kota' => $kota,
					'datetime_order' => $today,
					'kode_unik' => $kode_unik,
					'total_order' => $total_order,
					'status_pembayaran' => 'belum',
					'nama_produk' => $nama_produk,
					'id_produk' => $id_produk,
					'status_cancel' => 'no',
				];

			$result = $this->M_orders->createOrder($data);
			$id_order = $this->db->insert_id();
			// $encodeIdProduk = base64_encode($id_order);
			$encodeJumlahTransfer = base64_encode($total_order);
			$encodeIdOrder = base64_encode($id_order);
			if($result){
				redirect(base_url("store/katalog/checkoutSuccess/?cp=$encodeIdOrder"));
			}else{
				$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Maaf pembelian anda belum berhasil. silahkan ulangi lagi!</strong></div>');
				redirect(base_url("store/katalog/checkoutPage/$id_produk"));
			}
		}
	}
}