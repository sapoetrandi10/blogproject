<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tickets extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('M_tickets');
		$this->load->library('Datatables');
		$this->load->library('upload');

		if (!$this->session->userdata('email_administrator')) {
			redirect(base_url() . 'login');
        }
	}
	
	public function index()
	{
        $data['page_title'] = "Tickets";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('tickets/v_tickets', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function vCreateTicket()
	{
		$data['page_title'] = "Create ticket";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('tickets/v_create_ticket', $data);
		$this->load->view('parts/v_footer1', $data);
	}
	
	public function adminCreateTicket()
	{
		$datetime = date('Y-m-d h:i:s', time());
		$subject = $this->input->post('subject', TRUE);
		$isi_ticket = $this->input->post('isi_ticket', TRUE);
		$tujuan_pengirim = $this->input->post('tujuan_pengirim', TRUE);

		$this->form_validation->set_rules('subject', 'Nama Divisi', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('isi_ticket', 'Deskripsi', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('tujuan_pengirim', 'Tujuan', 'trim|required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Create Ticket Failed! '.validation_errors().'
			</div></div>');
			
			redirect(base_url() . 'tickets/vCreateTicket');
			exit;
		}else{
			$allowed_types_attachment = "jpg|jpeg|png|pdf|docx|csv|xlsx";
			$upload_path_attachment = "assets/uploaded_images";
			$resize_path_attachment = "assets/resize_images";
			$field_attachment = "attachment";
			$max_size = 5242880;

			$attachment = upload($allowed_types_attachment, $upload_path_attachment, $resize_path_attachment, $field_attachment, $max_size);
			
			$data = [
				'isi_ticket' => $isi_ticket,
				'email_pengirim' => $this->session->userdata('email_administrator'),
				'pengirim' => 'admin',
				'tujuan_pengirim' => $tujuan_pengirim,
				'subject' => $subject,
				'status_ticket' => 'unresolved',
				'datetime_ticket' => $datetime,
				'attachment' => $attachment['thumbnail'],
			];

			if($attachment['status'] == 'false'){
				$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Ticket gagal dibuat!! '.$attachment["message"].'!!</strong></div>');
				redirect(base_url("tickets/vCreateTicket"));
				exit;
			}

			if($attachment == null){
				$data = [
					'isi_ticket' => $isi_ticket,
					'email_pengirim' => $this->session->userdata('email_administrator'),
					'pengirim' => 'admin',
					'tujuan_pengirim' => $tujuan_pengirim,
					'subject' => $subject,
					'status_ticket' => 'unresolved',
					'datetime_ticket' => $datetime,
				];
			}
	
			$result = $this->M_tickets->adminCreateTicket($data);

			if($result){
                $this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Ticket berhasil dibuat!!</strong></div>');
                redirect(base_url('tickets'));
            }else{
                $this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Ticket gagal dibuat!! silahkan coba lagi!!</strong></div>');
                redirect(base_url("tickets/vCreateTicket"));
            }
		}
	}


	public function vDetailTicket($id_ticket)
	{
        $data['page_title'] = "Detail ticket";
        $data['detailTicket'] = $this->M_tickets->getDetailTicketById($id_ticket);
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('tickets/v_detail_ticket', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function adminResolvedTicket($id_ticket)
	{
		$adminResolvedTicket = $this->M_tickets->adminResolvedTicket($id_ticket);

		if($adminResolvedTicket){
			$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Ticket telah ditutup (resolved)!!</strong></div>');
			redirect(base_url('tickets'));
		}else{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Ticket gagal ditutup (unresolved)!! silahkan coba lagi!!</strong></div>');
			redirect(base_url('tickets'));
		}
	}

	public function getAllTicketJson()
	{
		header('Content-Type: application/json');
		echo $this->M_tickets->getAllTicketJson();
	}
	
	public function getTicketBalasan($id_ticket)
	{
		// header('Content-Type: application/json');
		echo $this->M_tickets->getTicketBalasan($id_ticket);
	}

	
	public function getAllMember(){
		$allMember = $this->M_tickets->getAllMember();
		$html = "<option value=''>-- Silahkan Pilih Tujuan Ticket --</option>";
		foreach ($allMember as $key => $value) {
			$html .= "<option value=".$value['email_member'].">".$value['email_member']." - ".$value['nama_member']."</option>";
		}
		echo(json_encode($html));
	}

	public function uploadWithAjax()
	{
		// $allowed_types_attachment = "jpg|jpeg|png|pdf|docx|csv|xlsx";
		$allowed_types_attachment = ["jpg", "jpeg", "png", "pdf", "docx", "xlsx" , "csv"];
		$upload_path_attachment = "assets/uploaded_images";
		$resize_path_attachment = "assets/resize_images";
		$field_attachment = "attachment";
		$this->load->library('image_lib');

		$attachment = upload($allowed_types_attachment, $upload_path_attachment, $resize_path_attachment, $field_attachment);
		
		// $attachment = multipleUploadImage($field_attachment, $upload_path_attachment, $resize_path_attachment, $allowed_types_attachment);

		if(isset($attachment['status']) && $attachment['status'] == 'false'){
			echo json_encode($attachment);
		}else{
			echo json_encode($attachment);
		}


	}
	
	public function postBalasanAdmin()
	{
		$datetime = date('Y-m-d H:i:s', time());
		$id_ticket = $this->input->post('id_ticket', TRUE);
		$tujuan_pengirim = $this->input->post('tujuan_pengirim', TRUE);
		$komentar = $this->input->post('komentar', TRUE);
		$attachment = $this->input->post('attachment', TRUE);

		$data = [
			'id_ticket' => $id_ticket,
			'pengirim' => 'admin',
			'email_pengirim' => $this->session->userdata('email_administrator'),
			'tujuan_pengirim' => $tujuan_pengirim,
			'komentar' => $komentar,
			'tanggal_post' => $datetime,
			'status_baca' => 'belum_dibaca',
		];

		if($attachment != null){
			$data['attachment'] = $attachment;
		}	

		$result = $this->M_tickets->postBalasanAdmin($data);
		
		return $result;
	}

	public function updateStatusBaca($id_ticket){
		$updateStatusBaca = $this->M_tickets->updateStatusBaca($id_ticket);
		
		// echo $updateStatusBaca;
		return $updateStatusBaca;
    }
}