<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('M_produk');
		$this->load->model('M_rekening');
		$this->load->model('M_kategori_produk');
		$this->load->model('M_gateway');
		$this->load->library('Datatables');
		$this->load->library('upload');

		if (!$this->session->userdata('email_administrator')) {
			redirect(base_url() . 'login');
        }
	}
	
	public function index()
	{
        $data['page_title'] = "Produk";
        $data['nama_admin'] = $this->session->userdata('nama_administrator');
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('produk/v_produk', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function vAddProduk()
	{
        $data['page_title'] = "Produk";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('produk/v_add_produk', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function addProduk()
	{
		$nama_produk = $this->input->post('nama_produk', TRUE);
		$kategori = $this->input->post('kategori', TRUE);
		$harga = $this->input->post('harga', TRUE);
		$harga_coret = $this->input->post('harga_coret', TRUE);
		$url = $this->input->post('url', TRUE);
		$keterangan_produk = $this->input->post('keterangan_produk', TRUE);
		$text_button_beli = $this->input->post('text_button_beli', TRUE);
		$subtext_button_beli = $this->input->post('subtext_button_beli', TRUE);
		$warna_button_beli = $this->input->post('warna_button_beli', TRUE);
		$rekening_pembayaran = $this->input->post('rekening_pembayaran[]', TRUE);
		$text_konfirmasi_pembayaran = $this->input->post('text_konfirmasi_pembayaran', FALSE);
		$select_payment_gateway = $this->input->post('select_payment_gateway', TRUE);
		$bukti_pembayaran = $this->input->post('bukti_pembayaran', TRUE) ? $this->input->post('bukti_pembayaran', TRUE) : "off";
		$bank_transfer = $this->input->post('bank_transfer', TRUE) ? "bank" : null;
		$payment_gateway = $this->input->post('payment_gateway', TRUE) ? "payment_gateway" : null;
		$thumbnail_produk = "";
		
		$payment_checked = [];
		
		if($bank_transfer != null){
			array_push($payment_checked, $bank_transfer);
		}
		
		if($payment_gateway != null){
			array_push($payment_checked, $payment_gateway);
		}
		$available_payment = json_encode($payment_checked);
		$select_bank = json_encode($rekening_pembayaran);

		$this->form_validation->set_rules('nama_produk', 'Nama Produk', 'trim|required|min_length[3]|max_length[30]');
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required');
		$this->form_validation->set_rules('harga', 'Harga', 'trim|required|numeric');
		$this->form_validation->set_rules('harga_coret', 'Harga Coret', 'trim|required|numeric');
		$this->form_validation->set_rules('url', 'URL', 'trim|required|min_length[5]');
		$this->form_validation->set_rules('keterangan_produk', 'Keterangan Produk', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('text_button_beli', 'Text Button Beli', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('subtext_button_beli', 'Subtext Button Beli', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('warna_button_beli', 'Warna Button Beli', 'trim|required');
		$this->form_validation->set_rules('rekening_pembayaran[]', 'Rekening Pembayaran', 'trim|required');
		// $this->form_validation->set_rules('bukti_pembayaran', 'Bukti Pembayaran', 'trim|required|min_length[5]');
		$this->form_validation->set_rules('text_konfirmasi_pembayaran', 'Text Konfirmasi Pembayaran', 'trim|required|min_length[5]');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b>Gagal menambahkan produk!! '.validation_errors().'
			</div></div>');

			// redirect(base_url() . 'vAddProduk');
			redirect(base_url() . 'Produk/vAddProduk');
			exit;
		}else{

			$allowed_types = ["jpg", "jpeg", "png"];
			$upload_path = "./assets/uploaded_images";
			$resize_path = "./assets/resize_images";
			$max_size = 5242880;
			$field_attachment = "thumbnail_produk";
	
			$thumbnail_produk_name = multipleUploadImage($field_attachment, $upload_path, $resize_path, $allowed_types, $max_size);

			$data = [
				'nama_produk' => $nama_produk,
				'kategori' => $kategori,
				'harga' => $harga,
				'harga_coret' => $harga_coret,
				'url' => $url,
				'keterangan_produk' => $keterangan_produk,
				'text_button_beli' => $text_button_beli,
				'subtext_button_beli' => $subtext_button_beli,
				'warna_button_beli' => $warna_button_beli,
				'rekening_pembayaran' => $select_bank,
				'select_payment_gateway' => $select_payment_gateway,
				'bukti_pembayaran' => $bukti_pembayaran,
				'available_payment' => $available_payment,
				'text_konfirmasi_pembayaran' => $text_konfirmasi_pembayaran
			];

			if($thumbnail_produk_name['status'] == 'false'){
				if(isset($thumbnail_produk_name['message'])){
					$message = $thumbnail_produk_name['message'];
					$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>'.$message.'</strong></div>');
					redirect(base_url('produk/vAddProduk'));
				}
			}else if($thumbnail_produk_name['status'] == 'true'){
				if(isset($thumbnail_produk_name['image'])){
					$thumbnail_produk = json_encode($thumbnail_produk_name['image']);
					$data['thumbnail_produk'] = $thumbnail_produk;
				}
			}
			
			$result = $this->M_produk->addProduk($data);

			if($result){
                $this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Produk berhasil ditambahkan!!</strong></div>');
                redirect(base_url('produk'));
            }else{
                $this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Produk gagal ditambahkan!! silahkan coba lagi!!</strong></div>');
                redirect(base_url("produk/vAddProduk"));
            }
		}
	}

	public function vEditProduk($id_produk)
	{
        $data['page_title'] = "Produk";
        $data['ProdukById'] = $this->M_produk->getProdukById($id_produk);
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('produk/v_edit_produk', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function editProduk()
	{
		$id_produk = $this->input->post('id_produk', TRUE);
		$nama_produk = $this->input->post('nama_produk', TRUE);
		$kategori = $this->input->post('kategori', TRUE);
		$harga = $this->input->post('harga', TRUE);
		$harga_coret = $this->input->post('harga_coret', TRUE);
		$url = $this->input->post('url', TRUE);
		$keterangan_produk = $this->input->post('keterangan_produk', TRUE);
		$text_button_beli = $this->input->post('text_button_beli', TRUE);
		$subtext_button_beli = $this->input->post('subtext_button_beli', TRUE);
		$warna_button_beli = $this->input->post('warna_button_beli', TRUE);
		$rekening_pembayaran = $this->input->post('rekening_pembayaran[]', TRUE);
		$select_payment_gateway = $this->input->post('select_payment_gateway', TRUE);
		$bukti_pembayaran = $this->input->post('bukti_pembayaran', TRUE) ? $this->input->post('bukti_pembayaran', TRUE) : "off";
		$text_konfirmasi_pembayaran = $this->input->post('text_konfirmasi_pembayaran', FALSE);
		$bank_transfer = $this->input->post('bank_transfer', TRUE) ? "bank" : null;
		$payment_gateway = $this->input->post('payment_gateway', TRUE) ? "payment_gateway" : null;
		$thumbnail_produk = "";
		$payment_checked = [];

		if($bank_transfer != null){
			array_push($payment_checked, $bank_transfer);
		}

		if($payment_gateway != null){
			array_push($payment_checked, $payment_gateway);
		}
		$available_payment = json_encode($payment_checked);
		$select_bank = json_encode($rekening_pembayaran);

		$this->form_validation->set_rules('nama_produk', 'Nama Produk', 'trim|required|min_length[3]|max_length[30]');
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required');
		$this->form_validation->set_rules('harga', 'Harga', 'trim|required|numeric');
		$this->form_validation->set_rules('harga_coret', 'Harga Coret', 'trim|required|numeric');
		$this->form_validation->set_rules('url', 'URL', 'trim|required|min_length[5]');
		$this->form_validation->set_rules('keterangan_produk', 'Keterangan Produk', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('text_button_beli', 'Text Button Beli', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('subtext_button_beli', 'Subtext Button Beli', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('warna_button_beli', 'Warna Button Beli', 'trim|required');
		$this->form_validation->set_rules('rekening_pembayaran[]', 'Rekening Pembayaran', 'trim|required');
		// $this->form_validation->set_rules('bukti_pembayaran', 'Bukti Pembayaran', 'trim|required|min_length[5]');
		$this->form_validation->set_rules('text_konfirmasi_pembayaran', 'Text Konfirmasi Pembayaran', 'trim|required|min_length[5]');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b>Gagal mengedit produk!! '.validation_errors().'
			</div></div>');

			// redirect(base_url() . 'vAddProduk');
			redirect(base_url() . "Produk/vEditProduk/$id_produk");
			exit;
		}else{

			$allowed_types = ["jpg", "jpeg", "png"];
			$upload_path = "./assets/uploaded_images";
			$resize_path = "./assets/resize_images";
			$max_size = 5242880;
			$field_attachment = "thumbnail_produk";
	
			$thumbnail_produk_name = multipleUploadImage($field_attachment, $upload_path, $resize_path, $allowed_types, $max_size);

			$data = [
				'nama_produk' => $nama_produk,
				'kategori' => $kategori,
				'harga' => $harga,
				'harga_coret' => $harga_coret,
				'url' => $url,
				'keterangan_produk' => $keterangan_produk,
				'text_button_beli' => $text_button_beli,
				'subtext_button_beli' => $subtext_button_beli,
				'warna_button_beli' => $warna_button_beli,
				'rekening_pembayaran' => $select_bank,
				'select_payment_gateway' => $select_payment_gateway,
				'bukti_pembayaran' => $bukti_pembayaran,
				'available_payment' => $available_payment,
				'text_konfirmasi_pembayaran' => $text_konfirmasi_pembayaran
			];

			if($thumbnail_produk_name['status'] == 'false'){
				if(isset($thumbnail_produk_name['message'])){
					$message = $thumbnail_produk_name['message'];
					$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>'.$message.'</strong></div>');
					redirect(base_url("Produk/vEditProduk/$id_produk"));
				}
			}else if($thumbnail_produk_name['status'] == 'true'){
				if(isset($thumbnail_produk_name['image'])){
					$thumbnail_produk = json_encode($thumbnail_produk_name['image']);
					$data['thumbnail_produk'] = $thumbnail_produk;
				}
			}
						
			$editProduk = $this->M_produk->editProduk($id_produk, $data);
	
			if($editProduk){
				$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Produk berhasil diedit!!</strong></div>');
				redirect(base_url('produk'));
			}else{
				$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Produk gagal diedit!! silahkan coba lagi!!</strong></div>');
				redirect(base_url("Produk/vEditProduk/$id_produk"));
			}
		}
	}

	public function getAllRekening(){
		$allRekening = $this->M_rekening->getAllRekening();
		$html = "<option value='' disabled>-- Silahkan pilih rekening --</option>";
		foreach ($allRekening as $key => $value) {
			$html .= "<option value=".$value['no_rekening'].">".$value['nama_bank']." - ".$value['nama_rekening']." - ".$value['no_rekening']."</option>";
		}
		echo(json_encode($html));
	}

	public function getAllPaymentGateway(){
		$result = $this->M_gateway->getAllPaymentGateway();
		$html = "<option value='default'>-- Default --</option>";
		foreach ($result as $key => $value) {
			$html .= "<option value=".$value['id'].">".$value['payment_gateway']." - ".$value['nama']."</option>";
		}
		echo(json_encode($html));
	}

	public function getAllKategoriproduk(){
		$getAllKategoriproduk = $this->M_kategori_produk->getAllKategoriproduk();
		
		$html = "<option value='' selected disabled>-- Silahkan pilih kategori --</option>";
		foreach ($getAllKategoriproduk as $key => $value) {
			$nama_kategori_produk = $value['nama_kategori_produk'];
			$html .= '<option value="'.$nama_kategori_produk.'">'.$nama_kategori_produk.'</option>';
		}
		echo(json_encode($html));
	}

	public function getAllProduk(){
		$getAllProduk = $this->M_produk->getAllProduk();
		echo(json_encode($getAllProduk));
	}

	public function deleteProduk($id_produk)
	{
		$deleteProduk = $this->M_produk->deleteProduk($id_produk);

		if($deleteProduk){
			$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Produk berhasil didelete!!</strong></div>');
			redirect(base_url('produk'));
		}else{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Produk gagal didelete!! silahkan coba lagi!!</strong></div>');
			redirect(base_url('produk'));
		}
	}

	public function searchProduk()
	{
		$keyword = $this->input->post('keyword', TRUE);

		$result = $this->M_produk->searchProduk($keyword);
		$result = json_encode($result);
		echo $result;
	}
}