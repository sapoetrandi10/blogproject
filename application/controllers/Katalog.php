<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Katalog extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('M_produk');
		$this->load->model('M_orders');
		$this->load->model('M_katalog');
		$this->load->model('M_rekening');
		$this->load->model('M_gateway');
		$this->load->library('upload');
		$this->load->library('ipaymu');
		$this->load->library('sendemail');

		// if (!$this->session->userdata('email_administrator')) {
		// 	redirect(base_url() . 'login');
        // }
	}	
	
	public function index()
	{
        $data['page_title'] = "Dashboard";
		$this->load->view('parts/v_header', $data);
		$this->load->view('katalog/v_katalog', $data);
		$this->load->view('parts/v_footer', $data);
	}

	public function trackOrder()
	{
        $data['page_title'] = "Track order";
		$this->load->view('parts/v_header', $data);
		$this->load->view('katalog/v_track_order', $data);
		$this->load->view('parts/v_footer', $data);
	}

	public function infoTracking()
	{
		$id_order = $this->input->post('id_order', TRUE);
		$no_whatsapp = $this->input->post('no_whatsapp', TRUE);

		$this->form_validation->set_rules('id_order', 'id_order', 'trim|required');
		$this->form_validation->set_rules('no_whatsapp', 'no_whatsapp', 'trim|required');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> '.validation_errors().'
			</div></div>');
			redirect(storeUrl() . "katalog/trackOrder/");
			exit;
		}else{
			
			// var_dump($id_order);
			// var_dump($no_whatsapp);

			$data['dataTracking'] = $this->M_katalog->getTrackingOrder($id_order, $no_whatsapp);
			if($data['dataTracking'] == null){
				$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
				<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Id order atau nomor hp tidak ditemukan!/div></div>');
				redirect(storeUrl() . "katalog/trackOrder/");
				exit;
			}
			$data['page_title'] = "Tracking order";
			$this->load->view('parts/v_header', $data);
			$this->load->view('katalog/v_info_tracking', $data);
			$this->load->view('parts/v_footer', $data);
		}
	}

	public function checkoutPage($id_produk)
	{
		
        $data['page_title'] = "Checkout Page";
        // $data['produkById'] = $this->M_produk->getProdukById($id_produk);
        $data['produkById'] = $id_produk;
		$this->load->view('parts/v_blank_header', $data);
		$this->load->view('katalog/v_checkout_page', $data);
		$this->load->view('parts/v_footer', $data);
	}

	public function checkoutSuccess()
	{
		// $decodeIdProduk = base64_decode($encodeIdProduk);
        $data['idOrder'] = base64_decode($_GET['cp']);
        $data['encodeIdOrder'] = $_GET['cp'];
        $data['page_title'] = "Checkout Success";
		$this->load->view('parts/v_blank_header', $data);
		$this->load->view('katalog/v_checkout_success', $data);
		$this->load->view('parts/v_footer', $data);
	}

	public function paymentConfirmation()
	{
		$decodeIdOrder = base64_decode($_GET['cp']);
        $data['idOrder'] = $decodeIdOrder;
        $data['decodeIdOrder'] = $_GET['cp'];
        $data['page_title'] = "Payment Confirmation";
		$this->load->view('parts/v_blank_header', $data);
		$this->load->view('katalog/v_payment_confirm', $data);
		$this->load->view('parts/v_footer', $data);
	}

	public function thankYouPage()
	{
		$decodeIdOrder = base64_decode($_GET['cp']);
        $data['idOrder'] = $decodeIdOrder;
        $data['page_title'] = "Thank You";
		$this->load->view('parts/v_blank_header', $data);
		$this->load->view('katalog/v_success_page', $data);
		$this->load->view('parts/v_footer', $data);
	}

	public function getOrderById($id){
		$getOrderById = $this->M_orders->getOrderById($id);

		echo $getOrderById;
    }

	public function getProdukById($id){
        $getProdukById = $this->M_produk->getProdukById($id);

		// echo $getProdukById;
		echo(json_encode($getProdukById));
    }

	public function getAllKota(){
		$getAllKota = $this->M_katalog->getAllKota();
		
		$html = "<option value=''>-- Silahkan pilih kota --</option>";
		foreach ($getAllKota as $key => $value) {
			$nama_kota = $value['nama_kabupaten_kota'];
			$html .= '<option value="'.$nama_kota.'">'.$nama_kota.'</option>';
		}
		echo(json_encode($html));
	}

	public function getAllProduk(){
		header('Access-Control-Allow-Origin: *');
		$getAllProduk = $this->M_produk->getAllProduk();
		echo(json_encode($getAllProduk));
	}

	public function getProdukByKeyword(){
		// header('Access-Control-Allow-Origin: *');
		$keyword = $this->input->post('keyword', TRUE);
		$getProdukByKeyword = $this->M_produk->getProdukByKeyword($keyword);
		echo(json_encode($getProdukByKeyword));
	}

	public function getAllRekening(){
		$allRekening = $this->M_rekening->getAllRekening();
		$html = "<option value=''>-- Silahkan pilih rekening --</option>";
		foreach ($allRekening as $key => $value) {
			$html .= "<option value=".$value['no_rekening'].">".$value['nama_bank']." - ".$value['nama_rekening']." - ".$value['no_rekening']."</option>";
		}
		echo(json_encode($html));
	}

	public function createOrder()
	{
		$today = date('Y-m-d h:i:s', time());
		$name = $this->input->post('name', TRUE);
		$no_whatsapp = $this->input->post('no_whatsapp', TRUE);
		$email = $this->input->post('email', TRUE);
		$kota = $this->input->post('kota', TRUE);
		$kode_unik = $this->input->post('kode_unik', TRUE);
		$total_order = $this->input->post('total_order', TRUE);
		$nama_produk = $this->input->post('nama_produk', TRUE);
		$id_produk = $this->input->post('id_produk', TRUE);
		// $post = $this->input->post();

		$this->form_validation->set_rules('name', 'Nama', 'trim|required');
		$this->form_validation->set_rules('no_whatsapp', 'No Whatsapp', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('kota', 'Kota', 'trim|required');
		$this->form_validation->set_rules('kode_unik', 'Kode unik', 'trim');
		$this->form_validation->set_rules('total_order', 'Total order', 'trim');
		$this->form_validation->set_rules('nama_produk', 'Nama produk', 'trim');
		$this->form_validation->set_rules('id_produk', 'Id produk', 'trim');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b>Pembelian anda belum berhasil!! '.validation_errors().'
			</div></div>');
			redirect(storeUrl() . "katalog/checkoutPage/$id_produk");
			exit;
		}else{
			
				$data = [
					'nama_pemesan' => $name,
					'no_whatsapp' => $no_whatsapp,
					'email' => $email,
					'kota' => $kota,
					'datetime_order' => $today,
					'kode_unik' => $kode_unik,
					'total_order' => $total_order,
					'status_pembayaran' => 'belum',
					'status_order' => 'pending',
					'nama_produk' => $nama_produk,
					'id_produk' => $id_produk,
					'status_cancel' => 'no',
				];

			$result = $this->M_orders->createOrder($data);
			$id_order = $this->db->insert_id();
			// $encodeIdProduk = base64_encode($id_order);
			$encodeJumlahTransfer = base64_encode($total_order);
			$encodeIdOrder = base64_encode($id_order);
			if($result){
				$linkPembayaran = storeUrl("katalog/checkoutSuccess/?cp=$encodeIdOrder");
				$data['email_customer'] = $email;
				$data['subject'] = 'yeay! Pesanan berhasil dibuat!';
				$data['content_email'] =  "<center>
				<td colspan='2' style='padding:30px;text-align:center'>
				<p><span style='font-size:20px'><span style='color:#024668'><strong>Pesanan dengan ID Order #$id_order berhasil dibuat!</strong></span></span></p>
				<p>Silahkan klik link di bawah ini untuk melakukan konfirmasi pembayaran!</p>
				<label style='border:1px solid #024668;font-size:30px;padding:10px;border-radius:15px;color:#024668'><strong> <a href='$linkPembayaran'>Ke halaman konfirmasi pembayaran</a></strong></label>
				<p>Terima Kasih,<br>Administrator Dropshipaja.com</p>
				</td>
				</center>";

				$emailsend =$this->sendemail->send($data);
				redirect(storeUrl("katalog/checkoutSuccess/?cp=$encodeIdOrder"));
			}else{
				$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Maaf pembelian anda belum berhasil. silahkan ulangi lagi!</strong></div>');
				redirect(storeUrl("katalog/checkoutPage/$id_produk"));
			}
		}
	}

	public function confirmPayment()
	{
		$today = date('Y-m-d h:i:s', time());
		$post = $this->input->post();
		$id_order = $this->input->post('id_order', TRUE);
		$nama_bank = $this->input->post('nama_bank', TRUE);
		$no_rek = $this->input->post('no_rek', TRUE);
		$atas_nama = $this->input->post('atas_nama', TRUE);
		$rekening_pembayaran = $this->input->post('rekening_pembayaran', TRUE);
		$tgl_transfer = $this->input->post('tgl_transfer', TRUE);
		$encodeIdOrder = base64_encode($id_order);
		$nama_bank = strtoupper($nama_bank);		

		$this->form_validation->set_rules('nama_bank', 'Nama Bank', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('no_rek', 'No rekening', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('atas_nama', 'Nama pemilik rekening', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('rekening_pembayaran', 'Rekening Tujuan', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('tgl_transfer', 'Tanggal transfer', 'trim|required');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Konfirmasi Pembayaran anda belum berhasil!! '.validation_errors().'
			</div></div>');
			redirect(storeUrl() . "katalog/paymentConfirmation/?cp=$encodeIdOrder");
			exit;
		}else{
			$allowed_types = "jpg|jpeg|png";
			$upload_path = "assets/uploaded_images";
			$resize_path = "assets/resize_images";
			$field = "bukti_transfer";
			$bukti_transfer = upload($allowed_types, $upload_path, $resize_path, $field);
			

			if($bukti_transfer['status'] == 'false'){
				$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
				<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Konfirmasi Pembayaran anda belum berhasil!! '.$bukti_transfer["message"].'
				</div></div>');
				redirect(storeUrl() . "katalog/paymentConfirmation/?cp=$encodeIdOrder");
				exit;
			}

			if($bukti_transfer['status'] == 'true'){
				$data = [
					'tgl_transfer' => $tgl_transfer,
					'datetime_konfirmasi_pembayaran' => $today,
					'no_rek' => $no_rek,
					'atas_nama' => $atas_nama,
					'dari_bank' => $nama_bank,
					'ke_bank' => $rekening_pembayaran,
					'bukti_transfer' => $bukti_transfer['thumbnail']
				];
			}

			if($bukti_transfer == null){
				$data = [
					'tgl_transfer' => $tgl_transfer,
					'datetime_konfirmasi_pembayaran' => $today,
					'no_rek' => $no_rek,
					'atas_nama' => $atas_nama,
					'dari_bank' => $nama_bank,
					'ke_bank' => $rekening_pembayaran,
				];
			}

			$result = $this->M_orders->confirmPayment($id_order, $data);
			// $id_order = $this->db->insert_id();
			if($result){
				redirect(storeUrl("katalog/thankYouPage/?cp=$encodeIdOrder"));
			}else{
				$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Maaf Konfirmasi pembayaran anda belum berhasil. silahkan ulangi lagi!</strong></div>');
				redirect(storeUrl("katalog/paymentConfirmation/?cp=$encodeIdOrder"));
			}
		}
	}

	public function createPaymentIpaymu()
	{
		$decodeIdOrder = base64_decode($_GET['cp']);
		$paymentMethod = $_GET['pm'];
		$paymentChannel = $_GET['pc'];

		$order = $this->M_katalog->getOrderById($decodeIdOrder);

		//production
		// $va = "1179005847683259";
		// $secret = "hTZEAtru8jzd0hi7PsaENKiEu2SpT0";

		// sandbox
		// $va = "1179005847683259";
		// $secret = "hTZEAtru8jzd0hi7PsaENKiEu2SpT0";
		$va = $order['va'];
		$secret = $order['api_key'];

		$product = array($order['nama_produk'], 'Kode Unik');
		$qty = array('1', '1');
		$price = array($order['total_order'], $order['kode_unik']);
		$description = array($order['keterangan_produk']);
		$referenceId = $order['id_order'];
		$buyerName = $order['nama_pemesan'];
		$buyerEmail = $order['email'];
		$buyerPhone = $order['no_whatsapp'];
		$notifyUrl = storeUrl('katalog/ipaymuThankyou');
		$returnUrl = storeUrl('katalog/thanksPageIpaymu');
		// $notifyUrl = storeUrl('katalog/ipaymuThankyou');
		// $returnUrl = storeUrl('katalog/createPaymentIpaymu');
		// $returnUrl = 'https://httpreq.com/flat-scene-1cokvrq0';
		// $notifyUrl = 'https://httpreq.com/flat-scene-1cokvrq0';
		$cancelUrl = storeUrl('katalog');

		// $cancelUrl = storeUrl('katalog');
		// $total_price = 50000;
		// $description = array('adf', 'adfa');
		// $referenceId = '6565';
		// $buyerName = 'customer';
		// $buyerEmail = 'customer@gmail.com';
		// $buyerPhone = '05661655';
		// $returnUrl = 'https://httpreq.com/flat-scene-1cokvrq0/record';
		// $notifyUrl = 'https://httpreq.com/flat-scene-1cokvrq0/record';
		// $cancelUrl = storeUrl('katalog');

		$data = array(
			"product" => $product,
			"qty" => $qty,
			"price" => $price,
			"returnUrl" => $returnUrl,
			"notifyUrl" => $notifyUrl,
			"cancelUrl" => $cancelUrl,
			"referenceId" => $referenceId,
			"buyerName" => $buyerName,
			"buyerEmail" => $buyerEmail,
			"buyerPhone" => $buyerPhone,
			"paymentChannel" => $paymentChannel,
		);

        $result = $this->ipaymu->requestPayment($va, $secret, $data, 'production');
		
		if($result->Message == 'success'){
			
			$dataRequest = [
				"key_ipaymu" => $secret,
				"id_order" => $order['id_order'],
				"total_price" => $result->Data->total_price,
				"pay_method" => $paymentMethod,
				"pay_channel" => $paymentChannel,
				"buyer_name" => $buyerName,
				"buyer_email" => $buyerEmail,
				"buyer_phone" => $buyerPhone,
				"session_id" => $result->Data->SessionID,
				"url" => $result->Data->Url,
			];

			$insertRequestIpaymu = $this->M_gateway->insertRequestIpaymu($dataRequest);

			if($insertRequestIpaymu){
				$id_order = $order['id_order'];
				$linkPembayaran = $result->Data->Url;
				$data['email_customer'] = $buyerEmail;
				$data['subject'] = 'Konfirmasi pembayaran';
				$data['content_email'] =  "<center>
				<td colspan='2' style='padding:30px;text-align:center'>
				<p><span style='font-size:20px'><span style='color:#024668'><strong>Pesanan dengan ID Order #$id_order berhasil dibuat!</strong></span></span></p>
				<p>Silahkan klik link di bawah ini untuk menyelesaikan pembayaran!</p>
				<label style='border:1px solid #024668;font-size:30px;padding:10px;border-radius:15px;color:#024668'><strong> <a href='$linkPembayaran'>Ke halaman pembayaran</a> </strong></label>
				<p>Terima Kasih,<br>Administrator Dropshipaja.com</p>
				</td>
				</center>";

				$emailsend =$this->sendemail->send($data);
				header('Location:' . $result->Data->Url);
			}else{
				echo 'Gagal insert log request!!';
			}
		}else{
			return $result;
		}
	}
	
	public function ipaymuThankyou(){
		$tgl=date('Y-m-d H:i:s');
		$trx_id = $_POST['trx_id'];
		$sid = $_POST['sid'];
		$status = $_POST['status']; 
		$via = $_POST['via'];
		// $status_code = $_POST['status_code'];
		// $is_escrow = $_POST['is_escrow'];
		// $url = $_POST['url'];
		// $channel = $_POST['channel'];
		// $reference_id = $_POST['reference_id'];
		// $date = $_POST['date'];
		// $amount = $_POST['amount'];
		// $total = $_POST['total'];
		// $fees = $_POST['fees'];

		// if(isset($_POST['status_code']))
		// {
		// 	$status_code = $_POST['status_code'];
		// }else{
		// 	$status_code = '';
		// }

		// if(isset($_POST['is_escrow']))
		// {
		// 	$is_escrow = $_POST['is_escrow'];
		// }else{
		// 	$is_escrow = '';
		// }

		// if(isset($_POST['url']))
		// {
		// 	$url = $_POST['url'];
		// }else{
		// 	$url = '';
		// }

		// if(isset($_POST['channel']))
		// {
		// 	$channel = $_POST['channel'];
		// }else{
		// 	$channel = '';
		// }

		// if(isset($_POST['reference_id']))
		// {
		// 	$reference_id = $_POST['reference_id'];
		// }else{
		// 	$reference_id = '';
		// }

		// if(isset($_POST['date']))
		// {
		// 	$date = $_POST['date'];
		// }else{
		// 	$date = '';
		// }

		// if(isset($_POST['amount']))
		// {
		// 	$amount = $_POST['amount'];
		// }else{
		// 	$amount = '';
		// }

		// if(isset($_POST['total']))
		// {
		// 	$total = $_POST['total'];
		// }else{
		// 	$total = '';
		// }

		// if(isset($_POST['fees']))
		// {
		// 	$fees = $_POST['fees'];
		// }else{
		// 	$fees = '';
		// }

		$respond_ipaymu = json_encode($_POST);

		$isi = 'Update dari notify';
		$ipaymu_log= [
			'id_trx_ipaymu'   => $trx_id,
			'tgl'      => $tgl,
			'isi'  => $isi,
			'respond_ipaymu' => $respond_ipaymu,
			
		];

		$this->M_gateway->insertLogIpaymu($ipaymu_log);


		if($status == 'berhasil'){
			// update status order
			$data = array(
				'status_pembayaran' => 'sudah',
				'status_order' => 'process',
				'tgl_transfer'      => $tgl,
				'datetime_konfirmasi_pembayaran'      => $tgl,
				'dari_bank' => $via,
				'ke_bank' => $via,
			);
			$result = $this->M_orders->setPaymentSuccess($_POST['reference_id'], $data);

			$updatedata = array(
				'trx_id' => $trx_id,
				'sid' => $sid,
				'status_ipaymu' => 'dibayar',
				'via' => $via,
			);
			$updateLogRequest = $this->M_orders->updateLogRequest($_POST['sid'], $updatedata);
			// header('Location:' . storeUrl('katalog/thanksPageIpaymu'));
			echo "success";
		}else{
			echo "failed";
		}
	}

	public function thanksPageIpaymu()
	{
        $data['page_title'] = "Pembayaran berhasil";
		$this->load->view('parts/v_blank_header', $data);
		$this->load->view('katalog/v_thanksPageIpaymu', $data);
		$this->load->view('parts/v_footer', $data);
	}

	public function failedPageIpaymu()
	{
        $data['page_title'] = "Pembayaran gagal";
		$this->load->view('parts/v_blank_header', $data);
		$this->load->view('katalog/v_failedPageIpaymu', $data);
		$this->load->view('parts/v_footer', $data);
	}
}