<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gateway extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('M_gateway');
		$this->load->library('Datatables');
		$this->load->library('ipaymu');

		if (!$this->session->userdata('email_administrator')) {
			redirect(base_url() . 'login');
        }
	}
	
	public function index()
	{
		$data['page_title'] = "Gateway";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('gateway/v_gateway', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function vAddGateway()
	{
        $data['page_title'] = "Add Payment Gateway";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('gateway/v_add_gateway', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function addPaymentGateway()
	{
		$payment_gateway = $this->input->post('payment_gateway', TRUE);
		$nama = $this->input->post('nama', TRUE);
		$va = $this->input->post('va', TRUE);
		$api_key = $this->input->post('api_key', TRUE);
		$layanan = $this->input->post('layanan[]', TRUE);
		$status = $this->input->post('status', TRUE);
		$status_level = $this->input->post('status_level', TRUE);
		$layanan_json = json_encode($layanan);

		$this->form_validation->set_rules('payment_gateway', 'payment_gateway', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('nama', 'nama', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('va', 'va', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('api_key', 'api_key', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('layanan[]', 'layanan', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('status', 'status', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('status_level', 'status_level', 'trim|required|min_length[2]');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Payment gateway gagal ditambahkan!! '.validation_errors().'
			</div></div>');
			redirect(base_url() . "gateway/vAddGateway");
			exit;
		}else{
			$data = [
				'nama' => $nama,
				'va' => $va,
				'api_key' => $api_key,
				'status' => $status,
				'status_level' => $status_level,
				'layanan' => $layanan_json,
				'payment_gateway' => $payment_gateway,
				'id_administrator' => $this->session->userdata('id_administrator'),
			];
	
			$result = $this->M_gateway->addPaymentGateway($data);

			if($result){
                $this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong> Payment gateway berhasil ditambahkan!!</strong></div>');
                redirect(base_url('gateway'));
            }else{
                $this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong> Payment gateway gagal ditambahkan!! silahkan coba lagi!!</strong></div>');
                redirect(base_url("gateway/vAddGateway"));
            }
			
		}
	}

	public function vEditGateway($id)
	{
        $data['page_title'] = "Edit Payment Gateway";
        $data['paymentGatewayById'] = $this->M_gateway->getPaymentGatewayById($id);
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('gateway/v_edit_gateway', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function editPaymentGateway()
	{
		$post = $this->input->post();
		$id = $this->input->post('id', TRUE);
		$payment_gateway = $this->input->post('payment_gateway', TRUE);
		$nama = $this->input->post('nama', TRUE);
		$va = $this->input->post('va', TRUE);
		$api_key = $this->input->post('api_key', TRUE);
		$layanan = $this->input->post('layanan[]', TRUE);
		$status = $this->input->post('status', TRUE);
		$status_level = $this->input->post('status_level', TRUE);
		$layanan_json = json_encode($layanan);

		$this->form_validation->set_rules('payment_gateway', 'payment_gateway', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('nama', 'nama', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('va', 'va', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('api_key', 'api_key', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('layanan[]', 'layanan', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('status', 'status', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('status_level', 'status_level', 'trim|required|min_length[2]');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Payment gateway gagal diedit!! '.validation_errors().'
			</div></div>');
			redirect(base_url() . "gateway/vEditGateway/$id");
			exit;
		}else{
			$data = [
				'nama' => $nama,
				'va' => $va,
				'api_key' => $api_key,
				'status' => $status,
				'status_level' => $status_level,
				'layanan' => $layanan_json,
				'payment_gateway' => $payment_gateway,
			];
	
			$result = $this->M_gateway->editPaymentGateway($id, $data);

			if($result){
                $this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong> Payment gateway berhasil diedit!!</strong></div>');
                redirect(base_url('gateway'));
            }else{
                $this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong> Payment gateway gagal diedit!! silahkan coba lagi!!</strong></div>');
                redirect(base_url("gateway/vEditGateway/$id"));
            }
			
		}
	}

	public function deletePaymentGateway($id)
	{
		$result = $this->M_gateway->deletePaymentGateway($id);

		if($result){
			$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Payment gateway berhasil didelete!!</strong></div>');
			redirect(base_url('gateway'));
		}else{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Payment gateway gagal didelete!! silahkan coba lagi!!</strong></div>');
			redirect(base_url('gateway'));
		}
	}

	public function getAllPaymentGatewayJson()
	{
		header('Content-Type: application/json');
		echo $this->M_gateway->getAllPaymentGatewayJson();
	}

	public function getPaymentGatewayById()
	{
		$id_gateway = $this->input->post("id_gateway", TRUE);
		$result = $this->M_gateway->getPaymentGatewayById($id_gateway);

		echo json_encode($result);
	}

	public function paymnetGatewayChecker()
	{
		$va = $this->input->post('va', TRUE);
		$secret = $this->input->post('secret', TRUE);
		
		$checkBalance = $this->ipaymu->checkBalance($va, $secret, 'sandbox');
		// var_dump($checkBalance);
		// exit;

		echo json_encode($checkBalance);
	}

	public function ApiKeyChecker()
	{
		$post = $this->input->post();

		

		$message = '';
		$status  = '';
		$data = [];
		if ($post['type'] == "ipaymu"){
			$key = "60D066C4-C1F2-4209-A506-BBB68000CC1A";
			if($post['passkey'] != ''){
				$url = 'https://my.ipaymu.com/api/saldo?key='.trim($post['passkey']).'';
				$ch  = curl_init($url);
				$data = array(
					"key"    =>$key
				  );
				$data_string = json_encode($data);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_VERBOSE, 0);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
				curl_setopt($ch, CURLOPT_TIMEOUT, 360);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				  'Content-Type: application/json',
				  'Content-Length: ' . strlen($data_string))
				);
				$res=curl_exec($ch);
				// var_dump($res);
				// exit;
				if (json_decode($res)->Status == 200){
					$message =  "✔ Valid Apikey";
					$status  = "success";
					$data = [
								'message' => $message,
								'status' => $status
							]; 
				} else{
					$message =  "❌ Wrong Apikey";
					$status  = "fail";
					$data = [
								'message' => $message,
								'status' => $status
							]; 
				}
			}else{
				$message =  "Apikey can't be empty";
				$status  = "fail";
				$data = [
							'message' => $message,
							'status' => $status
						]; 
			}
			echo json_encode($data);
		}
	}
}