<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		// Load form helper library
		$this->load->library('session');
		// $this->load->helper('form');
		// $this->load->library('form_validation');
		$this->load->model('M_login');

		
	}

    public function index()
	{
		if ($this->session->userdata('email_administrator')) {
			redirect(base_url());
		}
		$data['page_title'] = 'Login | Blog Project';

		// if($this->input->get('destination', TRUE)){
		// 	$destination = $this->input->get('destination', TRUE);
		// 	$data['destination'] = base_url("proses-login?destination=$destination");
		// }else{
		// 	$data['destination'] = base_url("proses-login");
		// }
		$this->load->view('login/v_login_header1', $data);
		$this->load->view('login/v_login', $data);
		$this->load->view('login/v_login_footer1', $data);
	}

	public function proses_login()
	{
		if ($this->session->userdata('email_administrator')) {
			redirect(base_url());
		}
		date_default_timezone_set('Asia/Jakarta');
    	$currentDateTime = date('Y-m-d H:i:s');
		$email_administrator = $this->input->post('email_administrator', TRUE);
		$password_administrator = $this->input->post('password_administrator', TRUE);
		$password_administrator = md5(md5($password_administrator));
		$checkLogin = $this->M_login->checkLogin($email_administrator, $password_administrator);
		if($checkLogin){
			$session_data = array(
				'email_administrator' => $checkLogin['email_administrator'],
				'nama_administrator' => $checkLogin['nama_administrator'],
				'id_administrator' => $checkLogin['id_administrator'],
			);
			$this->session->set_userdata($session_data);
			$lastlogin = $this->M_login->lastlogin($email_administrator, $currentDateTime);
			redirect(base_url().'Dashboard');
		}else{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
						<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Login gagal, pastikan email & password sudah benar !</div></div>');

			redirect(base_url() . 'login');
		}
	}

	public function vRegistration(){
		$data['page_title'] = 'Registration | Blog Project';
		$this->load->view('login/v_login_header1', $data);
		$this->load->view('login/v_register', $data);
		$this->load->view('login/v_login_footer1', $data);
	}

	public function registrationSuccess(){
		$data['page_title'] = 'Registration Success | Blog Project';
		$this->load->view('login/v_login_header1', $data);
		$this->load->view('login/v_success_page', $data);
		$this->load->view('login/v_login_footer1', $data);
	}

	public function registration()
	{
		date_default_timezone_set('Asia/Jakarta');
    	$currentDateTime = date('Y-m-d H:i:s');
		$email_administrator = $this->input->post('email_administrator', TRUE);
		$nama_administrator = $this->input->post('nama_administrator', TRUE);
		$telp_administrator = $this->input->post('telp_administrator', TRUE);
		$password_administrator = $this->input->post('password_administrator', TRUE);
		$password_confirmation = $this->input->post('password_confirmation', TRUE);
		$passwordHash = md5(md5($password_administrator));

		$this->form_validation->set_rules('email_administrator', 'Email', 'trim|required|valid_email|is_unique[tb_administrator.email_administrator]');
		$this->form_validation->set_rules('nama_administrator', 'Nama Administrator', 'trim|required|min_length[5]');
		$this->form_validation->set_rules('telp_administrator', 'No Telpon', 'trim|required|numeric|min_length[5]');
		$this->form_validation->set_rules('password_administrator', 'Password', 'trim|required|min_length[4]');
		$this->form_validation->set_rules('password_confirmation', 'Konfirmasi Password', 'trim|required|matches[password_administrator]');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Registration Failed! '.validation_errors().'
			</div></div>');

			redirect(base_url() . 'login/vRegistration');
			exit;
		}else{
			$data = [
				'email_administrator' => $email_administrator,
				'password_administrator' => $passwordHash,
				'nama_administrator' => $nama_administrator,
				'telp_administrator' => $telp_administrator,
				'datetime_join' => $currentDateTime,
				'status_administrator' => 'konfirmasi',
				'role_administrator' => 1,
			];
	
			$result = $this->M_login->register($data);

			if($result){
				redirect(base_url() . 'login/registrationSuccess');
			}else{
				redirect(base_url() . 'login/vRegistration');
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url() . 'login');
	}

}