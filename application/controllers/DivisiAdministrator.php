<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DivisiAdministrator extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('M_divisiadministrator');
		$this->load->library('Datatables');

		if (!$this->session->userdata('email_administrator')) {
			redirect(base_url() . 'login');
        }
	}
	
	public function index()
	{
        $data['page_title'] = "Divisi Administrator";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('divisi_administrator/v_divisi_administrator', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function vAddDivisiAdmin()
	{
        $data['page_title'] = "Divisi Administrator";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('divisi_administrator/v_add_divisi_administrator', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function addDivisiAdmin()
	{
		$nama_divisi = $this->input->post('nama_divisi', TRUE);
		$deskripsi = $this->input->post('deskripsi', TRUE);
		$id_menu = $this->input->post('id_menu', TRUE);

		$this->form_validation->set_rules('nama_divisi', 'Nama Divisi', 'trim|required|is_unique[tb_divisi_administrator.nama_divisi]|min_length[2]|max_length[20]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|min_length[5]');
		$this->form_validation->set_rules('id_menu', 'Id Menu', 'trim|required');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Tambah Divisi gagal! '.validation_errors().'
			</div></div>');

			redirect(base_url() . 'divisiAdministrator/vAddDivisiAdmin');
			exit;
		}else{
			$data = [
				'nama_divisi' => $nama_divisi,
				'deskripsi' => $deskripsi,
				'id_menu' => $id_menu,
			];
	
			$result = $this->M_divisiadministrator->addDivisiAdmin($data);

			if($result){
                $this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Data Divisi Administrator berhasil ditambahkan!!</strong></div>');
                redirect(base_url('divisiAdministrator'));
            }else{
                $this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Data Divisi Administrator gagal ditambahkan!! silahkan coba lagi!!</strong></div>');
                redirect(base_url("divisiAdministrator/vAddDivisiAdmin"));
            }
		}
	}

	public function vEditDivisiAdmin($id_divisi)
	{
        $data['divisiById'] = $this->M_divisiadministrator->getDivisiById($id_divisi);
        $data['page_title'] = "Divisi Administrator";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('divisi_administrator/v_edit_divisi_administrator', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function editDivisiAdmin()
	{
		$id_divisi = $this->input->post('id_divisi', TRUE);
		$nama_divisi = $this->input->post('nama_divisi', TRUE);
		$deskripsi = $this->input->post('deskripsi', TRUE);	
		$id_menu = $this->input->post('id_menu', TRUE);

		$this->form_validation->set_rules('nama_divisi', 'Nama Divisi', 'trim|required|min_length[2]|max_length[20]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|min_length[5]');
		$this->form_validation->set_rules('id_menu', 'Id Menu', 'trim|required');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Divisi gagal diedit!! '.validation_errors().'
			</div></div>');

			redirect(base_url() . "divisiAdministrator/vEditDivisiAdmin/$id_divisi");
			exit;
		}else{
			$data = [
				"nama_divisi" => $nama_divisi,
				"deskripsi" => $deskripsi,
				"id_menu" => $id_menu
			];
			
			$editDivisiAdmin = $this->M_divisiadministrator->editDivisiAdmin($id_divisi, $data);
			
			if($editDivisiAdmin){
				$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Divisi berhasil diedit!!</strong></div>');
				redirect(base_url('divisiAdministrator'));
			}else{
				$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Divisi gagal diedit!! silahkan coba lagi!!</strong></div>');
				redirect(base_url("divisiAdministrator/vEditDivisiAdmin/$id_divisi"));
			}
		}
	}

	public function getAllDivisiAdminJson()
	{
		header('Content-Type: application/json');
		echo $this->M_divisiadministrator->getAllDivisiAdminJson();
	}

	public function deleteDivisiAdmin($id_divisi)
	{
		$deleteDivisiAdmin = $this->M_divisiadministrator->deleteDivisiAdmin($id_divisi);

		if($deleteDivisiAdmin){
			$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Divisi berhasil didelete!!</strong></div>');
			redirect(base_url('divisiAdministrator'));
		}else{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Divisi gagal didelete!! silahkan coba lagi!!</strong></div>');
			redirect(base_url('divisiAdministrator'));
		}
	}
}
