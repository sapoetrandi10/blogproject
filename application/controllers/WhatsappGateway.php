<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WhatsappGateway extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('M_whatsappgateway');
		$this->load->library('Datatables');

		if (!$this->session->userdata('email_administrator')) {
			redirect(base_url() . 'login');
        }
	}
	
	public function index()
	{
        $data['page_title'] = "WhatsApp Gateway";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('whatsapp_gateway/v_wa_gateway', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function vAddWoowaAccount()
	{
        $data['page_title'] = "Add Woowa Account";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('whatsapp_gateway/v_add_woowa', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function vEditWoowaAccount($id)
	{
        $data['page_title'] = "Edit Woowa Account";
        $data['idWoowa'] = $id;
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('whatsapp_gateway/v_edit_woowa', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function vSettingFollowUp($id_produk)
	{
		$data['copywritingFuwa'] = $this->M_whatsappgateway->getCopywritingFuwaByIdProduk($id_produk);
		if($data['copywritingFuwa']){
			// form edit
			$data['actionUrl'] = "editCopywritingFuwa";
			$data['id_produk'] = $data['copywritingFuwa']['id_produk'];
		}else{
			// form add
			$data['actionUrl'] = "addCopywritingFuwa";
			$data['id_produk'] = $id_produk;
		}

		$data['page_title'] = "Setting follow up Woowa";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('whatsapp_gateway/v_setting_fu', $data);
		$this->load->view('parts/v_footer1', $data);
	}
	
	public function addCopywritingFuwa()
	{
		// $post = $this->input->post();
		$id_produk = $this->input->post('id_produk', TRUE);
		$auto_fu = $this->input->post('auto_fu', TRUE);
		$woowa_integration = $this->input->post('woowa_integration', TRUE);
		$day_fu1 = $this->input->post('day_fu1', TRUE);
		$hour_fu1 = $this->input->post('hour_fu1', TRUE);
		$minute_fu1 = $this->input->post('minute_fu1', TRUE);
		$tamplate_fuwa1 = $this->input->post('tamplate_fuwa1');
		$day_fu2 = $this->input->post('day_fu2', TRUE);
		$hour_fu2 = $this->input->post('hour_fu2', TRUE);
		$minute_fu2 = $this->input->post('minute_fu2', TRUE);
		$tamplate_fuwa2 = $this->input->post('tamplate_fuwa2');
		$day_fu3 = $this->input->post('day_fu3', TRUE);
		$hour_fu3 = $this->input->post('hour_fu3', TRUE);
		$minute_fu3 = $this->input->post('minute_fu3', TRUE);
		$tamplate_fuwa3 = $this->input->post('tamplate_fuwa3');
		$day_fu4 = $this->input->post('day_fu4', TRUE);
		$hour_fu4 = $this->input->post('hour_fu4', TRUE);
		$minute_fu4 = $this->input->post('minute_fu4', TRUE);
		$tamplate_fuwa4 = $this->input->post('tamplate_fuwa4');
		$day_fu_process = $this->input->post('day_fu_process', TRUE);
		$hour_fu_process = $this->input->post('hour_fu_process', TRUE);
		$minute_fu_process = $this->input->post('minute_fu_process', TRUE);
		$tamplate_fuwa_process = $this->input->post('tamplate_fuwa_process');
		$auto_fu_cancel = $this->input->post('auto_fu_cancel', TRUE);
		$tamplate_fuwa_cancel = $this->input->post('tamplate_fuwa_cancel');

		$this->form_validation->set_rules('id_produk', 'id produk', 'trim|required');
		$this->form_validation->set_rules('auto_fu', 'auto follow up', 'trim|required');
		$this->form_validation->set_rules('woowa_integration', 'woowa integration', 'trim|required');
		$this->form_validation->set_rules('day_fu1', 'day follow up 1', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('hour_fu1', 'hour follow up 1', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('minute_fu1', 'minute follow up 1', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('tamplate_fuwa1', 'tamplate follow up 1', 'trim');
		$this->form_validation->set_rules('day_fu2', 'day follow up 2', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('hour_fu2', 'hour follow up 2', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('minute_fu2', 'minute follow up 2', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('tamplate_fuwa2', 'tamplate follow up 2', 'trim');
		$this->form_validation->set_rules('day_fu3', 'day follow up 3', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('hour_fu3', 'hour follow up 3', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('minute_fu3', 'minute follow up 3', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('tamplate_fuwa3', 'tamplate follow up 3', 'trim');
		$this->form_validation->set_rules('day_fu4', 'day follow up 4', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('hour_fu4', 'hour follow up 4', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('minute_fu4', 'minute follow up 4', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('tamplate_fuwa4', 'tamplate follow up 4', 'trim');
		$this->form_validation->set_rules('day_fu_process', 'day follow up process', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('hour_fu_process', 'hour follow up process', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('minute_fu_process', 'minute follow up process', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('tamplate_fuwa_process', 'tamplate follow up process', 'trim');
		$this->form_validation->set_rules('auto_fu_cancel', 'auto follow up cancel', 'trim|required');
		$this->form_validation->set_rules('tamplate_fuwa_cancel', 'tamplate follow up process', 'trim');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Follow up gagal dibuat!! '.validation_errors().'
			</div></div>');
			redirect(base_url() . "whatsappGateway/vSettingFollowUp/$id_produk");
			exit;
		}else{

			$data = [
				'id_produk' => $id_produk,
				'auto_fu' => $auto_fu,
				'woowa_integration' => $woowa_integration,
				'day_fu1' => $day_fu1,
				'hour_fu1' => $hour_fu1,
				'minute_fu1' => $minute_fu1,
				'tamplate_fuwa1' => $tamplate_fuwa1,
				'day_fu2' => $day_fu2,
				'hour_fu2' => $hour_fu2,
				'minute_fu2' => $minute_fu2,
				'tamplate_fuwa2' => $tamplate_fuwa2,
				'day_fu3' => $day_fu3,
				'hour_fu3' => $hour_fu3,
				'minute_fu3' => $minute_fu3,
				'tamplate_fuwa3' => $tamplate_fuwa3,
				'day_fu4' => $day_fu4,
				'hour_fu4' => $hour_fu4,
				'minute_fu4' => $minute_fu4,
				'tamplate_fuwa4' => $tamplate_fuwa4,
				'day_fu_process' => $day_fu_process,
				'hour_fu_process' => $hour_fu_process,
				'minute_fu_process' => $minute_fu_process,
				'tamplate_fuwa_process' => $tamplate_fuwa_process,
				'auto_fu_cancel' => $auto_fu_cancel,
				'tamplate_fuwa_cancel' => $tamplate_fuwa_cancel,
			];
	
			$result = $this->M_whatsappgateway->addCopywritingFuwa($data);

			if($result){
                $this->session->set_flashdata('notif_fu', '<div class="alert alert-success text-center" role="alert"><strong> Follow up berhasil dibuat!!</strong></div>');
                redirect(base_url('whatsappGateway#fuwa_copywriting'));
            }else{
                $this->session->set_flashdata('notif_fu', '<div class="alert alert-danger text-center" role="alert"><strong> Follow up gagal dibuat!! silahkan coba lagi!!</strong></div>');
                redirect(base_url("whatsappGateway/vSettingFollowUp/$id_produk"));
            }
		}
	}

	public function editCopywritingFuwa()
	{
		// $post = $this->input->post();
		$id_produk = $this->input->post('id_produk', TRUE);
		$auto_fu = $this->input->post('auto_fu', TRUE);
		$woowa_integration = $this->input->post('woowa_integration', TRUE);
		$day_fu1 = $this->input->post('day_fu1', TRUE);
		$hour_fu1 = $this->input->post('hour_fu1', TRUE);
		$minute_fu1 = $this->input->post('minute_fu1', TRUE);
		$tamplate_fuwa1 = $this->input->post('tamplate_fuwa1');
		$day_fu2 = $this->input->post('day_fu2', TRUE);
		$hour_fu2 = $this->input->post('hour_fu2', TRUE);
		$minute_fu2 = $this->input->post('minute_fu2', TRUE);
		$tamplate_fuwa2 = $this->input->post('tamplate_fuwa2');
		$day_fu3 = $this->input->post('day_fu3', TRUE);
		$hour_fu3 = $this->input->post('hour_fu3', TRUE);
		$minute_fu3 = $this->input->post('minute_fu3', TRUE);
		$tamplate_fuwa3 = $this->input->post('tamplate_fuwa3');
		$day_fu4 = $this->input->post('day_fu4', TRUE);
		$hour_fu4 = $this->input->post('hour_fu4', TRUE);
		$minute_fu4 = $this->input->post('minute_fu4', TRUE);
		$tamplate_fuwa4 = $this->input->post('tamplate_fuwa4');
		$day_fu_process = $this->input->post('day_fu_process', TRUE);
		$hour_fu_process = $this->input->post('hour_fu_process', TRUE);
		$minute_fu_process = $this->input->post('minute_fu_process', TRUE);
		$tamplate_fuwa_process = $this->input->post('tamplate_fuwa_process');
		$auto_fu_cancel = $this->input->post('auto_fu_cancel', TRUE);
		$tamplate_fuwa_cancel = $this->input->post('tamplate_fuwa_cancel');

		$this->form_validation->set_rules('id_produk', 'id produk', 'trim|required');
		$this->form_validation->set_rules('auto_fu', 'auto follow up', 'trim|required');
		$this->form_validation->set_rules('woowa_integration', 'woowa integration', 'trim|required');
		$this->form_validation->set_rules('day_fu1', 'day follow up 1', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('hour_fu1', 'hour follow up 1', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('minute_fu1', 'minute follow up 1', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('tamplate_fuwa1', 'tamplate follow up 1', 'trim');
		$this->form_validation->set_rules('day_fu2', 'day follow up 2', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('hour_fu2', 'hour follow up 2', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('minute_fu2', 'minute follow up 2', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('tamplate_fuwa2', 'tamplate follow up 2', 'trim');
		$this->form_validation->set_rules('day_fu3', 'day follow up 3', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('hour_fu3', 'hour follow up 3', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('minute_fu3', 'minute follow up 3', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('tamplate_fuwa3', 'tamplate follow up 3', 'trim');
		$this->form_validation->set_rules('day_fu4', 'day follow up 4', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('hour_fu4', 'hour follow up 4', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('minute_fu4', 'minute follow up 4', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('tamplate_fuwa4', 'tamplate follow up 4', 'trim');
		$this->form_validation->set_rules('day_fu_process', 'day follow up process', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('hour_fu_process', 'hour follow up process', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('minute_fu_process', 'minute follow up process', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[60]');
		$this->form_validation->set_rules('tamplate_fuwa_process', 'tamplate follow up process', 'trim');
		$this->form_validation->set_rules('auto_fu_cancel', 'auto follow up cancel', 'trim|required');
		$this->form_validation->set_rules('tamplate_fuwa_cancel', 'tamplate follow up process', 'trim');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Follow up gagal diedit!! '.validation_errors().'
			</div></div>');
			redirect(base_url() . "whatsappGateway/vSettingFollowUp/$id_produk");
			exit;
		}else{

			$data = [
				'id_produk' => $id_produk,
				'auto_fu' => $auto_fu,
				'woowa_integration' => $woowa_integration,
				'day_fu1' => $day_fu1,
				'hour_fu1' => $hour_fu1,
				'minute_fu1' => $minute_fu1,
				'tamplate_fuwa1' => $tamplate_fuwa1,
				'day_fu2' => $day_fu2,
				'hour_fu2' => $hour_fu2,
				'minute_fu2' => $minute_fu2,
				'tamplate_fuwa2' => $tamplate_fuwa2,
				'day_fu3' => $day_fu3,
				'hour_fu3' => $hour_fu3,
				'minute_fu3' => $minute_fu3,
				'tamplate_fuwa3' => $tamplate_fuwa3,
				'day_fu4' => $day_fu4,
				'hour_fu4' => $hour_fu4,
				'minute_fu4' => $minute_fu4,
				'tamplate_fuwa4' => $tamplate_fuwa4,
				'day_fu_process' => $day_fu_process,
				'hour_fu_process' => $hour_fu_process,
				'minute_fu_process' => $minute_fu_process,
				'tamplate_fuwa_process' => $tamplate_fuwa_process,
				'auto_fu_cancel' => $auto_fu_cancel,
				'tamplate_fuwa_cancel' => $tamplate_fuwa_cancel,
			];
	
			$result = $this->M_whatsappgateway->editCopywritingFuwa($id_produk, $data);

			if($result){
                $this->session->set_flashdata('notif_fu', '<div class="alert alert-success text-center" role="alert"><strong> Follow up berhasil diedit!!</strong></div>');
                redirect(base_url('whatsappGateway#fuwa_copywriting'));
            }else{
                $this->session->set_flashdata('notif_fu', '<div class="alert alert-danger text-center" role="alert"><strong> Follow up gagal diedit!! silahkan coba lagi!!</strong></div>');
                redirect(base_url("whatsappGateway/vSettingFollowUp/$id_produk"));
            }
		}
	}

	public function getWoowaAccount()
	{
		$getWoowaAccount = $this->M_whatsappgateway->getWoowaAccount();
		
		$html = "<option value='off'>off</option>";
		foreach ($getWoowaAccount as $key => $value) {
			$id = $value['id'];
			$phone_number = $value['phone_number'];
			$html .= '<option value="'.$id.'">'.$phone_number.'</option>';
		}
		echo(json_encode($html));
	}


	public function getWoowaById()
	{
		$idWoowa = $this->input->post("idWoowa", TRUE);
		$result = $this->M_whatsappgateway->getWoowaById($idWoowa);

		echo json_encode($result);
	}

	public function editWoowaAccount()
	{
		$id = $this->input->post('id', TRUE);
		$phone_number = $this->input->post('phone_number', TRUE);
		$woowa_key = $this->input->post('woowa_key', TRUE);
		$id_adminsitrator = $this->session->userdata('id_administrator');

		$this->form_validation->set_rules('id', 'id', 'trim|required');
		$this->form_validation->set_rules('phone_number', 'phone_number', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('woowa_key', 'woowa_key', 'trim|required|min_length[2]');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Akun Woowa gagal diupdate!! '.validation_errors().'
			</div></div>');
			redirect(base_url() . "whatsappGateway/vEditWoowaAccount/$id");
			exit;
		}else{
			$data = [
				'phone_number' => $phone_number,
				'woowa_key' => $woowa_key,
				'id_administrator' => $id_adminsitrator,
			];
	
			$result = $this->M_whatsappgateway->editWoowaAccount($id, $data);

			if($result){
                $this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong> Akun Woowa berhasil diupdate!!</strong></div>');
                redirect(base_url('whatsappGateway'));
            }else{
                $this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong> Akun Woowa gagal diupdate!! silahkan coba lagi!!</strong></div>');
                redirect(base_url("whatsappGateway/vEditWoowaAccount/$id"));
            }
		}
	}
	
	public function addWoowaAccount()
	{
        $phone_number = $this->input->post('phone_number', TRUE);
		$woowa_key = $this->input->post('woowa_key', TRUE);
		$id_adminsitrator = $this->session->userdata('id_administrator');

		$this->form_validation->set_rules('phone_number', 'phone_number', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('woowa_key', 'woowa_key', 'trim|required|min_length[2]');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Akun Woowa gagal ditambahkan!! '.validation_errors().'
			</div></div>');
			redirect(base_url() . "whatsappGateway/vAddWoowaAccount");
			exit;
		}else{
			$data=[
				'phone_number' => $phone_number,
				'woowa_key' => $woowa_key,
				'id_administrator' => $id_adminsitrator,
			];

			$result = $this->M_whatsappgateway->addWoowaAccount($data);

			if($result){
                $this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong> Akun Woowa berhasil ditambahkan!!</strong></div>');
                redirect(base_url('whatsappGateway'));
            }else{
                $this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong> Akun Woowa gagal ditambahkan!! silahkan coba lagi!!</strong></div>');
                redirect(base_url("whatsappGateway/vAddWoowaAccount"));
            }
		}
	}

	public function woowaKeyChecker()
	{
		$phone_number = $this->input->post('phone_number', TRUE);
		$woowa_key = $this->input->post('woowa_key', TRUE);
		
		$woowa = $this->woowa->checkNumber($phone_number, $woowa_key);

		echo $woowa;
	}
	
	public function getAllWoowaAccountJson()
	{
		header('Content-Type: application/json');
		echo $this->M_whatsappgateway->getAllWoowaAccountJson();
	}

	public function deleteWoowaAccount($id)
	{
		$result = $this->M_whatsappgateway->deleteWoowaAccount($id);

		if($result){
			$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Akun Woowa berhasil didelete!!</strong></div>');
			redirect(base_url('whatsappGateway'));
		}else{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Akun Woowa gagal didelete!! silahkan coba lagi!!</strong></div>');
			redirect(base_url('whatsappGateway'));
		}
	}

	public function getAllProduk()
	{
		header('Content-Type: application/json');
		echo $this->M_whatsappgateway->getAllProduk();
	}
}
