<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('M_blog');
		$this->load->model('M_kategori');
		$this->load->library('Datatables');
		$this->load->library('upload');

		if (!$this->session->userdata('email_administrator')) {
			redirect(base_url() . 'login');
        }
	}
	
	public function index()
	{
		$data['allBlog'] = $this->M_blog->getAllBlog();
        $data['page_title'] = "Blog";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('blog/V_blog', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function vAddBlog()
	{
		$data['page_title'] = "Add Blog";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('blog/V_add_blog', $data);
		$this->load->view('parts/v_footer1', $data);
	}
	
	public function getAllKategori(){
		$allKategori = $this->M_kategori->getAllKategori();
		$html = "<option value=''>-- Silahkan Pilih Kategori --</option>";
		foreach ($allKategori as $key => $value) {
			$html .= "<option value=".$value['id'].">".$value['nama_kategori']."</option>";
		}
		echo(json_encode($html));
	}


	public function addBlog()
	{
		$date = date('Y-m-d h:i:s', time());
		$id_kategori = $this->input->post('id_kategori', TRUE);
		$judul_blog = $this->input->post('judul_blog', TRUE);
		$content_blog = $this->input->post('content_blog', FALSE);
		$view_count = $this->input->post('view_count', TRUE);
		$status_blog = $this->input->post('status_blog', TRUE);
		$blog_seo_url = $this->input->post('blog_seo_url', TRUE);
		$blog_meta_author = $this->input->post('blog_meta_author', TRUE);
		$blog_meta_keyword = $this->input->post('blog_meta_keyword', TRUE);
		$blog_og_title = $this->input->post('blog_og_title', TRUE);
		$blog_og_description = $this->input->post('blog_og_description', TRUE);
		$blog_terhapus = $this->input->post('blog_terhapus', TRUE);
		$blog_admin_last_edit = $this->input->post('blog_admin_last_edit', TRUE);

		$this->form_validation->set_rules('id_kategori', 'Id kategori', 'trim|required');
		$this->form_validation->set_rules('judul_blog', 'Judul blog', 'trim|required|min_length[2]|max_length[30]');
		$this->form_validation->set_rules('content_blog', 'Content blog', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('view_count', 'View count', 'trim|required');
		$this->form_validation->set_rules('status_blog', 'Status blog', 'trim|required');
		$this->form_validation->set_rules('blog_seo_url', 'Blog Seo Url', 'trim|required');
		$this->form_validation->set_rules('blog_meta_author', 'Blog meta author', 'trim|required');
		$this->form_validation->set_rules('blog_meta_keyword', 'Blog meta keyword', 'trim|required');
		$this->form_validation->set_rules('blog_og_title', 'Blog og title', 'trim|required');
		$this->form_validation->set_rules('blog_og_description', 'Blog og description', 'trim|required');
		$this->form_validation->set_rules('blog_terhapus', 'blog terhapus', 'trim|required');
		$this->form_validation->set_rules('blog_admin_last_edit', 'blog admin last edit', 'trim|required');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Blog gagal disimpan!! '.validation_errors().'
			</div></div>');
			redirect(base_url() . "blog/vAddBlog");
			exit;
		}else{

			$data = [
				"id_kategori" => $id_kategori,
				"judul_blog" => $judul_blog,
				"content_blog" => $content_blog,
				"datetime_publish" => $date,
				"datetime_last_edit" => $date,
				"view_count" => $view_count,
				"status_blog" => $status_blog,
				"blog_seo_url" => $blog_seo_url,
				"blog_meta_author" => $blog_meta_author,
				"blog_meta_keyword" => $blog_meta_keyword,
				"blog_og_title" => $blog_og_title,
				"blog_og_description" => $blog_og_description,
				"blog_terhapus" => $blog_terhapus,
				"blog_admin_last_edit" => $blog_admin_last_edit,
			];
	
			$allowed_types_ThumbnailBlog = "gif|jpg|jpeg|png";
			$upload_path_ThumbnailBlog = "assets/uploaded_images";
			$resize_path_ThumbnailBlog = "assets/resize_images";
			$field_ThumbnailBlog = "thumbnail_blog";
			$allowed_types_BlogOgImage = "gif|jpg|jpeg|png";
			$upload_path_BlogOgImage = "assets/uploaded_images";
			$resize_path_BlogOgImage = "assets/resize_images";
			$field_BlogOgImage = "blog_og_image";
	
			$thumbnail_blog = upload($allowed_types_ThumbnailBlog, $upload_path_ThumbnailBlog, $resize_path_ThumbnailBlog, $field_ThumbnailBlog);
	
			$blog_og_image = upload($allowed_types_BlogOgImage, $upload_path_BlogOgImage, $resize_path_BlogOgImage, $field_BlogOgImage);

			if($thumbnail_blog['status'] == 'true'){
				if(isset($thumbnail_blog['thumbnail'])){
					$data["thumbnail_blog"] =  $thumbnail_blog['thumbnail'];
				}
			}else if($thumbnail_blog['status'] == 'false'){
				$massage = $thumbnail_blog['message'];
				$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>'.$massage.'!!</strong></div>');
				redirect(base_url("blog/vAddBlog"));
				exit;
			}
			
			if($blog_og_image['status'] == 'true'){
				if(isset($blog_og_image['thumbnail'])){
					$data["blog_og_image"] =  $blog_og_image['thumbnail'];
				}
			}else if($blog_og_image['status'] == 'false'){
				$massage = $blog_og_image['message'];
				$this->session->set_flashdata('notif', "<div class='alert alert-danger text-center' role='alert'><strong>$massage !!</strong></div>");
				redirect(base_url("blog/vAddBlog"));
				exit;
			}
	
			$insertBlog = $this->M_blog->addBlog($data);
			if($insertBlog){
				$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Blog berhasil disimpan!!</strong></div>');
				redirect(base_url('blog'));
			}else{
				$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Blog gagal disimpan!! silahkan coba lagi!!</strong></div>');
				redirect(base_url("blog/vAddBlog"));
			}
		}
	}
	

	public function vEditBlog($id_blog)
	{
		$data['blogById'] = $this->M_blog->getBlogById($id_blog);
		$data['allKategori'] = $this->M_kategori->getAllKategori();
        $data['page_title'] = "Edit Blog";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('blog/V_edit_blog', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	
	public function editBlog()
	{
		$date = date('Y-m-d h:i:s', time());
		$id_blog = $this->input->post('id_blog', TRUE);
		$id_kategori = $this->input->post('id_kategori', TRUE);
		$judul_blog = $this->input->post('judul_blog', TRUE);
		$content_blog = $this->input->post('content_blog', FALSE);
		$view_count = $this->input->post('view_count', TRUE);
		$status_blog = $this->input->post('status_blog', TRUE);
		$blog_seo_url = $this->input->post('blog_seo_url', TRUE);
		$blog_meta_author = $this->input->post('blog_meta_author', TRUE);
		$blog_meta_keyword = $this->input->post('blog_meta_keyword', TRUE);
		$blog_og_title = $this->input->post('blog_og_title', TRUE);
		$blog_og_description = $this->input->post('blog_og_description', TRUE);
		$blog_terhapus = $this->input->post('blog_terhapus', TRUE);
		$blog_admin_last_edit = $this->input->post('blog_admin_last_edit', TRUE);

		$this->form_validation->set_rules('id_blog', 'Id blog', 'trim|required');
		$this->form_validation->set_rules('id_kategori', 'Id kategori', 'trim|required');
		$this->form_validation->set_rules('judul_blog', 'Judul blog', 'trim|required|min_length[2]|max_length[30]');
		$this->form_validation->set_rules('content_blog', 'Content blog', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('view_count', 'View count', 'trim|required');
		$this->form_validation->set_rules('status_blog', 'Status blog', 'trim|required');
		$this->form_validation->set_rules('blog_seo_url', 'Blog Seo Url', 'trim|required');
		$this->form_validation->set_rules('blog_meta_author', 'Blog meta author', 'trim|required');
		$this->form_validation->set_rules('blog_meta_keyword', 'Blog meta keyword', 'trim|required');
		$this->form_validation->set_rules('blog_og_title', 'Blog og title', 'trim|required');
		$this->form_validation->set_rules('blog_og_description', 'Blog og description', 'trim|required');
		$this->form_validation->set_rules('blog_terhapus', 'blog terhapus', 'trim|required');
		$this->form_validation->set_rules('blog_admin_last_edit', 'blog admin last edit', 'trim|required');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Blog gagal diedit!! '.validation_errors().'
			</div></div>');
			redirect(base_url() . "blog/vEditBlog/$id_blog");
			exit;
		}else{

			$data = [
				"id_kategori" => $id_kategori,
				"judul_blog" => $judul_blog,
				"content_blog" => $content_blog,
				"datetime_publish" => $date,
				"datetime_last_edit" => $date,
				"view_count" => $view_count,
				"status_blog" => $status_blog,
				"blog_seo_url" => $blog_seo_url,
				"blog_meta_author" => $blog_meta_author,
				"blog_meta_keyword" => $blog_meta_keyword,
				"blog_og_title" => $blog_og_title,
				"blog_og_description" => $blog_og_description,
				"blog_terhapus" => $blog_terhapus,
				"blog_admin_last_edit" => $blog_admin_last_edit,
			];
			
			$allowed_types_ThumbnailBlog = "gif|jpg|jpeg|png";
			$upload_path_ThumbnailBlog = "assets/uploaded_images";
			$resize_path_ThumbnailBlog = "assets/resize_images";
			$field_ThumbnailBlog = "thumbnail_blog";
			$allowed_types_BlogOgImage = "gif|jpg|jpeg|png";
			$upload_path_BlogOgImage = "assets/uploaded_images";
			$resize_path_BlogOgImage = "assets/resize_images";
			$field_BlogOgImage = "blog_og_image";

			$thumbnail_blog = upload($allowed_types_ThumbnailBlog, $upload_path_ThumbnailBlog, $resize_path_ThumbnailBlog, $field_ThumbnailBlog);
	
			$blog_og_image = upload($allowed_types_BlogOgImage, $upload_path_BlogOgImage, $resize_path_BlogOgImage, $field_BlogOgImage);
			
			if($thumbnail_blog['status'] == 'true'){
				if(isset($thumbnail_blog['thumbnail'])){
					$data["thumbnail_blog"] =  $thumbnail_blog['thumbnail'];
				}
			}else if($thumbnail_blog['status'] == 'false'){
				$massage = $thumbnail_blog['message'];
				$this->session->set_flashdata('notif', "<div class='alert alert-danger text-center' role='alert'><strong>$massage!!</strong></div>");
				redirect(base_url("blog/vEditBlog/$id_blog"));
				exit;
			}
			
			if($blog_og_image['status'] == 'true'){
				if(isset($blog_og_image['thumbnail'])){
					$data["blog_og_image"] =  $blog_og_image['thumbnail'];
				}
			}else if($blog_og_image['status'] == 'false'){
				$massage = $blog_og_image['message'];
				$this->session->set_flashdata('notif', "<div class='alert alert-danger text-center' role='alert'><strong>$massage!!</strong></div>");
				redirect(base_url("blog/vEditBlog/$id_blog"));
				exit;
			}
			
			$editBlog = $this->M_blog->editBlog($id_blog, $data);
			
			if($editBlog){
				$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Blog berhasil diupdate!!</strong></div>');
				redirect(base_url('blog'));
			}else{
				$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Blog gagal diedit!! silahkan coba lagi!!</strong></div>');
				redirect(base_url("blog/vEditBlog/$id_blog"));
			}
		}
	}

	public function deleteBlog($id_blog)
	{
		$deleteBlog = $this->M_blog->deleteBlog($id_blog);

		if($deleteBlog){
			$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Blog berhasil didelete!!</strong></div>');
			redirect(base_url('blog'));
		}else{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Blog gagal didelete!! silahkan coba lagi!!</strong></div>');
			redirect(base_url('blog'));
		}
	}

	public function getAllBlogJson()
	{
		header('Content-Type: application/json');
		echo $this->M_blog->getAllBlogJson();
	}
}
