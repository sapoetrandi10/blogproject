<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KategoriProduk extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_kategori_produk');
		$this->load->library('Datatables');
		$this->load->library('upload');

		if (!$this->session->userdata('email_administrator')) {
			redirect(base_url() . 'login');
        }
	}
	
	public function index()
	{
		// $data['allKategoriproduk'] = $this->M_kategori_produk->getAllKategoriproduk();
        $data['page_title'] = "Kategori produk";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('kategori_produk/v_kategori_produk', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function vAddKategoriproduk()
	{
		$data['page_title'] = "Add Kategori Produk";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('kategori_produk/v_add_kategori_produk');
		$this->load->view('parts/v_footer1', $data);
	}

	public function addKategoriproduk()
	{
		$nama_kategori_produk = $this->input->post('nama_kategori_produk', TRUE);

		$allowed_types = "jpg|jpeg|png";
		$upload_path = "assets/uploaded_images";
		$resize_path = "assets/resize_images";
		$field = "thumbnail_kategori";
		$thumbnail_kategori = upload($allowed_types, $upload_path, $resize_path, $field);

		$data = [
			"nama_kategori_produk" => $nama_kategori_produk,
			"thumbnail_kategori" => $thumbnail_kategori
		];
		
		if($thumbnail_kategori['status'] == 'false'){
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Kategori produk gagal disimpan!! '.$thumbnail_kategori["message"].'!!</strong></div>');
			redirect(base_url("kategoriProduk/vAddKategoriproduk"));
		}

		if($thumbnail_kategori == null){
			$data = [
				"nama_kategori_produk" => $nama_kategori_produk
			];
		}

		$insertKategori = $this->M_kategori_produk->addKategoriproduk($data);
		if($insertKategori){
			$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Kategori produk berhasil disimpan!!</strong></div>');
			redirect(base_url('kategoriProduk'));
		}else{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Kategori produk gagal disimpan!! silahkan coba lagi!!</strong></div>');
			redirect(base_url("kategoriProduk/vAddKategoriproduk"));
		}

	}

	public function vEditKategoriproduk($id_kategori)
	{
		$data['KategoriprodukById'] = $this->M_kategori_produk->getKategoriprodukById($id_kategori);
        $data['page_title'] = "Edit Kategori produk";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('kategori_produk/v_edit_kategori_produk', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function editKategoriproduk()
	{
		$id_kategori_produk = $this->input->post('id_kategori_produk', TRUE);
		$nama_kategori_produk = $this->input->post('nama_kategori_produk', TRUE);
		// $thumbnail_kategori = $this->input->post('thumbnail_kategori', TRUE);

		$allowed_types = "jpg|jpeg|png";
		$upload_path = "assets/uploaded_images";
		$resize_path = "assets/resize_images";
		$field = "thumbnail_kategori";
		$thumbnail_kategori = upload($allowed_types, $upload_path, $resize_path, $field);
		$data = [
			"nama_kategori_produk" => $nama_kategori_produk,
			"thumbnail_kategori" => $thumbnail_kategori
		];
		
		if($thumbnail_kategori['status'] == 'false'){
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Kategori produk gagal diedit!! '.$thumbnail_kategori["message"].'!!</strong></div>');
			redirect(base_url("kategoriProduk/vEditKategoriproduk/$id_kategori_produk"));
			exit;
		}

		if($thumbnail_kategori == null){
			$data = [
				"nama_kategori_produk" => $nama_kategori_produk
			];
		}
		
		$editKategoriproduk = $this->M_kategori_produk->editKategoriproduk($id_kategori_produk, $data);

		if($editKategoriproduk){
			$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Kategori produk berhasil diedit!!</strong></div>');
			redirect(base_url('kategoriProduk'));
		}else{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Kategori produk gagal diedit!! silahkan coba lagi!!</strong></div>');
			redirect(base_url("kategoriProduk/vEditKategoriproduk/$id_kategori_produk"));
		}
	}

	public function deleteKategoriproduk($id_kategori)
	{
		$deleteKategori = $this->M_kategori_produk->deleteKategoriproduk($id_kategori);

		if($deleteKategori){
			$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Kategori produk berhasil didelete!!</strong></div>');
			redirect(base_url('kategoriProduk'));
		}else{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Kategori produk gagal didelete!! silahkan coba lagi!!</strong></div>');
			redirect(base_url('kategoriProduk'));
		}
	}

	function getAllKategoriprodukJson()
	{
		header('Content-Type: application/json');
		echo $this->M_kategori_produk->getAllKategoriprodukJson();
	}
}
