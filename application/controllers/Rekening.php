<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekening extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('M_rekening');
		$this->load->library('Datatables');

		if (!$this->session->userdata('email_administrator')) {
			redirect(base_url() . 'login');
        }
	}
	
	public function index()
	{
        $data['page_title'] = "Rekening";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('rekening/v_rekening', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function vAddRekening()
	{
        $data['page_title'] = "Add Rekening";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('rekening/v_add_rekening', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function addRekening()
	{
		$no_rekening = $this->input->post('no_rekening', TRUE);
		$nama_rekening = $this->input->post('nama_rekening', TRUE);
		$nama_bank = $this->input->post('nama_bank', TRUE);
		$bank_id = $this->input->post('bank_id', TRUE);
		$moota_key = $this->input->post('moota_key', TRUE);

		$this->form_validation->set_rules('no_rekening', 'No Rekening', 'trim|required|numeric|is_unique[tb_rekening.no_rekening]');
		$this->form_validation->set_rules('nama_rekening', 'Nama Rekening', 'trim|required|min_length[2]|max_length[20]');
		$this->form_validation->set_rules('nama_bank', 'Nama Bank', 'trim|required');
		$this->form_validation->set_rules('bank_id', 'ID Bank', 'trim|required');
		$this->form_validation->set_rules('moota_key', 'Moota Key', 'trim|required');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Tambah Rekening gagal! '.validation_errors().'
			</div></div>');

			redirect(base_url() . 'rekening/vAddRekening');
			exit;
		}else{
			$data = [
				'no_rekening' => $no_rekening,
				'nama_rekening' => $nama_rekening,
				'nama_bank' => $nama_bank,
				'bank_id' => $bank_id,
				'moota_key' => $moota_key,
			];
	
			$result = $this->M_rekening->addRekening($data);

			if($result){
                $this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Data Rekening berhasil ditambahkan!!</strong></div>');
                redirect(base_url('rekening'));
            }else{
                $this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Data Rekening gagal ditambahkan!! silahkan coba lagi!!</strong></div>');
                redirect(base_url("rekening/vAddRekening"));
            }
		}
	}

	public function vEditRekening($id_rekening)
	{
		$data['rekeningById'] = $this->M_rekening->getRekeningById($id_rekening);
        $data['page_title'] = "Edit Rekening";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('rekening/v_edit_rekening', $data);
		$this->load->view('parts/v_footer1', $data);
	}

	public function editRekening()
	{
		$id_rekening = $this->input->post('id_rekening', TRUE);
		$no_rekening = $this->input->post('no_rekening', TRUE);
		$nama_rekening = $this->input->post('nama_rekening', TRUE);
		$nama_bank = $this->input->post('nama_bank', TRUE);
		$bank_id = $this->input->post('bank_id', TRUE);
		$moota_key = $this->input->post('moota_key', TRUE);

		$isRekeningExist = $this->M_rekening->isRekeningExist($id_rekening);
		if($isRekeningExist['no_rekening'] != $no_rekening){
			$this->form_validation->set_rules('no_rekening', 'No Rekening', 'trim|required|numeric|is_unique[tb_rekening.no_rekening]');
		}else{
			$this->form_validation->set_rules('no_rekening', 'No Rekening', 'trim|required|numeric');
		}
		
		$this->form_validation->set_rules('nama_rekening', 'Nama Rekening', 'trim|required|min_length[2]|max_length[20]');
		$this->form_validation->set_rules('nama_bank', 'Nama Bank', 'trim|required');
		$this->form_validation->set_rules('bank_id', 'ID Bank', 'trim|required');
		$this->form_validation->set_rules('moota_key', 'Moota Key', 'trim|required');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Edit Rekening gagal! '.validation_errors().'
			</div></div>');

			redirect(base_url() . "rekening/vEditRekening/$id_rekening");
			exit;
		}else{
			$data = [
				'no_rekening' => $no_rekening,
				'nama_rekening' => $nama_rekening,
				'nama_bank' => $nama_bank,
				'bank_id' => $bank_id,
				'moota_key' => $moota_key,
			];
	
			$result = $this->M_rekening->editRekening($id_rekening, $data);

			if($result){
                $this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Data Rekening berhasil diedit!!</strong></div>');
                redirect(base_url('rekening'));
            }else{
                $this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Data Rekening gagal diedit!! silahkan coba lagi!!</strong></div>');
                redirect(base_url("rekening"));
            }
		}
	}

	public function getAllRekeningJson()
	{
		header('Content-Type: application/json');
		echo $this->M_rekening->getAllRekeningJson();
	}

	public function deleteRekening($id_rekening)
	{
		$deleteRekening = $this->M_rekening->deleteRekening($id_rekening);

		if($deleteRekening){
			$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Data rekening berhasil didelete!!</strong></div>');
			redirect(base_url('rekening'));
		}else{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Data rekening gagal didelete!! silahkan coba lagi!!</strong></div>');
			redirect(base_url('rekening'));
		}
	}
}