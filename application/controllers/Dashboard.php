<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('M_blog');
		$this->load->model('M_kategori');
		$this->load->library('Datatables');

		if (!$this->session->userdata('email_administrator')) {
			redirect(base_url() . 'login');
        }
	}
	
	public function index()
	{
        $data['page_title'] = "Dashboard";
        $data['nama_admin'] = $this->session->userdata('nama_administrator');
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('dashboard/v_dashboard', $data);
		$this->load->view('parts/v_footer1', $data);
		// $this->load->view('parts/v_header', $data);
		// $this->load->view('parts/v_sidebar', $data);
		// $this->load->view('dashboard/v_dashboard', $data);
		// $this->load->view('parts/v_footer', $data);
	}
}