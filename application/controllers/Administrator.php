<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('M_administrator');
		$this->load->model('M_divisiadministrator');
		$this->load->library('Datatables');

		if (!$this->session->userdata('email_administrator')) {
			redirect(base_url() . 'login');
        }
	}
	
	public function index()
	{
        $data['page_title'] = "Administrator";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('administrator/v_administrator', $data);
		$this->load->view('parts/v_footer1', $data);
	}

    public function vAddAdmin()
	{
        $data['page_title'] = "Add Administrator";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('administrator/v_add_administrator', $data);
		$this->load->view('parts/v_footer1', $data);
    }

    public function addAdmin()
	{
        date_default_timezone_set('Asia/Jakarta');
    	$currentDateTime = date('Y-m-d H:i:s');
		$email_administrator = $this->input->post('email_administrator', TRUE);
		$nama_administrator = $this->input->post('nama_administrator', TRUE);
		$telp_administrator = $this->input->post('telp_administrator', TRUE);
		$status_administrator = $this->input->post('status_administrator', TRUE);
		$role_administrator = $this->input->post('role_administrator', TRUE);
		$password_administrator = '1234';
		$passwordHash = md5(md5($password_administrator));

		// $data = [
		// 	'email_administrator' => $email_administrator,
		// 	'password_administrator' => $passwordHash,
		// 	'nama_administrator' => $nama_administrator,
		// 	'telp_administrator' => $telp_administrator,
		// 	'datetime_join' => $currentDateTime,
		// 	'status_administrator' => $status_administrator,
		// 	'role_administrator' => $role_administrator
		// ];

		// var_dump($data);
		// exit;

		$this->form_validation->set_rules('email_administrator', 'Email', 'trim|required|valid_email|is_unique[tb_administrator.email_administrator]');
		$this->form_validation->set_rules('nama_administrator', 'Nama Administrator', 'trim|required|min_length[5]|max_length[20]');
		$this->form_validation->set_rules('telp_administrator', 'No Telpon', 'trim|required|numeric|min_length[5]');
		$this->form_validation->set_rules('status_administrator', 'Status Administrator', 'trim|required');
		$this->form_validation->set_rules('role_administrator', 'Role Administrator', 'trim|required');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Tambah Administrator gagal! '.validation_errors().'
			</div></div>');

			redirect(base_url() . 'administrator/vAddAdmin');
			exit;
		}else{
			$data = [
				'email_administrator' => $email_administrator,
				'password_administrator' => $passwordHash,
				'nama_administrator' => $nama_administrator,
				'telp_administrator' => $telp_administrator,
				'datetime_join' => $currentDateTime,
				'status_administrator' => $status_administrator,
				'role_administrator' => $role_administrator
			];
	
			$result = $this->M_administrator->addAdmin($data);

			if($result){
                $this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Data Administrator berhasil ditambahkan!!</strong></div>');
                redirect(base_url('administrator'));
            }else{
                $this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Data Administrator gagal ditambahkan!! silahkan coba lagi!!</strong></div>');
                redirect(base_url("administrator/vAddAdmin"));
            }
		}
    }

    public function vEditAdmin($id_administrator)
	{
        $data['adminById'] = $this->M_administrator->getAdminById($id_administrator);
        $data['page_title'] = "Edit Administrator";
		$this->load->view('parts/v_header1', $data);
		$this->load->view('parts/v_sidebar1', $data);
		$this->load->view('administrator/v_edit_administrator', $data);
		$this->load->view('parts/v_footer1', $data);
    }

	public function editAdmin()
	{
		$id_administrator = $this->input->post('id_administrator', TRUE);
		$email_administrator = $this->input->post('email_administrator', TRUE);
		$nama_administrator = $this->input->post('nama_administrator', TRUE);
		$telp_administrator = $this->input->post('telp_administrator', TRUE);
		$status_administrator = $this->input->post('status_administrator', TRUE);
		$role_administrator = $this->input->post('role_administrator', TRUE);

		$isEmailExist = $this->M_administrator->isEmailExist($id_administrator);
		if($isEmailExist['email_administrator'] != $email_administrator){
			$this->form_validation->set_rules('email_administrator', 'Email', 'trim|required|valid_email|is_unique[tb_administrator.email_administrator]');
		}else{
			$this->form_validation->set_rules('email_administrator', 'Email', 'trim|required|valid_email');
		}

		$this->form_validation->set_rules('nama_administrator', 'Nama Administrator', 'trim|required|min_length[5]|max_length[20]');
		$this->form_validation->set_rules('telp_administrator', 'No Telpon', 'trim|required|numeric|min_length[5]');
		$this->form_validation->set_rules('status_administrator', 'Status Administrator', 'trim|required');
		$this->form_validation->set_rules('role_administrator', 'Role Administrator', 'trim|required');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="margin-top: 3px">
			<div class="header"><b><i class="fa fa-exclamation-circle"></i> Oops</b> Gagal edit Administrator! '.validation_errors().'
			</div></div>');

			redirect(base_url() . "administrator/vEditAdmin/$id_administrator");
			exit;
		}else{
			$data = [
				'email_administrator' => $email_administrator,
				'nama_administrator' => $nama_administrator,
				'telp_administrator' => $telp_administrator,
				'status_administrator' => $status_administrator,
				'role_administrator' => $role_administrator
			];
	
			$result = $this->M_administrator->editAdmin($id_administrator, $data);

			if($result){
                $this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Data Administrator berhasil diedit!!</strong></div>');
                redirect(base_url('administrator'));
            }else{
                $this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Data Administrator gagal diedit!! silahkan coba lagi!!</strong></div>');
                redirect(base_url("administrator/vEditAdmin/$id_administrator"));
            }
		}
	}

    function getAllAdminJson()
	{
		header('Content-Type: application/json');
		echo $this->M_administrator->getAllAdminJson();
	}

    public function deleteAdmin($id_administrator)
	{
		$deleteAdmin = $this->M_administrator->deleteAdmin($id_administrator);

		if($deleteAdmin){
			$this->session->set_flashdata('notif', '<div class="alert alert-success text-center" role="alert"><strong>Admin berhasil didelete!!</strong></div>');
			redirect(base_url('administrator'));
		}else{
			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" role="alert"><strong>Admin gagal didelete!! silahkan coba lagi!!</strong></div>');
			redirect(base_url('administrator'));
		}
	}

	public function getAllDivisiAdmin(){
		$allDivisiAdmin = $this->M_divisiadministrator->getAllDivisiAdmin();
		$html = "<option value=''>-- Silahkan pilih role --</option>";
		foreach ($allDivisiAdmin as $key => $value) {
			$html .= "<option value=".$value['id_divisi'].">".$value['nama_divisi']."</option>";
		}
		echo(json_encode($html));
	}
}