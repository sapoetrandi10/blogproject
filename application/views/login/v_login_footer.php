
     
    <footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
      <p class="text-muted text-center text-md-left">Copyright © 2021 <a href="https://www.nobleui.com" target="_blank">NobleUI</a>. All rights reserved</p>
      <p class="text-muted text-center text-md-left mb-0 d-none d-md-block">Handcrafted With <i class="mb-1 text-primary ml-1 icon-small" data-feather="heart"></i></p>
    </footer>
  <!-- </div>
</div> -->
<!-- base js -->
  <script src="<?= base_url() ?>assets/js/app.js"></script>
  <script src="<?= base_url() ?>assets/assets/plugins/feather-icons/feather.min.js"></script>
  <script src="<?= base_url() ?>assets/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<!-- end base js -->

<!-- plugin js -->
  <script src="<?= base_url() ?>assets/assets/plugins/chartjs/Chart.min.js"></script>
  <script src="<?= base_url() ?>assets/assets/plugins/jquery.flot/jquery.flot.js"></script>
  <script src="<?= base_url() ?>assets/assets/plugins/jquery.flot/jquery.flot.resize.js"></script>
  <script src="<?= base_url() ?>assets/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
  <script src="<?= base_url() ?>assets/assets/plugins/apexcharts/apexcharts.min.js"></script>
  <script src="<?= base_url() ?>assets/assets/plugins/progressbar-js/progressbar.min.js"></script>
<!-- end plugin js -->

<!-- common js -->
  <script src="<?= base_url() ?>assets/assets/js/template.js"></script>
<!-- end common js -->

  <script src="<?= base_url() ?>assets/assets/js/dashboard.js"></script>
  <script src="<?= base_url() ?>assets/assets/js/datepicker.js"></script>

<!-- mylib -->

<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>