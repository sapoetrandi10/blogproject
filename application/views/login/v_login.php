<style>
    .wraper{
        min-height: 668px;
    }
</style>

<div class="wraper">
  <div class="row mt-4">
    <div class="col-md-4 offset-md-4 grid-margin stretch-card">
      <div class="card text-center bg-primary">
        <div class="card-body text-white">
          <h2>Administrator</h2>
        </div>
      </div>
    </div>
  </div>

  <div class="row text-center ">
    <div class="col-md-4 offset-md-4">
      <?php
      if ($this->session->flashdata('notif')) {
      echo $this->session->flashdata('notif');
      }
      ?>
    </div>
  </div>

  <div class="row">
    <div class="col-md-4 offset-md-4 grid-margin stretch-card">
      <div class="card  text-center">
        <div class="card-body">
          <h3 class="">LOGIN</h3>
          <form class="forms-sample text-left" action="<?= base_url('login/proses_login')?>" method="POST">
            <div class="form-group">
              <label for="email_administrator">Username</label>
              <input type="text" class="form-control" id="email_administrator" name="email_administrator" autocomplete="off" placeholder="Username">
            </div>
            <div class="form-group">
              <label for="password_administrator">Password</label>
              <input type="password" class="form-control" id="password_administrator" name="password_administrator" autocomplete="off" placeholder="Password">
            </div>
            <div class="form-group text-center">
              <button type="submit" class="btn btn-primary mr-2">Login</button>
            </div>
            <div class="form-group text-center">
              <p class="card-description"><a href="<?= base_url() ?>register">Create Account</a></p>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
            
            
        