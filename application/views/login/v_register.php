<style>
    .wraper{
        min-height: 668px;
    }
</style>

<div class="wraper">
    <div class="row mt-4">
        <div class="col-md-4 offset-md-4 grid-margin stretch-card">
            <div class="card text-center bg-primary">
                <div class="card-body text-white">
                    <h2>Administrator</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="row text-center ">
        <div class="col-md-4 offset-md-4">
            <?php
            if ($this->session->flashdata('notif')) {
            echo $this->session->flashdata('notif');
            }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 offset-md-4 grid-margin stretch-card">
            <div class="card  text-center">
                <div class="card-body">
                    <h3 class="">REGISTRATION</h3>
                    <form class="forms-sample text-left" action="<?= base_url('login/registration')?>" method="POST">
                        <div class="form-group">
                            <label for="email_administrator">Email</label>
                            <input type="email" class="form-control" id="email_administrator" name="email_administrator" autocomplete="off" placeholder="Email" value="<?= set_value('email_administrator'); ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="nama_administrator">Nama Administrator</label>
                            <input type="text" class="form-control" id="nama_administrator" name="nama_administrator" autocomplete="off" placeholder="Nama Administrator" value="<?= set_value('nama_administrator'); ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="telp_administrator">No Telpon</label>
                            <input type="number" class="form-control" id="telp_administrator" name="telp_administrator" autocomplete="off" placeholder="No Telpon" value="<?= set_value('telp_administrator'); ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="password_administrator">Password</label>
                            <input type="password" class="form-control" id="password_administrator" name="password_administrator" autocomplete="off" placeholder="Password" value="<?= set_value('password_administrator'); ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Konfirmasi Password</label>
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" autocomplete="off" placeholder="Konfirmasi Password" value="<?= set_value('password_confirmation'); ?>" required>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary mr-2">Register</button>
                            <a href="<?= base_url() ?>login">
                                <button type="button" class="btn btn-light">Cancel</button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>