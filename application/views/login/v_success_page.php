<style>
    .wraper{
        min-height: 668px;
    }
</style>

<div class="wraper">
    <div class="row mt-4">
        <div class="col-md-4 offset-md-4">
            <div class="card text-center">
                <div class="card-body">
                    <h3>Congratulation Your Registration has been success!</h3>
    
                    <div class="login-wrap">
                        <p>Click here to login !!!</p>
                        <a href="<?= base_url() ?>login">
                            <button class="btn btn-primary" type="button">Login</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>