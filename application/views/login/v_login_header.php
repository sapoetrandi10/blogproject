<!DOCTYPE html>
<!--
Template Name: NobleUI - Laravel Admin Dashboard Template
Author: NobleUI
Website: https://www.nobleui.com
Contact: nobleui123@gmail.com
License: You must have a valid license purchased only from https://themeforest.net/user/nobleui/portfolio/ in order to legally use the theme for your project.
-->
<html>
<head>
  <title><?= $page_title ?></title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- CSRF Token -->
  <meta name="_token" content="4NdPp35C2HhHpSML3vRmkIUfwcnqugM280A74zGF">
  
  <link rel="shortcut icon" href="favicon.ico">

  <!-- plugin css -->
  <link href="<?= base_url() ?>assets/assets/fonts/feather-font/css/iconfont.css" rel="stylesheet">
  <link href="<?= base_url() ?>assets/assets/plugins/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>assets/assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet">
  <!-- end plugin css -->

    <link href="<?= base_url() ?>assets/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">

  <!-- common css -->
  <link href="<?= base_url() ?>assets/css/app.css" rel="stylesheet">
  <!-- end common css -->


  <!-- mylib -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">

</head>
<body data-base-url="https://www.nobleui.com/laravel/template/light">

  <script src="<?= base_url() ?>assets/assets/js/spinner.js"></script>