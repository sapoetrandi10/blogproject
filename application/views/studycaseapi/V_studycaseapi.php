<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style type="text/css">
    .card{
        width: 200px;
        height: 200px;
        margin-right: 10px;
        margin-bottom: 10px;
    }
</style>

<div class="container text-center">
    <h1>Study Case Api</h1>
    <div class="box-panel text-left d-flex flex-row justify-content-around flex-wrap">
    </div>
</div>

<script>
    let url = "<?= base_url().'studycaseapi/getBarang'?>";
    $.ajax({
        url: url,
        // data: {
        //     tgl_awal: tgl_awal,
        //     tgl_akhir: tgl_akhir,
        // },
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            // console.log(data);
            let panel = "";
            $.each(data, function( index, value ) {
                panel += `
                    <div class="card">
                        <div class="card-body">
                            <h4>${value.nama_kategori}</h4>
                            <h5>${value.stock_per_kategori}</h5>
                        </div>
                    </div>
                `;
            });
            $("div.box-panel").html(panel);
        }
    });
</script>