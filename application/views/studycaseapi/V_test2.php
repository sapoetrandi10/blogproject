<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style type="text/css">
    .card{
        width: 200px;
        height: 210px;
        margin-right: 10px;
        margin-bottom: 10px;
    }
</style>

<div class="container text-center" >
    <h1>Study Case</h1>
    <div class="box-panel text-left d-flex flex-row justify-content-start flex-wrap">

    </div>
</div>

<!-- <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> -->
<!-- </body>
</html> -->
<script>
    let url = "<?= base_url().'studycaseapi/getTest2'?>";
    let panel = "";
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            // console.log(data);
            let panel = "";
            $.each(data, function( index, value ) {
                panel += `
                    <div class="card">
                        <div class="card-body">
                            <h4><strong>Id Kategori</strong></h4>
                            <p>${value.id_kategori}</p>
                            <h4><strong>Nama Kategori</strong></h4>
                            <p>${value.nama_kategori}</p>
                            <h4><strong>Stok</strong></h4>
                            <p>${value.stok}</p>
                        </div>
                    </div>
                `;
            });
            $("div.box-panel").html(panel);
        }
    });
</script>