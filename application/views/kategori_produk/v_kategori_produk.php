<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<div class="container">
    <div class="card  text-center">
        <div class="card-body">
            <h1>Table Kategori Produk</h1>
            <div class="btn-tambah mb-3 text-left">
                <a href="<?= base_url("kategoriProduk/vAddKategoriproduk/"); ?>">
                    <button type="button" class="btn btn-primary btn-round">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Kategori
                    </button>
                </a>
            </div>
            <div class="row text-center ">
                <div class="col-md-12">
                <?php
                if ($this->session->flashdata('notif')) {
                echo $this->session->flashdata('notif');
                }
                ?>
                </div>
            </div>
            <table class="table table-bordered" id="example">
                <thead>
                    <tr>
                    <th scope="col">No</th>
                    <th scope="col">ID Kategori</th>
                    <th scope="col">Nama Kategori</th>
                    <th scope="col">Thumbnail Kategori</th>
                    <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var table = $("#example").DataTable({
        initComplete: function() {
            var api = this.api();
            $('#mytable_filter input')
            .off('.DT')
            .on('input.DT', function() {
                api.search(this.value).draw();
            });
        },
        oLanguage: {
            sProcessing: "loading..."
        },
        processing: true,
        serverSide: true,
        "autoWidth": false,
        "scrollX": true,
        "scrollY": "300px",
        'ajax': {
            url: "<?= base_url().'kategoriProduk/getAllKategoriprodukJson'?>",
            type: "POST"
        },
        columns: [
        {"data": "id_kategori_produk"},
        {"data": "id_kategori_produk"},
        {"data": "nama_kategori_produk"},
        {"data": "thumbnail_kategori"},
        {"data": "id_kategori_produk", render : btn_action}
        ],
        rowCallback: function(row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            $('td:eq(0)', row).html();
        }
    });

    // order.dt search.dt
    //untuk memberi nomor di setiap row
    table.on( 'draw.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    
});

function btn_action(id_kategori_produk){
    return `
    <ul class="nav">
        <li class="nav-item ">
            <a href="<?= base_url("kategoriProduk/vEditKategoriproduk/"); ?>${id_kategori_produk}" class="nav-link text-success" onclick="return confirm('Anda yakin ingin mengedit kategori ini?')">
                <i class="mdi mdi-tooltip-edit">Edit</i>
            </a>
        </li>
        <li class="nav-item ">
            <a href="<?= base_url("kategoriProduk/deleteKategoriproduk/"); ?>${id_kategori_produk}" class="nav-link text-danger" onclick="return confirm('Anda yakin ingin menghapus kategori ini?')">
                <i class="mdi mdi-delete">Delete</i>
            </a>
        </li>
    </ul>
    `;
}
</script>