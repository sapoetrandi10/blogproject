<div class="container">
    <div class="card  text-center">
        <div class="card-body">
            <h3>Form Edit Kategori Produk</h3>
                <div class="row text-center mt-2">
                    <div class="col-md-12">
                    <?php
                    if ($this->session->flashdata('notif')) {
                    echo $this->session->flashdata('notif');
                    }
                    ?>
                    </div>
                </div>
                <form action="<?= base_url("kategoriProduk/editKategoriproduk/"); ?>" method="POST" enctype="multipart/form-data" class="text-left">
                    <input type="hidden" class="form-control" id="id_kategori_produk" name="id_kategori_produk" autocomplete="off" value="<?= $KategoriprodukById['id_kategori_produk'] ?>">
                    <div class="form-group">
                        <label for="nama_kategori_produk">Nama kategori produk</label>
                        <input type="text" class="form-control" id="nama_kategori_produk" name="nama_kategori_produk" autocomplete="off" placeholder="Nama Kategori produk" value="<?= $KategoriprodukById['nama_kategori_produk'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="thumbnail_kategori">Thumbnail kategori produk</label>
                        <input type="file" id="thumbnail_kategori" name="thumbnail_kategori" class="file-upload-default">
                        <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image" value="<?= $KategoriprodukById['thumbnail_kategori'] ?>">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                        </span>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="<?= base_url("kategoriProduk"); ?>">
                        <button type="button" class="btn btn-danger btn-round">
                            <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Cancel
                        </button>
                    </a>
                </form>
        </div>
    </div>
</div>
