<style>
    .select2-selection__arrow b{
        display:none !important;
    }

    .note{
        font-size: .700rem;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="card text-dark bg-white">
            <div class="card-header card-title mb-0">Follow Up Woowa</div>
            <div class="card-body">
                <form class="forms-sample" action="<?= base_url("whatsappGateway/$actionUrl/"); ?>" method="post">
                    <input type="hidden" class="form-control" id="id_produk" name="id_produk" autocomplete="off" placeholder="ID produk" maxlength="30" value="<?= $id_produk ?>" required>
                    <div class="form-group">
                        <label for="auto_fu">Auto Follow Up</label>
                        <select class="form-control mb-3" id="auto_fu" name="auto_fu" required>
                            <option value="yes" <?= (isset($copywritingFuwa['auto_fu']) && $copywritingFuwa['auto_fu'] == "yes") ? "selected" : ""?>>YES</option>
                            <option value="no" <?= (isset($copywritingFuwa['auto_fu']) && $copywritingFuwa['auto_fu'] == "no") ? "selected" : "" ?>>NO</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="woowa_integration">Pilih nomor whatsapp</label>
                        <select class="js-example-basic-single w-100" id="woowa_integration" name="woowa_integration">
                        </select>
                    </div>
                    <ul class="nav nav-tabs nav-tabs-line" id="lineTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="fuwa1-line-tab" data-toggle="tab" href="#fuwasatu" role="tab" aria-controls="fuwa1" aria-selected="true">Follow up 1</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="fuwa2-line-tab" data-toggle="tab" href="#fuwadua" role="tab" aria-controls="fuwa2" aria-selected="false">Follow up 2</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="fuwa3-line-tab" data-toggle="tab" href="#fuwatiga" role="tab" aria-controls="fuwa3" aria-selected="false">Follow up 3</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="fuwa4-line-tab" data-toggle="tab" href="#fuwaempat" role="tab" aria-controls="fuwa4" aria-selected="false">Follow up 4</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="process-line-tab" data-toggle="tab" href="#fuwaprocess" role="tab" aria-controls="fuwaprocess" aria-selected="false">Follow up process</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="cancel-line-tab" data-toggle="tab" href="#fuwacancel" role="tab" aria-controls="fuwacancel" aria-selected="false">Follow up cancel</a>
                        </li>
                    </ul>
                    <div class="tab-content mt-3" id="lineTabContent">
                        <div class="tab-pane fade show active" id="fuwasatu" role="tabpanel" aria-labelledby="fuwa1-line-tab">
                            <h4>Auto Follow Up After Order</h4>
                            <div class="copywritingfuwa">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="day_fu1">Hari</label>
                                            <input type="number" class="form-control" id="day_fu1" name="day_fu1" autocomplete="off" placeholder="Hari" min="0" value="<?= (isset($copywritingFuwa['day_fu1'])) ? $copywritingFuwa['day_fu1'] : "0"?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="hour_fu1">Jam</label>
                                            <input type="number" class="form-control" id="hour_fu1" name="hour_fu1" autocomplete="off" placeholder="jam" min="0" max="60" value="<?= (isset($copywritingFuwa['hour_fu1'])) ? $copywritingFuwa['hour_fu1'] : "0"?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="minute_fu1">Menit</label>
                                            <input type="number" class="form-control" id="minute_fu1" name="minute_fu1" autocomplete="off" placeholder="menit" min="0" max="60" value="<?= (isset($copywritingFuwa['minute_fu1'])) ? $copywritingFuwa['minute_fu1'] : "0"?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="exampleFormControlTextarea1">Example textarea</label> -->
                                <textarea class="form-control" id="tamplate_fuwa1" name="tamplate_fuwa1" rows="20" cols="50">
                                    <?= $copywritingFuwa['tamplate_fuwa1'] ?>
                                </textarea>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="fuwadua" role="tabpanel" aria-labelledby="fuwa2-line-tab">
                            <h4>Auto Follow Up After Order</h4>
                            <div class="copywritingfuwa">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="day_fu2">Hari</label>
                                            <input type="number" class="form-control" id="day_fu2" name="day_fu2" autocomplete="off" placeholder="Hari" min="0" value="<?= (isset($copywritingFuwa['day_fu2'])) ? $copywritingFuwa['day_fu2'] : "0"?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="hour_fu2">Jam</label>
                                            <input type="number" class="form-control" id="hour_fu2" name="hour_fu2" autocomplete="off" placeholder="jam" min="0" max="60" value="<?= (isset($copywritingFuwa['hour_fu2'])) ? $copywritingFuwa['hour_fu2'] : "0"?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="minute_fu2">Menit</label>
                                            <input type="number" class="form-control" id="minute_fu2" name="minute_fu2" autocomplete="off" placeholder="menit" min="0" max="60" value="<?= (isset($copywritingFuwa['minute_fu2'])) ? $copywritingFuwa['minute_fu2'] : "0"?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="exampleFormControlTextarea1">Example textarea</label> -->
                                <textarea class="form-control" id="tamplate_fuwa2" name="tamplate_fuwa2" rows="20" cols="50">
                                    <?= $copywritingFuwa['tamplate_fuwa2'] ?>
                                </textarea>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="fuwatiga" role="tabpanel" aria-labelledby="fuwa3-line-tab">
                            <h4>Auto Follow Up After Order</h4>
                            <div class="copywritingfuwa">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="day_fu3">Hari</label>
                                            <input type="number" class="form-control" id="day_fu3" name="day_fu3" autocomplete="off" placeholder="Hari" min="0" value="<?= (isset($copywritingFuwa['day_fu3'])) ? $copywritingFuwa['day_fu3'] : "0"?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="hour_fu3">Jam</label>
                                            <input type="number" class="form-control" id="hour_fu3" name="hour_fu3" autocomplete="off" placeholder="jam" min="0" max="60" value="<?= (isset($copywritingFuwa['hour_fu3'])) ? $copywritingFuwa['hour_fu3'] : "0"?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="minute_fu3">Menit</label>
                                            <input type="number" class="form-control" id="minute_fu3" name="minute_fu3" autocomplete="off" placeholder="menit" min="0" max="60" value="<?= (isset($copywritingFuwa['minute_fu3'])) ? $copywritingFuwa['minute_fu3'] : "0"?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="exampleFormControlTextarea1">Example textarea</label> -->
                                <textarea class="form-control" id="tamplate_fuwa3" name="tamplate_fuwa3" rows="20" cols="50">
                                    <?= $copywritingFuwa['tamplate_fuwa3'] ?>
                                </textarea>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="fuwaempat" role="tabpanel" aria-labelledby="fuwa4-line-tab">
                            <h4>Auto Follow Up After Order</h4>
                            <div class="copywritingfuwa">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="day_fu4">Hari</label>
                                            <input type="number" class="form-control" id="day_fu4" name="day_fu4" autocomplete="off" placeholder="Hari" min="0" value="<?= (isset($copywritingFuwa['day_fu4'])) ? $copywritingFuwa['day_fu4'] : "0"?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="hour_fu4">Jam</label>
                                            <input type="number" class="form-control" id="hour_fu4" name="hour_fu4" autocomplete="off" placeholder="jam" min="0" max="60" value="<?= (isset($copywritingFuwa['hour_fu4'])) ? $copywritingFuwa['hour_fu4'] : "0"?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="minute_fu4">Menit</label>
                                            <input type="number" class="form-control" id="minute_fu4" name="minute_fu4" autocomplete="off" placeholder="menit" min="0" max="60" value="<?= (isset($copywritingFuwa['minute_fu4'])) ? $copywritingFuwa['minute_fu4'] : "0"?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="exampleFormControlTextarea1">Example textarea</label> -->
                                <textarea class="form-control" id="tamplate_fuwa4" name="tamplate_fuwa4" rows="20" cols="50">
                                    <?= $copywritingFuwa['tamplate_fuwa4'] ?>
                                </textarea>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="fuwaprocess" role="tabpanel" aria-labelledby="process-line-tab">
                            <h4>Auto Follow Up After Order</h4>
                            <div class="copywritingfuwa">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="day_fu_process">Hari</label>
                                            <input type="number" class="form-control" id="day_fu_process" name="day_fu_process" autocomplete="off" placeholder="Hari" min="0" value="<?= (isset($copywritingFuwa['day_fu_process'])) ? $copywritingFuwa['day_fu_process'] : "0"?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="hour_fu_process">Jam</label>
                                            <input type="number" class="form-control" id="hour_fu_process" name="hour_fu_process" autocomplete="off" placeholder="jam" min="0" max="60" value="<?= (isset($copywritingFuwa['hour_fu_process'])) ? $copywritingFuwa['hour_fu_process'] : "0"?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="minute_fu_process">Menit</label>
                                            <input type="number" class="form-control" id="minute_fu_process" name="minute_fu_process" autocomplete="off" placeholder="menit" min="0" max="60" value="<?= (isset($copywritingFuwa['minute_fu_process'])) ? $copywritingFuwa['minute_fu_process'] : "0"?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="exampleFormControlTextarea1">Example textarea</label> -->
                                <textarea class="form-control" id="tamplate_fuwa_process" name="tamplate_fuwa_process" rows="20" cols="50">
                                    <?= $copywritingFuwa['tamplate_fuwa_process'] ?>
                                </textarea>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="fuwacancel" role="tabpanel" aria-labelledby="cancel-line-tab">
                            <div class="form-group">
                                <label for="auto_fu_cancel">Auto follow up after cancel</label>
                                <select class="form-control mb-3" id="auto_fu_cancel" name="auto_fu_cancel">
                                    <option value="yes" <?= (isset($copywritingFuwa['auto_fu_cancel']) && $copywritingFuwa['auto_fu_cancel'] == "yes") ? "selected" : "" ?>>YES</option>
                                    <option value="no" <?= (isset($copywritingFuwa['auto_fu_cancel']) && $copywritingFuwa['auto_fu_cancel'] == "no") ? "selected" : "" ?>>NO</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <!-- <label for="exampleFormControlTextarea1">Example textarea</label> -->
                                <textarea class="form-control" id="tamplate_fuwa_cancel" name="tamplate_fuwa_cancel" rows="20" cols="50">
                                    <?= $copywritingFuwa['tamplate_fuwa_cancel'] ?>
                                </textarea>
                            </div>
                        </div>
                        <p class="note text-warning">NOTE : * TEXT INSIDE "[[" "]]" IT'S YOUR SELECTED VARIABEL!!</p>
                        <p class="note text-danger">WARNING: * AFTER SELECT VARIABEL FROM PLACEHOLDER PLEASE NEVER CHANGE THE FORMAT!!!</p>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <a href="<?= base_url("whatsappGateway"); ?>" class="btn btn-danger">Cancel
                            </a>    
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        CKEDITOR.replace('tamplate_fuwa1',{
            filebrowserImageBrowseUrl : '<?php echo base_url('kcfinder/browse.php');?>',
            height: '400px'             
        });
        CKEDITOR.replace('tamplate_fuwa2',{
            filebrowserImageBrowseUrl : '<?php echo base_url('kcfinder/browse.php');?>',
            height: '400px'             
        });
        CKEDITOR.replace('tamplate_fuwa3',{
            filebrowserImageBrowseUrl : '<?php echo base_url('kcfinder/browse.php');?>',
            height: '400px'             
        });
        CKEDITOR.replace('tamplate_fuwa4',{
            filebrowserImageBrowseUrl : '<?php echo base_url('kcfinder/browse.php');?>',
            height: '400px'             
        });
        CKEDITOR.replace('tamplate_fuwa_process',{
            filebrowserImageBrowseUrl : '<?php echo base_url('kcfinder/browse.php');?>',
            height: '400px'             
        });
        CKEDITOR.replace('tamplate_fuwa_cancel',{
            filebrowserImageBrowseUrl : '<?php echo base_url('kcfinder/browse.php');?>',
            height: '400px'             
        });
    });

    $.ajax({
        url: "<?= base_url("whatsappGateway/getWoowaAccount"); ?>",
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('#woowa_integration').html(data);
            let copywritingFuwa = <?= ($copywritingFuwa !== null) ? 'true' : 'false'?>;
            // if edit
            if(copywritingFuwa){
                let woowa_integration = '<?= $copywritingFuwa['woowa_integration'] ?>';
            
                $('#woowa_integration option[value="'+woowa_integration+'"]').attr("selected","selected");
                $('#woowa_integration').select2().trigger('load');
            }            
            $('#woowa_integration').select2({
                width: '100%'
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("Terjadi kesalahan server");
        }
    });
</script>
