<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style>
</style>

<div class="container">
    <div class="card mb-3">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <!-- <i class="mdi mdi-wallet" style="font-size: 36px;"></i> -->
                    <h4>WhatsApp Gateway</h4>
                    <a href="#fuwa_copywriting">klik</a>
                </div>
                <div class="col-md-6  text-right">
                    <a href="<?= base_url("whatsappGateway/vAddWoowaAccount"); ?>">
                        <button type="button" class="btn btn-primary text-white btn-md btn-round">
                            <i class="mdi mdi-plus">Add Woowa Account</i>
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row text-center ">
        <div class="col-md-12">
            <?php if ($this->session->flashdata('notif')) { echo $this->session->flashdata('notif');}?>
        </div>
    </div>

    <div class="card text-dark mb-5">
        <!-- <div class="card-header bg-warning"><strong>Store Integration</strong></div> -->
        <div class="card-body">
            <div class="table-responsive pt-3">
                <table class="table table-bordered table-hover text-center" id="example">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Phone Number</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="card mb-3" id="fuwa_copywriting">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <h4>Setting Copywriting Product For Woowa Follow Up Automation</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="row text-center ">
        <div class="col-md-12">
            <?php if ($this->session->flashdata('notif_fu')) { echo $this->session->flashdata('notif_fu');}?>
        </div>
    </div>

    <div class="card text-dark">
        <!-- <div class="card-header bg-warning"><strong>Store Integration</strong></div> -->
        <div class="card-body">
            <div class="table-responsive pt-3">
                <table class="table table-bordered table-hover text-center" id="produkTable">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>ID Produk</th>
                            <th>Nama Produk</th>
                            <th>Status Produk</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <a href="<?= base_url("integrations"); ?>">
        <button type="button" class="btn btn-danger text-white btn-md btn-round mt-3">
            <i class="mdi mdi-arrow-left"></i>
            back
        </button>
    </a>
</div>

<script>
$(document).ready(function(){
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var table = $("#example").DataTable({
        initComplete: function() {
            var api = this.api();
            $('#mytable_filter input')
            .off('.DT')
            .on('input.DT', function() {
                api.search(this.value).draw();
            });
        },
        oLanguage: {
            sProcessing: "loading..."
        },
        processing: true,
        serverSide: true,
        // "scrollY": true,
        'ajax': {
            url: "<?= base_url().'whatsappGateway/getAllWoowaAccountJson'?>",
            type: "POST"
        },
        columns: [
        {"data": "id"},
        {"data": "phone_number"},
        {"data": "id", render : btn_action}
        ],
        rowCallback: function(row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            $('td:eq(0)', row).html();
        }
    });

    // order.dt search.dt
    //untuk memberi nomor di setiap row
    table.on( 'draw.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    var produkTable = $("#produkTable").DataTable({
        initComplete: function() {
            var api = this.api();
            $('#mytable_filter input')
            .off('.DT')
            .on('input.DT', function() {
                api.search(this.value).draw();
            });
        },
        oLanguage: {
            sProcessing: "loading..."
        },
        processing: true,
        serverSide: true,
        // "scrollY": true,
        'ajax': {
            url: "<?= base_url().'whatsappGateway/getAllProduk'?>",
            type: "POST"
        },
        columns: [
            {"data": "id_produk"},
            {"data": "id_produk"},
            {"data": "nama_produk"},
            {"data": "status_produk"},
            {"data": "id_produk", render : btn_produk}
        ],
        rowCallback: function(row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            $('td:eq(0)', row).html();
        }
    });

    //untuk memberi nomor di setiap row
    produkTable.on( 'draw.dt', function () {
        produkTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
});

function btn_action(id){
    return `
    <ul class="nav">
        <li class="nav-item">
            <a href="<?= base_url("whatsappGateway/vEditWoowaAccount/"); ?>${id}" class="nav-link text-success" onclick="return confirm('Anda yakin ingin mengedit payment gateway ini?')">
                <i class="mdi mdi-tooltip-edit"> Edit</i>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?= base_url("whatsappGateway/deleteWoowaAccount/"); ?>${id}" class="nav-link text-danger" onclick="return confirm('Anda yakin ingin menghapus woowa account ini?')">
                <i class="mdi mdi-delete"> Delete</i>
            </a>
        </li>
    </ul>
    `;
}

function btn_produk(id_produk){
    return `
    <ul class="nav">
        <li class="nav-item">
            <a href="<?= base_url("whatsappGateway/vSettingFollowUp/"); ?>${id_produk}" class="nav-link text-success" onclick="return confirm('Anda yakin ingin setting produk ini?')">
                <i class="mdi mdi-tooltip-edit"> Setting</i>
            </a>
        </li>
    </ul>
    `;
}
</script>
