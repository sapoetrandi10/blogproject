<style>
    .note{
        font-size: .700rem;
    }
</style>

<div class="container">
    <div class="card text-dark">
        <div class="card-header bg-primary text-white text-center"><strong>Form Add Woowa Account</strong></div>
        <div class="card-body">
            <div class="row text-center ">
                <div class="col-md-12">
                    <?php if ($this->session->flashdata('notif')) { echo $this->session->flashdata('notif');}?>
                </div>
            </div>
            <form class="forms-sample" action="<?= base_url('whatsappGateway/addWoowaAccount') ?>" method="POST">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="phone_number">Woowa Phone Number</label>
                            <input type="number" class="form-control" id="phone_number" name="phone_number" autocomplete="off" placeholder="Woowa Number" required>
                        </div>
                        <div class="form-group">
                            <label for="woowa_key">Woowa Key</label>
                            <input type="text" class="form-control" id="woowa_key" name="woowa_key" autocomplete="off" placeholder="Woowa Key" required>
                            <div class="spinner">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary mr-2 btnSubmit">Submit</button>
                <a href="<?= base_url("whatsappGateway"); ?>">
                    <button type="button" class="btn btn-danger text-white btn-md btn-round">
                        <i class="mdi mdi-arrow-left" style="font-size: 12px;"></i> cancel
                    </button>
                </a>
            </form>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $('.btnSubmit').hide();
    $('#woowa_key, #phone_number').bind('input', function(){
        let phoneNumber = $("#phone_number").val();
        let woowaKey = $("#woowa_key").val();
        checkApiKey(phoneNumber, woowaKey);
    });
});

function checkApiKey(phone_number, woowa_key){
    let checker = `
    <div class="spinner-border text-secondary mt-1" role="status" style="width: 17px; height: 17px;">
        <span class="sr-only">Loading...</span>
    </div>
    <small>checking...</small>
    `;
    $(".spinner").html(checker);

    $.ajax({
        "url": "<?php echo base_url('whatsappGateway/woowaKeyChecker') ?>",
        "type": "POST",
        "data" : {
            phone_number : phone_number,
            woowa_key : woowa_key
        },
        dataType : 'json',
        success : function(data){
            if(data.status == 'success'){
                if(data.message == 'exists'){
                    checker = `
                        <i class="mdi mdi-check text-success"></i>
                    `;
                    $('.btnSubmit').show();
                }else{
                    checker = `
                        <small class="text-warning">Nomor anda tidak terdaftar sebagai akun whatsapp!</small>
                    `;
                    $('.btnSubmit').hide();
                }
                $(".spinner").html(checker);
            }else{
                checker = `
                    <small class="text-danger">${data.message}</small>
                `;
                $('.btnSubmit').hide();
                $(".spinner").html(checker);
            }
        },
        error: function(data){
            console.log(data);
        } 
    })
}
</script>
