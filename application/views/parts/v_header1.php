<!DOCTYPE html>
<!--
Template Name: NobleUI - Laravel Admin Dashboard Template
Author: NobleUI
Website: https://www.nobleui.com
Contact: nobleui123@gmail.com
License: You must have a valid license purchased only from https://themeforest.net/user/nobleui/portfolio/ in order to legally use the theme for your project.
-->
<html>
<head>
  <title><?= $page_title ?></title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- CSRF Token -->
  <meta name="_token" content="4NdPp35C2HhHpSML3vRmkIUfwcnqugM280A74zGF">
  
  <link rel="shortcut icon" href="<?= base_url(); ?>favicon.ico">
  <link href="<?= base_url(); ?>assets/assets/plugins/%40mdi/css/materialdesignicons.min.css" rel="stylesheet">

  <!-- plugin css -->
  <link href="<?= base_url(); ?>assets/assets/fonts/feather-font/css/iconfont.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/assets/plugins/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/assets/plugins/select2/select2.min.css" rel="stylesheet">
  <!-- end plugin css -->

  
  <!-- common css -->
  <link href="<?= base_url(); ?>assets/css/app.css" rel="stylesheet">
  <!-- end common css -->

  <!-- myLib -->
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous"> -->
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <script type="text/javascript" src="<?= base_url(); ?>ckeditor/ckeditor.js"></script>

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">

  </head>
<body data-base-url="https://www.nobleui.com/laravel/template/light">

  <script src="<?= base_url(); ?>assets/assets/js/spinner.js"></script>

  <div class="main-wrapper" id="app">
  
