<!DOCTYPE html>
<!--
Template Name: NobleUI - Laravel Admin Dashboard Template
Author: NobleUI
Website: https://www.nobleui.com
Contact: nobleui123@gmail.com
License: You must have a valid license purchased only from https://themeforest.net/user/nobleui/portfolio/ in order to legally use the theme for your project.
-->
<html>
<head>
  <title><?= $page_title ?></title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- CSRF Token -->
  <meta name="_token" content="4NdPp35C2HhHpSML3vRmkIUfwcnqugM280A74zGF">
  
  <link rel="shortcut icon" href="<?= storeUrl(); ?>/assets/favicon.ico">
  <link href="<?= storeUrl(); ?>assets/assets/plugins/%40mdi/css/materialdesignicons.min.css" rel="stylesheet">

  <!-- plugin css -->
  <link href="<?= storeUrl(); ?>assets/assets/fonts/feather-font/css/iconfont.css" rel="stylesheet">
  <link href="<?= storeUrl(); ?>assets/assets/plugins/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
  <link href="<?= storeUrl(); ?>assets/assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet">
  <link href="<?= storeUrl(); ?>assets/assets/plugins/select2/select2.min.css" rel="stylesheet">
  <link href="<?= storeUrl(); ?>assets/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet">
  <!-- end plugin css -->

  
  <!-- common css -->
  <link href="<?= storeUrl(); ?>/assets/css/app.css" rel="stylesheet">
  <!-- end common css -->

  <!-- myLib -->
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  </head>
<body data-base-url="https://www.nobleui.com/laravel/template/light">

  <script src="<?= storeUrl(); ?>/assets/assets/js/spinner.js"></script>

  <div class="main-wrapper" id="app">
    <div class="page-wrapper full-page">
      <div class="page-content">