<!DOCTYPE html>
<!--
Template Name: NobleUI - Laravel Admin Dashboard Template
Author: NobleUI
Website: https://www.nobleui.com
Contact: nobleui123@gmail.com
License: You must have a valid license purchased only from https://themeforest.net/user/nobleui/portfolio/ in order to legally use the theme for your project.
-->
<html>
<head>
  <title><?= $page_title ?></title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- CSRF Token -->
  <meta name="_token" content="4NdPp35C2HhHpSML3vRmkIUfwcnqugM280A74zGF">
  
  <link rel="shortcut icon" href="<?= storeUrl(); ?>/assets/favicon.ico">
  <link href="<?= storeUrl(); ?>assets/assets/plugins/%40mdi/css/materialdesignicons.min.css" rel="stylesheet">

  <!-- plugin css -->
  <link href="<?= storeUrl(); ?>assets/assets/fonts/feather-font/css/iconfont.css" rel="stylesheet">
  <link href="<?= storeUrl(); ?>assets/assets/plugins/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
  <link href="<?= storeUrl(); ?>assets/assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet">
  <link href="<?= storeUrl(); ?>assets/assets/plugins/select2/select2.min.css" rel="stylesheet">
  <!-- end plugin css -->

  <link href="<?= storeUrl(); ?>/assets/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet">


  
  <!-- common css -->
  <link href="<?= storeUrl(); ?>/assets/css/app.css" rel="stylesheet">
  <!-- end common css -->

  <!-- myLib -->
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <script type="text/javascript" src="<?= storeUrl(); ?>ckeditor/ckeditor.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
  <style>
    

    nav.navbarCatalog {
        position:absolute;
        left:0px;
        right:0px;
        width: 100%;
        background-color: #FFCC1D;
    }
  </style>
  </head>
<body data-base-url="https://www.nobleui.com/laravel/template/light">

  <script src="<?= storeUrl(); ?>/assets/assets/js/spinner.js"></script>

  <div class="main-wrapper" id="app">
    <div class="page-wrapper full-page">
      <nav class="navbar navbar-expand-lg navbar-light fixed-top navbarCatalog">
        <div class="container">
          <a class="navbar-brand" href="<?= storeUrl("katalog"); ?>"><b>STORE</b></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
  
          <div class="collapse navbar-collapse" id="navbarSupportedContent1">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" href="<?= storeUrl("katalog"); ?>">Home <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?= storeUrl("katalog/trackOrder"); ?>">Track order</a>
              </li>
              <!-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Dropdown
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Something else here</a>
                </div>
              </li> -->
            </ul>
            <form class="form-inline my-2 my-lg-0">
              <input class="form-control mr-sm-2" type="search" placeholder="Search" id="search" aria-label="Search">
              <button class="btn btn-outline-success my-2 my-sm-0 searchBtn" type="button">Search</button>
            </form>
          </div>
        </div>
      </nav>
      <div class="page-content">