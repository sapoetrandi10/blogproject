<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style>
    .produk{
        width: 300px;
        /* height: 300px; */
    }

    .actionBtn{
        width: 100%;
        /* width: 123px; */
        /* height: 300px; */
    }
</style>

<div class="container text-center">
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body text-left">
                    <h4 class="card-title">Daftar Produk</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <a class="btn btn-primary" href="<?= base_url("Produk/vAddProduk/"); ?>" role="button"><i class="mdi mdi-database-plus">Tambah produk</i></a>
                        </div>
                        <div class="col-md-4 offset-md-2">
                            <input type="text" class="form-control cariProduk" id="cariProduk" name="nama_produk" autocomplete="on" placeholder="Cari Produk">
                        </div>
                    </div>
                    <!-- <p class="card-description mb-0">Daftar Produk.</p> -->
                </div>
            </div>
        </div>
    </div>
    <div class="row text-center ">
        <div class="col-md-12">
            <?php
            if ($this->session->flashdata('notif')) {
            echo $this->session->flashdata('notif');
            }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body produkCard">
                    <!-- <div class="text-left d-flex flex-row justify-content-start flex-wrap produkContainer">
                        
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    
</div>

<script>
    let harga = "";
    let harga_coret = "";

    let url = "<?= base_url().'produk/getAllProduk'?>";
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        success: function(data) {
            let limages = "";
            let thumbnail_produk = "";
            let produkCard = `<div class="text-left d-flex flex-row justify-content-start flex-wrap produkContainer">`;
            if(data.length == 0){
                produkCard = `
                    <div><strong>Produk tidak tersedia!!</strong></div>
                `;
                $("div.produkCard").html(produkCard);
            }else{
                
                $.each(data, function( index, value ) {
                    if(value.thumbnail_produk == "" || value.thumbnail_produk == null){
                        limages = `
                            <li data-target="#carousel${value.id_produk}" data-slide-to="0" class="active"></li>
                        `;
                        thumbnail_produk = `
                            <div class="carousel-item active">
                                <img src="<?= base_url(); ?>assets/assets/images/no-image.png" style="height: 200px;" class="d-block w-100" alt="...">
                            </div>
                        `;
                    }else{
                        
                        limages = "";
                        thumbnail_produk = "";
                        let img_thumbnail_produk = JSON.parse(value.thumbnail_produk);
                        $.each(img_thumbnail_produk, function( index, img ) {
                            if(index === 0){
                                limages += `
                                    <li data-target="#carousel${value.id_produk}" data-slide-to="${index}" class="active"></li>
                                `;
                                thumbnail_produk += `
                                    <div class="carousel-item active">
                                        <img src="<?= base_url(); ?>assets/resize_images/${img}" style="height: 200px;" class="d-block w-100" alt="...">
                                    </div>
                                `;

                            }else{
                                limages += `
                                    <li data-target="#carousel${value.id_produk}" data-slide-to="${index}"></li>
                                `;
                                thumbnail_produk += `
                                    <div class="carousel-item">
                                        <img src="<?= base_url(); ?>assets/resize_images/${img}" style="height: 200px;" class="d-block w-100" alt="...">
                                    </div>
                                `;
                            }
                        });
                    }

                    harga =  new Intl.NumberFormat("id-ID", {
                        style: "currency",
                        currency: "IDR"
                    }).format(value.harga);

                    harga_coret =  new Intl.NumberFormat("id-ID", {
                        style: "currency",
                        currency: "IDR"
                    }).format(value.harga_coret);

                    produkCard += `
                    <div class="card produk mb-3 mr-3">
                        <div id="carousel${value.id_produk}" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                ${limages}
                            </ol>
                            <div class="carousel-inner">
                                ${thumbnail_produk}
                            </div>
                            <a class="carousel-control-prev" href="#carousel${value.id_produk}" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel${value.id_produk}" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title mb-0">${value.nama_produk} <small><i>${value.kategori}</i></small></h5>
                            <small class="text-danger"><i><s>${harga_coret}</s></i></small>
                            <p class="card-text">${harga}</p>
                            <p class="card-text">${value.keterangan_produk}</p>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6 p-1">
                                    <a href="<?= base_url("Produk/vEditProduk/"); ?>${value.id_produk}" onclick="return confirm('Anda yakin ingin mengedit produk ini?')" class="btn btn-primary actionBtn">Edit</a>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 p-1">
                                    <a href="<?= base_url("Produk/deleteProduk/"); ?>${value.id_produk}" onclick="return confirm('Anda yakin ingin menghapus produk ini?')" class="btn btn-danger actionBtn">Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                });
                produkCard += `</div>`;
                $("div.produkCard").html(produkCard);
            }
        }
    });


    $('#cariProduk').on('keyup', function(){
        let keyword = $('#cariProduk').val();
        $.ajax({
            type: 'post',
            url: '<?= base_url().'produk/searchProduk'?>',
            data: {
                keyword: keyword,
            },
            dataType: 'json',
            success: function(data) {
                let limages = "";
                let thumbnail_produk = "";
                let produk = `<div class="text-left d-flex flex-row justify-content-start flex-wrap produkContainer">`;
                if(data.length == 0){
                    produk = `
                    <div><strong>Produk tidak tersedia!!</strong></div>
                    `;
                    $("div.produkCard").html(produk);
                }else{
                    $("div.produkCard").html('');
                    $.each(data, function( index, value ) {
                        if(value.thumbnail_produk == ""){
                            limages = `
                                <li data-target="#carousel${value.id_produk}" data-slide-to="0" class="active"></li>
                            `;
                            thumbnail_produk = `
                                <div class="carousel-item active">
                                    <img src="<?= base_url(); ?>assets/assets/images/no-image.png" style="height: 200px;" class="d-block w-100" alt="...">
                                </div>
                            `;
                        }else{
                            
                            limages = "";
                            thumbnail_produk = "";
                            let img_thumbnail_produk = JSON.parse(value.thumbnail_produk);
                            $.each(img_thumbnail_produk, function( index, img ) {
                                if(index === 0){
                                    limages += `
                                        <li data-target="#carousel${value.id_produk}" data-slide-to="${index}" class="active"></li>
                                    `;
                                    thumbnail_produk += `
                                        <div class="carousel-item active">
                                            <img src="<?= base_url(); ?>assets/resize_images/${img}" style="height: 200px;" class="d-block w-100" alt="...">
                                        </div>
                                    `;

                                }else{
                                    limages += `
                                        <li data-target="#carousel${value.id_produk}" data-slide-to="${index}"></li>
                                    `;
                                    thumbnail_produk += `
                                        <div class="carousel-item">
                                            <img src="<?= base_url(); ?>assets/resize_images/${img}" style="height: 200px;" class="d-block w-100" alt="...">
                                        </div>
                                    `;
                                }
                            });
                        }
                    
                        harga =  new Intl.NumberFormat("id-ID", {
                            style: "currency",
                            currency: "IDR"
                        }).format(value.harga);

                        harga_coret =  new Intl.NumberFormat("id-ID", {
                            style: "currency",
                            currency: "IDR"
                        }).format(value.harga_coret);
                        
                        produk += `
                        <div class="card produk mb-3 mr-3">
                            <div id="carousel${value.id_produk}" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    ${limages}
                                </ol>
                                <div class="carousel-inner">
                                    ${thumbnail_produk}
                                </div>
                                <a class="carousel-control-prev" href="#carousel${value.id_produk}" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carousel${value.id_produk}" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title mb-0">${value.nama_produk} <small><i>${value.kategori}</i></small></h5>
                                <small class="text-danger"><i><s>${harga_coret}</s></i></small>
                                <p class="card-text">${harga}</p>
                                <p class="card-text">${value.keterangan_produk}</p>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6 p-1">
                                        <a href="<?= base_url("Produk/vEditProduk/"); ?>${value.id_produk}" onclick="return confirm('Anda yakin ingin mengedit produk ini?')" class="btn btn-primary actionBtn">Edit</a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 p-1">
                                        <a href="<?= base_url("Produk/deleteProduk/"); ?>${value.id_produk}" onclick="return confirm('Anda yakin ingin menghapus produk ini?')" class="btn btn-danger actionBtn">Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;  
                    });
                    produk += `</div>`;
                    $("div.produkCard").html(produk);
                }
            }
        });
    })
</script>