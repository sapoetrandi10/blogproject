<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style>
    .produk{
        width: 300px;
        /* height: 300px; */
    }

    .actionBtn{
        width: 100%;
        /* width: 123px; */
        /* height: 300px; */
    }

    .note{
        font-size: .700rem;
    }

    .select2-selection__arrow b::before{
        display:none !important;
    }
</style>
<div class="container text-center">
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body text-left">
                    <h4 class="card-title">Edit Produk</h4>
                    <!-- <p class="card-description mb-0">Daftar Produk.</p> -->
                </div>
            </div>
        </div>
    </div>
    <div class="row text-center ">
        <div class="col-md-12">
        <?php
        if ($this->session->flashdata('notif')) {
        echo $this->session->flashdata('notif');
        }
        ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <form action="<?= base_url("produk/editProduk/"); ?>" method="POST" enctype="multipart/form-data" class="text-left myform">
                        <input type="hidden" class="form-control" id="id_produk" name="id_produk" autocomplete="off" value="<?= $ProdukById['id_produk'] ?>" required>
                        <div class="form-group">
                            <label for="nama_produk">Nama produk</label>
                            <input type="text" class="form-control" id="nama_produk" name="nama_produk" autocomplete="off" placeholder="Nama produk" value="<?= $ProdukById['nama_produk'] ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="kategori">Kategori</label>
                            <select class="js-example-basic-single w-100" id="kategori" name="kategori" required>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="harga">Harga</label>
                            <input type="number" class="form-control" id="harga" name="harga" autocomplete="off" placeholder="Harga" value="<?= $ProdukById['harga'] ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="harga_coret">Harga coret</label>
                            <input type="number" class="form-control" id="harga_coret" name="harga_coret" autocomplete="off" placeholder="Harga coret" value="<?= $ProdukById['harga_coret'] ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="thumbnail_produk">Thumbnail produk</label>
                            <input type="file" name="thumbnail_produk[]" id="thumbnail_produk" class="file-upload-default" multiple accept=".jpg, .jpeg, .png">
                            <div class="input-group col-xs-12">
                            <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image" value=`<?= $ProdukById['thumbnail_produk'] ?>`>
                            <span class="input-group-append">
                                <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                            </span>
                            </div>
                            <p class="note text-danger">*file allowed : jpg, jpeg, png . max size: 5 Mb</p>
                        </div>
                        <div class="form-group">
                            <label for="url">URL</label>
                            <input type="text" class="form-control" id="url" name="url" autocomplete="off" placeholder="URL" value="<?= $ProdukById['url'] ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="keterangan_produk">Keterangan produk</label>
                            <textarea class="form-control" id="keterangan_produk" name="keterangan_produk" rows="5" required><?= $ProdukById['keterangan_produk'] ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="text_button_beli">Text button beli</label>
                            <input type="text" class="form-control" id="text_button_beli" name="text_button_beli" autocomplete="off" placeholder="Text button beli" value="<?= $ProdukById['text_button_beli'] ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="subtext_button_beli">Subtext button beli</label>
                            <input type="text" class="form-control" id="subtext_button_beli" name="subtext_button_beli" autocomplete="off" placeholder="Subtext button beli" value="<?= $ProdukById['subtext_button_beli'] ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="warna_button_beli">Warna button beli</label>
                            <input type="text" class="form-control" id="warna_button_beli" name="warna_button_beli" autocomplete="off" placeholder="Masukan hexa warna button beli" value="<?= $ProdukById['warna_button_beli'] ?>" required>
                            <p class="note text-warning">#note: lebih disarankan menggunakan hexa warna</p>
                        </div>
                        <div class="custom-control custom-switch form-group">
                            <input type="checkbox" class="custom-control-input" id="bukti_pembayaran" name="bukti_pembayaran" <?php ($ProdukById['bukti_pembayaran'] == "on") ? print "checked" : "";?>>
                            <label class="custom-control-label" for="bukti_pembayaran">Wajib upload bukti pembayaran?</label>
                        </div>
                        <div class="card text-dark bg-light mb-3">
                            <div class="card-body">
                                <h5 class="card-title">Select payment method for this product: </h5>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-check">
                                                <label class="form-check-label" for="bank_transfer">
                                                    <input type="checkbox" class="form-check-input" id="bank_transfer" name="bank_transfer" <?php if (in_array("bank", json_decode($ProdukById['available_payment']))) { echo 'checked'; }?>>
                                                    Bank Transfer
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-check">
                                                <label class="form-check-label" for="payment_gateway">
                                                    <input type="checkbox" class="form-check-input" id="payment_gateway" name="payment_gateway" <?php if (in_array("payment_gateway", json_decode($ProdukById['available_payment']))) { echo 'checked'; }?>>
                                                    Payment gateway
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group select_bank">
                                            <label for="rekening_pembayaran">Select Bank</label>
                                            <select class="js-example-basic-multiple w-100" multiple="multiple" id="rekening_pembayaran" name="rekening_pembayaran[]" style="width: 100%;">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group select_payment_gateway">
                                            <label for="select_payment_gateway">Select payment gateway</label>
                                            <select class="js-example-basic-single w-100" id="select_payment_gateway" name="select_payment_gateway">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="text_konfirmasi_pembayaran">Text konfirmasi pembayaran</label>
                            <textarea class="form-control" id="text_konfirmasi_pembayaran" name="text_konfirmasi_pembayaran" rows="5" placeholder="Text konfirmasi pembayaran"><?= $ProdukById['text_konfirmasi_pembayaran'] ?></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="<?= base_url("produk"); ?>">
                            <button type="button" class="btn btn-danger btn-round">
                                <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Cancel
                            </button>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="card text-dark text-left">
                <div class="card-header bg-warning"><strong>Store Integration</strong></div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-1 d-flex justify-content-center align-items-center">
                            <i data-feather="key"></i>
                        </div>
                        <div class="col-md-9">
                            <h4>Store Integration</h4>
                            <p>Check all integration here.</p>
                        </div>
                        <div class="col-md-2 d-flex justify-content-center align-items-center">
                            <a href="<?= base_url("integrations/?q=".$ProdukById['id_produk']); ?>">
                                <button type="button" class="btn btn-info text-white btn-sm btn-round">
                                    <!-- <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>  -->
                                    View store integrations
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let payment_method = '<?= $ProdukById['available_payment'] ?>';
    let available_payment = JSON.parse(payment_method);

    if(available_payment.includes($('#bank_transfer').val())){
        $('#bank_transfer').attr( 'checked', true );
    };

    if(available_payment.includes($('#payment_gateway').val())){
        $('#payment_gateway').attr( 'checked', true );
    };

    $.ajax({
        url: "<?= base_url("produk/getAllRekening"); ?>",
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('#rekening_pembayaran').html(data);
            if($('#bank_transfer').prop('checked')){
                $('.select_bank').show(); 
            }else{
                $('.select_bank').hide(); 
            }
            let rekening_pembayaran = '<?= $ProdukById['rekening_pembayaran']; ?>';
            $("#rekening_pembayaran").val(JSON.parse(rekening_pembayaran)).trigger("change");
            $('#rekening_pembayaran').select2({
                width: '100%'
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("Terjadi kesalahan server");
        }
    });

    $.ajax({
        url: "<?= base_url("produk/getAllPaymentGateway"); ?>",
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            if($('#payment_gateway').prop('checked')){
                $('.select_payment_gateway').show(); 
            }else{
                $('.select_payment_gateway').hide(); 
            }
            
            $('#select_payment_gateway').html(data);
            let id_payment_gateway = '<?= $ProdukById['select_payment_gateway']; ?>';
        
            $('#select_payment_gateway option[value="'+id_payment_gateway+'"]').attr("selected","selected");
            $('#select_payment_gateway').select2().trigger('load');
            $('#select_payment_gateway').select2({
                width: '100%'
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("Terjadi kesalahan server");
        }
    });

    $.ajax({
        url: "<?= base_url("produk/getAllKategoriproduk"); ?>",
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('#kategori').html(data);
            let kategori = '<?= $ProdukById['kategori']; ?>';
            
            $('#kategori option[value="'+kategori+'"]').attr("selected","selected");
            $('#kategori').select2().trigger('load');
            $('#kategori').select2({
                width: '100%'
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("Terjadi kesalahan server");
        }
    });

    $(function () {
        CKEDITOR.replace('text_konfirmasi_pembayaran',{
            filebrowserImageBrowseUrl : '<?php echo base_url('kcfinder/browse.php');?>',
            height: '400px'             
        });
    });
    
    $('.myform :checkbox#bank_transfer ').change(function() {
        if (this.checked) {
            $('.select_bank').show();
        } else {
            $('.select_bank').hide();
        }
    });

    $('.myform :checkbox#payment_gateway').change(function() {
        if (this.checked) {
            $('.select_payment_gateway').show();
        } else {
            $('.select_payment_gateway').hide();
        }
    });
</script>