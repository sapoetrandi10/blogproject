<style>
    .wraper{
        min-height: 668px;
    }

    .select2-selection__arrow b::before{
        display:none !important;
    }
</style>

<div class="wraper ">
    <div class="row text-center ">
        <div class="col-md-4 offset-md-4">
        <?php
        if ($this->session->flashdata('notif')) {
        echo $this->session->flashdata('notif');
        }
        ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-md-1 grid-margin stretch-card">
            <div class="card  text-center">
            <div class="card-body">
                <h3 class="">Form Add Administrator</h3>
                <div class="row text-center ">
                    <div class="col-md-12">
                    <?php
                    if ($this->session->flashdata('notif')) {
                    echo $this->session->flashdata('notif');
                    }
                    ?>
                    </div>
                </div>
                <form class="forms-sample text-left" action="<?= base_url('administrator/addAdmin')?>" method="POST">
                <div class="form-group">
                    <label for="email_administrator">Email</label>
                    <input type="email" class="form-control" id="email_administrator" name="email_administrator" autocomplete="off" placeholder="Email" value="<?= set_value('email_administrator'); ?>" required>
                </div>
                <div class="form-group">
                    <label for="nama_administrator">Nama Administrator</label>
                    <input type="text" class="form-control" id="nama_administrator" name="nama_administrator" autocomplete="off" placeholder="Nama Administrator" value="<?= set_value('nama_administrator'); ?>" maxlength="20" required>
                </div>
                <div class="form-group">
                    <label for="telp_administrator">No Telpon</label>
                    <input type="number" class="form-control" id="telp_administrator" name="telp_administrator" autocomplete="off" placeholder="No Telpon" value="<?= set_value('telp_administrator'); ?>" required>
                </div>
                <div class="form-group">
                    <label for="status_administrator">Status administrator</label>
                    <select class="form-control form-control-sm mb-3" id="status_administrator" name="status_administrator" required>
                        <!-- <option selected>Select status administrator</option> -->
                        <option value="konfirmasi">Konfirmasi</option>
                        <option value="active">Active</option>
                        <option value="deactive">Deactive</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="role_administrator">Role</label>
                    <select class="js-example-basic-single w-100" id="role_administrator" name="role_administrator" required>
                    </select>
                </div>
                <!-- <div class="form-group">
                    <label for="password_confirmation">Role</label>
                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" autocomplete="off" placeholder="Konfirmasi Password" value="<?= set_value('password_confirmation'); ?>">
                </div> -->
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary mr-2">Add</button>
                    <a href="<?= base_url() ?>administrator">
                        <button type="button" class="btn btn-light">Cancel</button>
                    </a>
                </div>  
                </form>
            </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">   
$( document ).ready(function() {
    $.ajax({
        url: "<?= base_url("Administrator/getAllDivisiAdmin"); ?>",
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('#role_administrator').html(data);
            $('#role_administrator').select2({
                width: '100%'
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("Terjadi kesalahan server");
        }
    });
})
</script>