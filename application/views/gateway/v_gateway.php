<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style>
</style>

<div class="container">
    <div class="card mb-3">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <!-- <i class="mdi mdi-wallet" style="font-size: 36px;"></i> -->
                    <h4>API Key Payment Gateway</h4>
                </div>
                <div class="col-md-6  text-right">
                    <a href="<?= base_url("gateway/vAddGateway"); ?>">
                        <button type="button" class="btn btn-primary text-white btn-md btn-round">
                            <i class="mdi mdi-plus">Add payment gateway</i>
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row text-center ">
        <div class="col-md-12">
            <?php if ($this->session->flashdata('notif')) { echo $this->session->flashdata('notif');}?>
        </div>
    </div>

    <div class="card text-dark">
        <!-- <div class="card-header bg-warning"><strong>Store Integration</strong></div> -->
        <div class="card-body">
            <div class="table-responsive pt-3">
                <table class="table table-bordered table-hover text-center" id="example">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Payment Gateway</th>
                            <th>Name</th>
                            <th>Key</th>
                            <th>Status Active</th>
                            <th>Status Level</th>
                            <th>Layanan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
  </div>

    <a href="<?= base_url("integrations"); ?>">
        <button type="button" class="btn btn-danger text-white btn-md btn-round mt-3">
            <i class="mdi mdi-arrow-left"></i>
            back
        </button>
    </a>
</div>

<script>
$(document).ready(function(){
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var table = $("#example").DataTable({
        initComplete: function() {
            var api = this.api();
            $('#mytable_filter input')
            .off('.DT')
            .on('input.DT', function() {
                api.search(this.value).draw();
            });
        },
        oLanguage: {
            sProcessing: "loading..."
        },
        processing: true,
        serverSide: true,
        // "scrollY": true,
        'ajax': {
            url: "<?= base_url().'gateway/getAllPaymentGatewayJson'?>",
            type: "POST"
        },
        columns: [
        {"data": "id"},
        {"data": "payment_gateway"},
        {"data": "nama"},
        {"data": "api_key"},
        {"data": "status"},
        {"data": "status_level"},
        {"data": "layanan", render : listLayanan},
        {"data": "id", render : btn_action}
        ],
        rowCallback: function(row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            $('td:eq(0)', row).html();
        }
    });

    // order.dt search.dt
    //untuk memberi nomor di setiap row
    table.on( 'draw.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    
});

function listLayanan(layanan){
    const obj = JSON.parse(layanan);
    let badge = `<div class="badgeContainer d-flex justify-content-center flex-wrap">`;

    $.each(obj, function( index, value ) {
        badge += `
            <span class="badge badge-pill badge-light mr-1 mb-1">${value}</span>
            `;
        });
        badge += `</div>`;
        return badge;
}

function btn_action(id){
    return `
    <ul class="nav">
        <li class="nav-item ">
            <a href="<?= base_url("gateway/vEditGateway/"); ?>${id}" class="nav-link text-success" onclick="return confirm('Anda yakin ingin mengedit payment gateway ini?')">
                <i class="mdi mdi-tooltip-edit"> Edit</i>
            </a>
        </li>
    </ul>
    `;
}
</script>