<style>
    .note{
        font-size: .700rem;
    }
</style>

<div class="container">
    <div class="card text-dark">
        <div class="card-header bg-primary text-white text-center"><strong>Form new add payment gateway</strong></div>
        <div class="card-body">
            <div class="row text-center ">
                <div class="col-md-12">
                    <?php if ($this->session->flashdata('notif')) { echo $this->session->flashdata('notif');}?>
                </div>
            </div>
            <form class="forms-sample" action="<?= base_url('gateway/addPaymentGateway') ?>" method="POST">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="payment_gateway">Payment Gateway</label>
                            <select class="form-control form-control-sm mb-3" id="payment_gateway" name="payment_gateway">
                                <option selected disabled>Select payment gateway</option>
                                <option value="ipaymu">Ipaymu</option>
                                <!-- <option value="midtrans">Midtrans</option> -->
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" id="nama" name="nama" autocomplete="off" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <label for="va">Virtual Account (va)</label>
                            <input type="text" class="form-control" id="va" name="va" autocomplete="off" placeholder="Virtual Account">
                            <div class="spinner">
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="api_key">API key</label>
                            <input type="text" class="form-control" id="api_key" name="api_key" autocomplete="off" placeholder="API key">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="layanan">Layanan</label>
                            <select class="js-example-basic-multiple w-100" multiple="multiple" id="layanan" name="layanan[]">
                                <option value="alfamart">Alfamart</option>
                                <option value="qris">Qris (Gopay,Ovo,Dana,ShopePay)</option>
                            </select>
                            <p class="note text-danger">*Penting: Pilih Layanan Yang Tersedia Di Payment Gateway Anda !!</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="status_level">Status level</label>
                            <select class="form-control" id="status_level" name="status_level">
                                <option value="primary" >Primary</option>
                                <option value="optional" >Optional</option>
                            </select>
                            <!-- <p class="note text-danger">*Penting: Pilih Layanan Yang Tersedia Di Payment Gateway Anda !!</p> -->
                        </div>
                    </div>
                    <div class="col-md-2">
                        <label for="status">Status</label>
                        <div class="form-group">
                            <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="status" id="statusYa" value="ya">
                                YA
                            </label>
                            </div>
                            <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="status" id="statusTidak" value="tidak" checked>
                                TIDAK
                            </label>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary mr-2 btnSubmit">Submit</button>
                <a href="<?= base_url("gateway"); ?>">
                    <button type="button" class="btn btn-danger text-white btn-md btn-round">
                        <i class="mdi mdi-arrow-left" style="font-size: 12px;"></i> cancel
                    </button>
                </a>
            </form>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $('.btnSubmit').hide();
    $('#api_key, #va').bind('input', function(){
        let va = $("#va").val();
        let api_key = $("#api_key").val();
        checkApiKey(va, api_key);
    });
});

function checkApiKey(va, api_key){
    let checker = `
    <div class="spinner-border text-secondary mt-1" role="status" style="width: 17px; height: 17px;">
        <span class="sr-only">Loading...</span>
    </div>
    <small>checking...</small>
    `;
    $(".spinner").html(checker);
    
    $.ajax({
        "url": "<?php echo base_url('gateway/paymnetGatewayChecker') ?>",
        "type": "POST",
        "data" : {
            va : va,
            secret : api_key
        },
        dataType : 'json',
        success : function(data){
            console.log(data);
            // return false;
            let checker = ``;
            if(data.Message == 'success'){
                    checker = `
                        <i class="mdi mdi-check text-success"></i>
                    `;
                    $('.btnSubmit').show();
                $(".spinner").html(checker);
            }else{
                checker = `
                    <small class="text-danger">${data.Error.account[0]}!!</small>
                `;
                $('.btnSubmit').hide();
                $(".spinner").html(checker);
            }
        },
        error: function(data){
            console.log(data);
        } 
    })
}
</script>