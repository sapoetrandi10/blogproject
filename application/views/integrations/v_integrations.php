<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style>
</style>

<div class="container">
    <div class="card mb-3">
        <div class="card-body">
            <h3>Integrations</h3>
        </div>
    </div>

    <div class="card text-dark">
        <div class="card-header bg-warning"><strong>Store Integration</strong></div>
        <div class="card-body">
            <div class="row border-bottom py-2 mb-3">
                <div class="col-md-1 text-center">
                    <i class="mdi mdi-wallet" style="font-size: 36px;"></i>
                </div>
                <div class="col-md-9">
                    <h4>Payment Gateway</h4>
                    <p>Connect your Payment Gateway account using API token.</p>
                </div>
                <div class="col-md-2">
                    <a href="<?= base_url("gateway"); ?>">
                        <button type="button" class="btn btn-info text-white btn-sm btn-round">
                            View payment gateway
                        </button>
                    </a>
                </div>
            </div>
            <div class="row border-bottom py-2 mb-3">
                <div class="col-md-1 d-flex justify-content-center align-items-center">
                    <i class="mdi mdi-message-text" style="font-size: 36px;"></i>
                </div>
                <div class="col-md-9">
                    <h4>Whatsapp Gateway</h4>
                    <p>Send messages to your customers via WhatsApp.</p>
                </div>
                <!-- whatsappGateway/vSettingFollowUp/2 -->
                <div class="col-md-2 d-flex justify-content-center align-items-center">
                <?php if (isset($_GET['q'])): ?>
                    <a href="<?= base_url("whatsappGateway/vSettingFollowUp/".$_GET['q']); ?>">
                <?php else: ?>
                    <a href="<?= base_url("whatsappGateway"); ?>">
                <?php endif; ?>
                        <button type="button" class="btn btn-info text-white btn-sm btn-round">
                            View WhatsApp gateway
                        </button>
                    </a>
                </div>
            </div>
        </div>
  </div>

    <a href="<?= base_url("settings"); ?>">
        <button type="button" class="btn btn-danger text-white btn-md btn-round mt-3">
            <i class="mdi mdi-arrow-left"></i>
            back
        </button>
    </a>
</div>
<script>
    // console.log(url);
    // $('.btnWaGateway').attr('href', url);
</script>
