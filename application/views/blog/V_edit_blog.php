<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<style>
    .select2-selection__arrow b::before{
        display:none !important;
    }
</style>
<div class="container">
    <div class="card  text-center">
        <div class="card-body">
            <h3>Form Edit Blog</h3>
            <div class="row text-center ">
                <div class="col-md-12">
                <?php
                if ($this->session->flashdata('notif')) {
                echo $this->session->flashdata('notif');
                }
                ?>
                </div>
            </div>
            <form action="<?= base_url("blog/editBlog/"); ?>" method="POST" enctype="multipart/form-data" class="text-left">
                <input type="hidden" class="form-control" id="id_blog" name="id_blog" value="<?= $blogById['id_blog'] ?>" required>
                <!-- <input type="text" class="form-control" id="latest_thumbnail_blog" name="latest_thumbnail_blog" value="<?= $blogById['thumbnail_blog'] ?>">
                <input type="text" class="form-control" id="latest_blog_og_image" name="latest_blog_og_image" value="<?= $blogById['blog_og_image'] ?>"> -->

                <div class="mb-3">
                    <label for="id_kategori" class="form-label">Kategori</label>
                    <select class="js-example-basic-single js-states form-control form-control-sm mb-3" id="id_kategori" name="id_kategori" required>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Judul Blog</label>
                    <input type="text" class="form-control" id="judul_blog" name="judul_blog" value="<?= $blogById['judul_blog'] ?>" maxlength="30" required>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Thumbnail Blog</label>
                    <span class="latestImg">Gambar sebelumnya : <?= $blogById['thumbnail_blog'] ?> </span>
                    <input class="form-control" type="file" id="thumbnail_blog" name="thumbnail_blog" accept=".jpg, .jpeg, gif, .png">
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Content Blog</label>
                    <textarea class="form-control" rows="10" id="content_blog" name="content_blog" required><?= $blogById['content_blog'] ?></textarea>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">View Count</label>
                    <input class="form-control" type="number" id="view_count" name="view_count" value="<?= $blogById['view_count'] ?>" required>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Status Blog</label>
                    <select class="form-select" id="status_blog" name="status_blog" required>
                        <!-- <option selected>Silahkan Pilih</option> -->
                        <option value="show" <?php if ($blogById['status_blog'] == "show") { echo 'selected'; }?>>Show</option>
                        <option value="hidden" <?php if ($blogById['status_blog'] == "hidden") { echo 'selected'; }?>>Hidden</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Blog Seo Url</label>
                    <input class="form-control" type="text" id="blog_seo_url" name="blog_seo_url" value="<?= $blogById['blog_seo_url'] ?>" required>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Blog Meta Author</label>
                    <input class="form-control" type="text" id="blog_meta_author" name="blog_meta_author" value="<?= $blogById['blog_meta_author'] ?>" required>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Blog Meta Keyword</label>
                    <input class="form-control" type="text" id="blog_meta_keyword" name="blog_meta_keyword" value="<?= $blogById['blog_meta_keyword'] ?>" required>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Blog Og Image</label>
                    <span class="latestImg">Gambar sebelumnya : <?= $blogById['blog_og_image'] ?> </span>
                    <input class="form-control" type="file" id="blog_og_image" name="blog_og_image" accept=".jpg, .jpeg, gif, .png">
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Blog Og Title</label>
                    <input class="form-control" type="text" id="blog_og_title" name="blog_og_title" value="<?= $blogById['blog_og_title'] ?>" required>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Blog Og Description</label>
                    <textarea class="form-control" rows="10" id="blog_og_description" name="blog_og_description" required><?= $blogById['blog_og_description'] ?></textarea>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Blog terhapus</label>
                    <select class="form-select" id="blog_terhapus" name="blog_terhapus" required>
                        <!-- <option selected>Silahkan Pilih</option> -->
                        <option value="no" <?php if ($blogById['blog_terhapus'] == "no") { echo 'selected'; }?>>No</option>
                        <option value="yes" <?php if ($blogById['blog_terhapus'] == "yes") { echo 'selected'; }?>>Yes</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Admin last edit</label>
                    <input class="form-control" type="text" id="blog_admin_last_edit" name="blog_admin_last_edit" value="<?= $blogById['blog_admin_last_edit'] ?>" required>
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
                <a href="<?= base_url("blog"); ?>">
                    <button type="button" class="btn btn-danger btn-round">
                        <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Cancle
                    </button>
                </a>
            </form>
        </div>
    </div>
</div>

<!-- filebrowserImageBrowseUrl: '<?= base_url(); ?>ckfinder/ckfinder.html?type=Images', -->
<script>
    $(function () {
        CKEDITOR.replace('content_blog',{
            filebrowserImageBrowseUrl : '<?php echo base_url('kcfinder/browse.php');?>',
            height: '400px'             
        });
    });
    // var editor = CKEDITOR.replace('content_blog', {
    //     height: 300,
    //     filebrowserBrowseUrl: '<?= base_url(); ?>ckfinder/ckfinder.html',
    //     filebrowserImageBrowseUrl: '<?= base_url(); ?>ckfinder/ckfinder.html?type=Images',
    //     filebrowserFlashBrowseUrl: '<?= base_url(); ?>ckfinder/ckfinder.html?type=Flash',
    //     filebrowserUploadUrl: '<?= base_url(); ?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    //     filebrowserImageUploadUrl: '<?= base_url(); ?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
    //     filebrowserFlashUploadUrl: '<?= base_url(); ?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
    //     filebrowserWindowWidth: '1000',
    //     filebrowserWindowHeight: '700'
    // });
    // CKFinder.setupCKEditor(editor, '<?= base_url(); ?>ckfinder/');
$( document ).ready(function() {
    $.ajax({
        url: "<?= base_url("blog/getAllKategori"); ?>",
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('#id_kategori').html(data);
            let id_kategori = '<?= $blogById['id_kategori']; ?>';
            
            $('#id_kategori option[value="'+id_kategori+'"]').attr("selected","selected");
            $('#id_kategori').select2().trigger('load');
            $('.js-example-basic-single').select2({
                width: '100%'
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("Terjadi kesalahan server");
        }
    });
});
</script>