<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">
    <div class="card  text-center">
        <div class="card-body">
            <h1>Table Blog</h1>
            <div class="btn-tambah mb-3 text-left">
                <a href="<?= base_url("blog/vAddBlog/"); ?>">
                    <button type="button" class="btn btn-primary btn-round">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Blog
                    </button>
                </a>
            </div>
            <div class="row text-center ">
                <div class="col-md-12">
                <?php
                if ($this->session->flashdata('notif')) {
                echo $this->session->flashdata('notif');
                }
                ?>
                </div>
            </div>
            <table class="table table-bordered" id="example">
                <thead>
                    <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Kategori</th>
                    <th scope="col">Status Show</th>
                    <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> -->
<script>
$(document).ready(function(){
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var table = $("#example").DataTable({
        initComplete: function() {
            var api = this.api();
            $('#mytable_filter input')
            .off('.DT')
            .on('input.DT', function() {
                api.search(this.value).draw();
            });
        },
        oLanguage: {
            sProcessing: "loading..."
        },
        processing: true,
        serverSide: true,
        "autoWidth": false,
        "scrollX": true,
        "scrollY": "300px",
        'ajax': {
            url: "<?= base_url().'blog/getAllBlogJson'?>",
            type: "POST"
        },
        columns: [
        {"data": "id_blog"},
        {"data": "judul_blog"},
        {"data": "nama_kategori"},
        {"data": "status_blog"},
        {"data": "id_blog", render : btn_action}
        ],
        rowCallback: function(row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            $('td:eq(0)', row).html();
        }
    });

    // order.dt search.dt
    //untuk memberi nomor di setiap row
    table.on( 'draw.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
});
    function btn_action(id_blog){
        return `
        <ul class="nav">
            <li class="nav-item">
                <a href="<?= base_url("blog/vEditBlog/"); ?>${id_blog}" class="nav-link text-success" onclick="return confirm('Anda yakin ingin edit blog ini?')">
                    <i class="mdi mdi-tooltip-edit"> Edit</i>
                </a>
            </li>
            <li class="nav-item ">
                <a href="<?= base_url("blog/deleteBlog/"); ?>${id_blog}" class="nav-link text-danger" onclick="return confirm('Anda yakin ingin hapus blog ini?')">
                    <i class="mdi mdi-delete"> Delete</i>
                </a>
            </li>
        </ul>
        `;
    }
</script>