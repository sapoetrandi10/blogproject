<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    .select2-selection__arrow b::before{
        display:none !important;
    }
</style>
<div class="container">
    <div class="card  text-center">
        <div class="card-body">
            <h3>Form Tambah Blog</h3>
            <div class="row text-center ">
                <div class="col-md-12">
                <?php
                if ($this->session->flashdata('notif')) {
                echo $this->session->flashdata('notif');
                }
                ?>
                </div>
            </div>
            <form action="<?= base_url("blog/addBlog/"); ?>" method="POST" enctype="multipart/form-data" id="addBlog" class="text-left">
                <div class="mb-3">
                    <label for="id_kategori" class="form-label">Kategori</label>
                    <select class="form-select" id="id_kategori" name="id_kategori" style="width: 15%;" maxlength="20" required>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Judul Blog</label>
                    <input type="text" class="form-control" id="judul_blog" name="judul_blog" maxlength="30" required>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Thumbnail Blog</label>
                    <input class="form-control" type="file" id="thumbnail_blog" name="thumbnail_blog" accept=".jpg, .jpeg, gif, .png" required>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Content Blog</label>
                    <textarea class="form-control" rows="10" id="content_blog" name="content_blog"></textarea>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">View Count</label>
                    <input class="form-control" type="number" id="view_count" name="view_count" required>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Status Blog</label>
                    <select class="form-select" id="status_blog" name="status_blog" required>
                        <!-- <option selected>Silahkan Pilih</option> -->
                        <option value="show">Show</option>
                        <option value="hidden">Hidden</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Blog Seo Url</label>
                    <input class="form-control" type="text" id="blog_seo_url" name="blog_seo_url" required>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Blog Meta Author</label>
                    <input class="form-control" type="text" id="blog_meta_author" name="blog_meta_author" required>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Blog Meta Keyword</label>
                    <input class="form-control" type="text" id="blog_meta_keyword" name="blog_meta_keyword" required>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Blog Og Image</label>
                    <input class="form-control" type="file" id="blog_og_image" name="blog_og_image" accept=".jpg, .jpeg, gif, .png" required>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Blog Og Title</label>
                    <input class="form-control" type="text" id="blog_og_title" name="blog_og_title" required>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Blog Og Description</label>
                    <textarea class="form-control" rows="10" id="blog_og_description" name="blog_og_description" required></textarea>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Blog terhapus</label>
                    <select class="form-select" id="blog_terhapus" name="blog_terhapus" required>
                        <!-- <option selected>Silahkan Pilih</option> -->
                        <option value="no">No</option>
                        <option value="yes">Yes</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Admin last edit</label>
                    <input class="form-control" type="text" id="blog_admin_last_edit" name="blog_admin_last_edit" required>
                </div>
                <button type="submit" class="btn btn-primary addBtn">Publish</button>
                <a href="<?= base_url("blog"); ?>">
                    <button type="button" class="btn btn-danger btn-round">
                        <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Cancle
                    </button>
                </a>
            </form>
        </div>
    </div>
</div>


<!-- filebrowserImageBrowseUrl: '<?= base_url(); ?>ckfinder/ckfinder.html?type=Images', -->
<script type="text/javascript">
    $(function () {
        CKEDITOR.replace('content_blog',{
            filebrowserImageBrowseUrl : '<?php echo base_url('kcfinder/browse.php');?>',
            height: '400px'             
        });
    });
$( document ).ready(function() {
    
    $.ajax({
        url: "<?= base_url("blog/getAllKategori"); ?>",
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('#id_kategori').html(data);
            $('#id_kategori').select2({
                width: '100%'
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("Terjadi kesalahan server");
        }
    });
})
</script>