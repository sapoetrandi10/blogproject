<style>
    .wraper{
        min-height: 668px;
    }
</style>

<div class="wraper ">
    
    <div class="row">
        <div class="col-md-10 offset-md-1 grid-margin stretch-card">
            <div class="card  text-center">
                <div class="card-body">
                    <h3 class="">Form Edit Divisi Administrator</h3>
                    <div class="row text-center ">
                        <div class="col-md-12">
                        <?php
                        if ($this->session->flashdata('notif')) {
                        echo $this->session->flashdata('notif');
                        }
                        ?>
                        </div>
                    </div>
                <form class="forms-sample text-left" action="<?= base_url('divisiAdministrator/editDivisiAdmin')?>" method="POST">
                    <input type="hidden" class="form-control" id="id_divisi" name="id_divisi" autocomplete="off" placeholder="ID Divisi" value="<?= $divisiById['id_divisi'] ?>" required>
                    <div class="form-group">
                        <label for="nama_divisi">Nama Divisi</label>
                        <input type="text" class="form-control" id="nama_divisi" name="nama_divisi" autocomplete="off" placeholder="Nama Divisi" value="<?= $divisiById['nama_divisi'] ?>" maxlength="20" required>
                    </div>
                    <div class="form-group">
                        <label for="deskripsi">Deskripsi</label>
                        <textarea class="form-control" id="deskripsi" name="deskripsi" rows="5" autocomplete="off" placeholder="put some description here.." required><?= $divisiById['deskripsi'] ?></textarea>
                    </div>
                    <!-- <div class="form-group">
                        <label for="id_menu">Id Menu</label>
                        <input type="text" class="form-control" id="id_menu" name="id_menu" autocomplete="off" placeholder="Id Menu" value="<?= $divisiById['id_menu'] ?>" required>
                    </div> -->
                    <div class="form-group">
                        <label for="id_menu">Id Menu</label>
                        <select class="form-control form-control-sm mb-3" id="id_menu" name="id_menu" required>
                            <option selected disabled>-- Select role --</option>
                            <option value="1" <?php if ($divisiById['id_menu'] == "1") { echo 'selected'; }?>>1</option>
                            <option value="2" <?php if ($divisiById['id_menu'] == "2") { echo 'selected'; }?>>2</option>
                            <option value="3" <?php if ($divisiById['id_menu'] == "3") { echo 'selected'; }?>>3</option>
                            <option value="4" <?php if ($divisiById['id_menu'] == "4") { echo 'selected'; }?>>4</option>
                        </select>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary mr-2">Edit</button>
                        <a href="<?= base_url() ?>divisiAdministrator">
                            <button type="button" class="btn btn-light">Cancel</button>
                        </a>
                    </div>  
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
