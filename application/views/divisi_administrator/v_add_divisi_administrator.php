<style>
    .wraper{
        min-height: 668px;
    }
</style>

<div class="wraper ">
    <div class="row">
        <div class="col-md-10 offset-md-1 grid-margin stretch-card">
            <div class="card  text-center">
                <div class="card-body">
                    <h3 class="">Form Add Divisi Administrator</h3>
                    <div class="row text-center ">
                        <div class="col-md-12">
                        <?php
                        if ($this->session->flashdata('notif')) {
                        echo $this->session->flashdata('notif');
                        }
                        ?>
                        </div>
                    </div>
                <form class="forms-sample text-left" action="<?= base_url('divisiAdministrator/addDivisiAdmin')?>" method="POST">
                <div class="form-group">
                    <label for="nama_divisi">Nama Divisi</label>
                    <input type="text" class="form-control" id="nama_divisi" name="nama_divisi" autocomplete="off" placeholder="Nama Divisi" value="<?= set_value('nama_divisi'); ?>" maxlength="20" required>
                </div>
                <div class="form-group">
                    <label for="deskripsi">Deskripsi</label>
                    <textarea class="form-control" id="deskripsi" name="deskripsi" rows="5" autocomplete="off" placeholder="put some description here.." required></textarea>
                </div>
                <div class="form-group">
                    <label for="id_menu">Id Menu</label>
                    <select class="form-control form-control-sm mb-3" id="id_menu" name="id_menu" required>
                        <option selected disabled>-- Select role --</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="3">4</option>
                    </select>
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary mr-2">Add</button>
                    <a href="<?= base_url() ?>divisiAdministrator">
                        <button type="button" class="btn btn-light">Cancel</button>
                    </a>
                </div>  
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
