
<style>
    .note{
        font-size: .700rem;
    }
</style>

<div class="container">
    <div class="card  text-center">
        <div class="card-body">
            <h3>Form Tambah Kategori Blog</h3>
            <div class="row text-center mt-2">
                <div class="col-md-12">
                <?php
                if ($this->session->flashdata('notif')) {
                echo $this->session->flashdata('notif');
                }
                ?>
                </div>
            </div>
            <form action="<?= base_url("kategori/addKategori/"); ?>" method="POST" enctype="multipart/form-data" class="text-left">
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Nama Kategori</label>
                    <input type="text" class="form-control" id="nama_kategori" name="nama_kategori" maxlength="20" required>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Thumbnail Kategori</label>
                    <input class="form-control" type="file" id="thumbnail_kategori" name="thumbnail_kategori" accept=".jpg, .jpeg, gif, .png" required>
                    <p class="note text-danger">*file diizinkan : jpg, jpeg, gif, png. ukuran maksimal: 5 Mb</p>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Status Kategori</label>
                    <select class="form-select" id="status_kategori" name="status_kategori" required>
                        <!-- <option selected>Silahkan Pilih</option> -->
                        <option value="show">Show</option>
                        <option value="hidden">Hidden</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?= base_url("kategori"); ?>">
                    <button type="button" class="btn btn-danger btn-round">
                        <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Cancle
                    </button>
                </a>
            </form>
        </div>
    </div>
</div>
