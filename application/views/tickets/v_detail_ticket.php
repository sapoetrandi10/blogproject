<style>
    .iconFile{
        width: 40px;
        height: 40px;
        background-color: #EAEAEA;
    }
    .iconFile{
        text-align:center;
        line-height: 40px;
        font-size: 20px;
        color: white;
    }
</style>
<div class="container">
    <!-- <?= var_dump($detailTicket) ?> -->
    <div class="row mb-3">  
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h3>Detail ticket</h3>
                    <span>ID Ticket: <?= $detailTicket['id_ticket'] ?> | Status: <?= $detailTicket['status_ticket'] ?> | created: <?= $detailTicket['datetime_ticket'] ?>   </span>
                </div>
            </div>
        </div>
    </div>
    <div class="row chat-wrapper">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row position-relative">
                        <div class="col-lg-12 chat-content">
                            <div class="chat-header border-bottom pb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex align-items-center">
                                        <i data-feather="corner-up-left" id="backToChatList" class="icon-lg mr-2 ml-n2 text-muted d-lg-none"></i>
                                        <figure class="mb-0 mr-2">
                                            <img src="<?= base_url(); ?>assets/assets/images/faces/face2.jpg" class="img-sm rounded-circle" alt="image">
                                            <div class="status online"></div>
                                            <div class="status online"></div>
                                        </figure>
                                        <div>
                                            <p><?= $detailTicket['nama_member'] ?></p>
                                            <p class="text-muted tx-13"><?= $detailTicket['email_member'] ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="chat-body">
                                <ul class="messages">
                                                                        
                                </ul>
                            </div>
                            <div class="chat-footer d-flex">
                                <div class="form-group d-none d-md-block">
                                    <!-- <input type="file" name="img[]" class="file-upload-default"> enctype="multipart/form-data" -->
                                    <form id="target">
                                    <input type="file" id="attachment" name="attachment" class="file-upload-default" accept=".jpg, .jpeg, gif, .png, .docx, .csv, .xlsx">
                                        <div class="input-group col-xs-12">
                                            <span class="input-group-append">
                                                <button class="file-upload-browse btn border btn-icon rounded-circle mr-2" type="button" data-toggle="tooltip" title="Attatch files">
                                                    <i data-feather="paperclip" class="text-muted"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- <form class="search-form flex-grow mr-2"> -->
                                    <div class="input-group mr-2">
                                        <input type="text" class="form-control rounded-pill" id="komentar" name="komentar" placeholder="Type a message">
                                    </div>
                                    <!-- </form> -->
                                    <div>
                                        <button type="submit" class="btn btn-primary btn-icon rounded-circle" id="btnSend">
                                            <i data-feather="send"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <a href="<?= base_url("tickets"); ?>" class="mr-1">
                        <button type="button" class="btn btn-light btn-round">
                            <i class="mdi mdi-arrow-left-bold"> Back</i>
                        </button>
                    </a>
                    <a href="<?= base_url("tickets/adminResolvedTicket/".$detailTicket['id_ticket']); ?>" onclick="return confirm('Anda yakin ingin menutup ticket ini?')">
                        <button type="button" class="btn btn-success btn-round">
                            <i class="mdi mdi-checkbox-marked-circle-outline">  Resolved</i>
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
let komentar = "";
let id_ticket = <?= $detailTicket['id_ticket'] ?>;
let tujuan_pengirim = '<?= $detailTicket['email_member'] ?>';
$( document ).ready(function() {
    
    $("#komentar").keyup(function(e){ 
        komentar = $('#komentar').val();
        komentar = $.trim(komentar);

        var code = e.key;
        if(code==="Enter") e.preventDefault();
        if(code==="Enter"){
            $('#target').trigger('submit');
        }
    });

    $('#target').submit(function(e){
        e.preventDefault();
        let attachment = "";
        if(komentar.length > 0){
            $.ajax({
                url: "<?= base_url("tickets/uploadWithAjax/"); ?>",
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data){
                    let responseUpload = JSON.parse(data);
                    if(responseUpload !== null){
                        if(responseUpload.status === 'false'){
                            alert(responseUpload.message);
                            return false;
                        }else if(responseUpload.status === 'true'){
                            attachment = responseUpload.thumbnail;                     
                            postBalasan(id_ticket, tujuan_pengirim, komentar, attachment);
                            $("#target")[0].reset();
                            komentar = "";
                        }
                    }else{
                        postBalasan(id_ticket, tujuan_pengirim, komentar, null);
                        $("#target")[0].reset();
                        komentar = "";
                    }
                },
                error: function(data){
                    console.log(data);
                    komentar = "";
                }
            });
        }
    });

    function postBalasan(id_ticket, tujuan_pengirim, komentar, attachment){
        $.ajax({
            url: "<?= base_url("tickets/postBalasanAdmin/"); ?>",
            type: "POST",
            data: {
                id_ticket : id_ticket,
                tujuan_pengirim : tujuan_pengirim,
                komentar : komentar,
                attachment : attachment
            },
            dataType: "JSON",
            success: function(data) {
                
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("Terjadi kesalahan server");
            }
        });
        $('div.chat-body').scrollTop($('div.chat-body')[0].scrollHeight);
    }
    
    getTicketBalasan();

    function getTicketBalasan(){
        let subject = "<?= $detailTicket['subject'] ?>";
        let datetime_ticket = '<?= $detailTicket['datetime_ticket'] ?>';
        datetime_ticket = formatTgl(datetime_ticket);
        let isi_ticket = `<?= $detailTicket['isi_ticket']; ?>`;
        let pengirim = "<?= $detailTicket['pengirim'] ?>";
        let id_ticket = "<?= $detailTicket['id_ticket'] ?>";
        let file_attachment = "<?= $detailTicket['attachment'] ?>";
        // $('div.chat-body').scrollTop($('div.chat-body')[0].scrollHeight);
        let chat = '';
        // return false;
        if(pengirim === 'admin'){
            chat += `
            <li class="message-item me">
                <img src="<?= base_url(); ?>assets/assets/images/faces/face1.jpg" class="img-xs rounded-circle" alt="avatar">
                <div class="content">
                    <span  class="mb-2 subject"><strong class="border-bottom">Subject: ${subject}</strong></span>
                    <div class="message">
                        <div class="bubble">
                        `;
                        if(file_attachment != null && file_attachment != "" ){
                            attachment = file_attachment.split(".");
                            let imgFile = ["jpg", "jpeg", "png"];
                            if(imgFile.includes(attachment[1])){
                                chat += `<img src="<?= base_url(); ?>assets/resize_images/${file_attachment}" class="ml-0 text-right img-sm rounded" alt="avatar">`;
                            }else{
                                chat += `
                                    <div class="row d-flex justify-content-center align-items-center mb-2">
                                        <div class="col-md-2">
                                            <div class="iconFile rounded-circle">
                                                <i class="mdi mdi-file"></i>
                                            </div>
                                        </div>
                                        <div class="col-md-10">
                                            <p>${file_attachment}</p>
                                        </div>
                                    </div>
                                `;
                            }
                        }
                        chat +=`
                            <p>${isi_ticket}</p>
                            <span><i class="mdi mdi-check-all"></i></span>
                        </div>
                        <span>${datetime_ticket}</span>
                    </div>
                </div>
            </li>
            `;
        }else if(pengirim === 'customer'){
            chat += `
            <li class="message-item friend">
                <img src="<?= base_url(); ?>assets/assets/images/faces/face2.jpg" class="img-xs rounded-circle" alt="avatar">
                <div class="content">
                    <span  class="mb-2 subject"><strong class="border-bottom">Subject: ${subject}</strong></span>
                    <div class="message">
                        <div class="bubble">
                        `;
                        if(file_attachment != null && file_attachment != "" ){
                            attachment = file_attachment.split(".");
                            let imgFile = ["jpg", "jpeg", "png"];
                            if(imgFile.includes(attachment[1])){
                                chat += `<img src="<?= base_url(); ?>assets/resize_images/${file_attachment}" class="ml-0 text-right img-sm rounded" alt="avatar">`;
                            }else{
                                chat += `
                                    <div class="row d-flex justify-content-center align-items-center mb-2">
                                        <div class="col-md-2">
                                            <div class="iconFile rounded-circle">
                                                <i class="mdi mdi-file"></i>
                                            </div>
                                        </div>
                                        <div class="col-md-10">
                                            <p>${file_attachment}</p>
                                        </div>
                                    </div>
                                `;
                            }
                        }
                        chat +=`
                            <p>${isi_ticket}</p>
                            <span><i class="mdi mdi-check"></i></span>
                        </div>
                        <span>${datetime_ticket}</span>
                    </div>
                </div>
            </li>
            `;
        }
        $('div.chat-body ul.messages').html(chat);

        $.ajax({
            url: "<?= base_url("tickets/getTicketBalasan/"); ?>"+id_ticket,
            type: "GET",
            // data: {
            //     id_ticket : id_ticket,
            // },
            dataType: "JSON",
            success: function(data) {
                let attachment = "";
                let img = "";
                $.each(data, function( index, value ) {
                    if(value.pengirim == 'admin'){
                        chat += `
                        <li class="message-item me">
                            <img src="<?= base_url(); ?>assets/assets/images/faces/face1.jpg" class="img-xs rounded-circle" alt="avatar">
                            <div class="content">
                                <span  class="mb-2 subject"><strong class="border-bottom">Subject: ${subject}</strong></span>
                                <div class="message">
                                    <div class="bubble">
                                        `;
                                    if(value.attachment != null && value.attachment != "" ){
                                        attachment = value.attachment.split(".");
                                        let imgFile = ["jpg", "jpeg", "png"];
                                        if(imgFile.includes(attachment[1])){
                                            chat += `<img src="<?= base_url(); ?>assets/resize_images/${value.attachment}" class="ml-0 text-right img-sm rounded" alt="avatar">`;
                                        }else{
                                            chat += `
                                                <div class="row d-flex justify-content-center align-items-center mb-2">
                                                    <div class="col-md-2">
                                                        <div class="iconFile rounded-circle">
                                                            <i class="mdi mdi-file"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <p>${value.attachment}</p>
                                                    </div>
                                                </div>
                                            `;
                                        }
                                    }
                                        chat +=`
                                        <p>${value.komentar}</p>
                                        <span><i class="mdi mdi-check"></i></span>
                                    </div>
                                    <span>${formatTgl(value.tanggal_post)}</span>
                                </div>
                            </div>
                        </li>
                        `;
                    }else if(value.pengirim == 'customer'){
                        chat += `
                        <li class="message-item friend">
                            <img src="<?= base_url(); ?>assets/assets/images/faces/face2.jpg" class="img-xs rounded-circle" alt="avatar">
                            <div class="content">
                                <span  class="mb-2 subject"><strong class="border-bottom">Subject: ${subject}</strong></span>
                                <div class="message">
                                    <div class="bubble">
                                    `;
                                    if(value.attachment != null && value.attachment != "" ){
                                        attachment = value.attachment.split(".");
                                        let imgFile = ["jpg", "jpeg", "png"];
                                        if(imgFile.includes(attachment[1])){
                                            chat += `<img src="<?= base_url(); ?>assets/resize_images/${value.attachment}" class="ml-0 text-right img-sm rounded" alt="avatar">`;
                                        }else{
                                            chat += `
                                                <div class="row d-flex justify-content-center align-items-center mb-2">
                                                    <div class="col-md-2">
                                                        <div class="iconFile rounded-circle">
                                                            <i class="mdi mdi-file"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <p>${value.attachment}</p>
                                                    </div>
                                                </div>
                                            `;
                                        }
                                    }
                                        chat +=`
                                        <p>${value.komentar}</p>
                                        <span><i class="mdi mdi-check-all"></i></span>
                                    </div>
                                    <span>${formatTgl(value.tanggal_post)}</span>
                                </div>
                            </div>
                        </li>
                        `;
                    }
                });
                $('div.chat-body ul.messages').html(chat);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("Terjadi kesalahan server");
            }
        });
    }    

    // setInterval(function() {
    //     getTicketBalasan();   
	// }, 2000);
})


function formatTgl(datetime){
    let date = new Date(datetime);

    let day = date.getDay();
    let month = date.toLocaleString('default', { month: 'long' });
    let year = date.getFullYear();
    let hour = date.getHours();
    let minute = date.getMinutes();
    let second = date.getSeconds();
    let datetimeOrder = day + " " + month + " " + year + " " + hour + ":" + minute;
    return datetimeOrder;
}

// function updateStatusBacaAdmin(){
//     $.ajax({
//         url: "<?= base_url("tickets/updateStatusBacaAdmin/"); ?>"+id_ticket,
//         type: "GET",
//         dataType: "JSON",
//         success: function(data) {
            
//         },
//         error: function(jqXHR, textStatus, errorThrown) {
//             console.log("Terjadi kesalahan server");
//         }
//     });
// }
</script>