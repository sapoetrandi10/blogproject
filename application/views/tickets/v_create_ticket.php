<style>
    .note{
        font-size: .700rem;
    }

    .select2-selection__arrow b::before{
        display:none !important;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card text-center">
                <div class="card-body">
                    <h3 >Create Ticket</h3>
                    <p class="mb-4">Silahkan sampaikan permasalahan yang anda alami secara detail.</p>
                    <div class="row text-center ">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata('notif')) { echo $this->session->flashdata('notif');}?>
                        </div>
                    </div>
                    <form class="forms-sample text-left" action="<?= base_url("tickets/adminCreateTicket/"); ?>" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="subject">Subject</label>
                        <input type="text" class="form-control" id="subject" name="subject" autocomplete="off" placeholder="Subject" required>
                    </div>
                    <div class="form-group">
                        <label for="tujuan_pengirim">Tujuan</label>
                        <select class="js-example-basic-single w-100" id="tujuan_pengirim" name="tujuan_pengirim" required>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="isi_ticket">Isi ticket</label>
                        <textarea class="form-control" id="isi_ticket" name="isi_ticket" rows="10" placeholder="Sampaikan permasalahan yang anda alami secara detail pada kolom ini!" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="attachment">File upload</label>
                        <!-- <input type="file" name="img[]" class="file-upload-default"> -->
                        <input type="file" id="attachment" name="attachment" class="file-upload-default" accept=".jpg, .jpeg, .png, .pdf, .docx, .csv, .xlsx">
                        <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                        </span>
                        </div>
                        <p class="note text-danger">*file allowed : jpg, png, pdf, docx, csv, xlsx. max size: 5 Mb</p>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <a href="<?= base_url("tickets"); ?>">
                        <button type="button" class="btn btn-danger btn-round">
                            <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Cancle
                        </button>
                    </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$( document ).ready(function() {
    // CKEDITOR.replace( 'content_blog' );
    // CKEDITOR.replace( 'blog_og_description' );

    
    $.ajax({
        url: "<?= base_url("tickets/getAllMember"); ?>",
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            // console.log(data);
            $('#tujuan_pengirim').html(data);
            $('#tujuan_pengirim').select2({
                width: '100%'
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("Terjadi kesalahan server");
        }
    });
})
</script>