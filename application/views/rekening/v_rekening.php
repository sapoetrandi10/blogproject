<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">
    <div class="card  text-center">
        <div class="card-body">
            <h3>Table Rekening</h3>
            <div class="btn-tambah mb-3 text-left">
                <a href="<?= base_url("Rekening/vAddRekening/"); ?>">
                    <button type="button" class="btn btn-primary btn-round">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Rekening
                    </button>
                </a>
            </div>
            <div class="row text-center ">
                <div class="col-md-12">
                <?php
                if ($this->session->flashdata('notif')) {
                echo $this->session->flashdata('notif');
                }
                ?>
                </div>
            </div>
            <table class="table table-bordered" id="example">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">No Rekening</th>
                        <th scope="col">Nama Rekening</th>
                        <th scope="col">Nama Bank</th>
                        <th scope="col">Bank ID</th>
                        <th scope="col">Moota Key</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                
                </tbody>
            </table>
        </div>
    </div>
</div>
 
<script>
$(document).ready(function(){
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var table = $("#example").DataTable({
        initComplete: function() {
            var api = this.api();
            $('#mytable_filter input')
            .off('.DT')
            .on('input.DT', function() {
                api.search(this.value).draw();
            });
        },
        oLanguage: {
            sProcessing: "loading..."
        },
        processing: true,
        serverSide: true,
        "autoWidth": false,
        "scrollX": true,
        "scrollY": "300px",
        'ajax': {
            url: "<?= base_url().'rekening/getAllRekeningJson'?>",
            type: "POST"
        },
        // autoWidth: false,
        columns: [
        {"data": "id_rekening"},
        {"data": "no_rekening"},
        {"data": "nama_rekening"},
        {"data": "nama_bank"},
        {"data": "bank_id"},
        {"data": "moota_key"},
        {"data": "id_rekening", render : btn_action}
        ],
        rowCallback: function(row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            $('td:eq(0)', row).html();
        }
    });

    table.on( 'draw.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
});
    function btn_action(id_rekening ){
        return `
        <ul class="nav">
            <li class="nav-item ">
                <a href="<?= base_url("rekening/vEditRekening/"); ?>${id_rekening}" onclick="return confirm('Anda yakin ingin edit administrator ini?')" class="nav-link text-success">
                    <i class="mdi mdi-tooltip-edit">Edit</i>
                </a>
            </li>
            <li class="nav-item ">
                <a href="<?= base_url("rekening/deleteRekening/"); ?>${id_rekening}" class="nav-link text-danger" onclick="return confirm('Anda yakin ingin menghapus data rekening ini?')">
                    <i class="mdi mdi-delete">Delete</i>
                </a>
            </li>
        </ul>
        `;
    }
</script>