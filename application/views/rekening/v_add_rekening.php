<style>
    .wraper{
        min-height: 668px;
    }
</style>

<div class="wraper ">
    <div class="row text-center ">
        <div class="col-md-4 offset-md-4">
        <?php
        if ($this->session->flashdata('notif')) {
        echo $this->session->flashdata('notif');
        }
        ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-md-1 grid-margin stretch-card">
            <div class="card  text-center">
            <div class="card-body">
                <h3 class="">Form Add Rekening</h3>
                <form class="forms-sample text-left" action="<?= base_url('rekening/addRekening')?>" method="POST">
                    <div class="form-group">
                        <label for="no_rekening">No Rekening</label>
                        <input type="number" class="form-control" id="no_rekening" name="no_rekening" autocomplete="off" placeholder="No rekening" required>
                    </div>
                    <div class="form-group">
                        <label for="nama_rekening">Nama Rekening</label>
                        <input type="text" class="form-control" id="nama_rekening" name="nama_rekening" autocomplete="off" placeholder="Nama Rekening" maxlength="20" required>
                    </div>
                    <div class="form-group">
                        <label for="nama_bank">Nama Bank</label>
                        <input type="text" class="form-control" id="nama_bank" name="nama_bank" autocomplete="off" placeholder="Nama Bank" required>
                    </div>
                    <div class="form-group">
                        <label for="bank_id">Id Bank</label>
                        <input type="text" class="form-control" id="bank_id" name="bank_id" autocomplete="off" placeholder="Id Bank" required>
                    </div>
                    <div class="form-group">
                        <label for="moota_key">Moota Key</label>
                        <input type="text" class="form-control" id="moota_key" name="moota_key" autocomplete="off" placeholder="Moota Key" required>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary mr-2">Add</button>
                        <a href="<?= base_url() ?>rekening">
                            <button type="button" class="btn btn-light">Cancel</button>
                        </a>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>