<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style>
</style>

<div class="container">
    <div class="card mb-3">
        <div class="card-body">
            <h3>Settings</h3>
        </div>
    </div>

    <div class="card text-dark">
        <div class="card-header bg-warning"><strong>Store Integration</strong></div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-1 d-flex justify-content-center align-items-center">
                    <i data-feather="key"></i>
                </div>
                <div class="col-md-9">
                    <h4>Store Integration</h4>
                    <p>Check all integration here.</p>
                </div>
                <div class="col-md-2 d-flex justify-content-center align-items-center">
                    <a href="<?= base_url("integrations"); ?>">
                        <button type="button" class="btn btn-info text-white btn-sm btn-round">
                            <!-- <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>  -->
                            View store integrations
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="col-md-12"> -->
        <a href="<?= base_url("dashboard"); ?>">
            <button type="button" class="btn btn-danger text-white btn-md btn-round mt-3">
                <i class="mdi mdi-arrow-left"></i>
                back
            </button>
        </a>
    <!-- </div> -->
</div>