<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    .detailOrder{
        width: 115px;
    }
</style>
<div class="container">
    <div class="card  text-center">
        <div class="card-body">
            <h1>List Order</h1>
            <div class="row text-center ">
                <div class="col-md-12">
                <?php
                if ($this->session->flashdata('notif')) {
                echo $this->session->flashdata('notif');
                }
                ?>
                </div>
            </div>
            <table class="table table-bordered" id="example">
                <thead>
                    <tr>
                    <th scope="col">No</th>
                    <th scope="col">ID order</th>
                    <th scope="col">Tgl order</th>
                    <th scope="col">Nama produk</th>
                    <th scope="col">Status order</th>
                    <th scope="col">Total order</th>
                    <th scope="col">Status pembayaran</th>
                    <th scope="col">Bukti pembayaran</th>
                    <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal buktiTf -->
<!-- <div class="modal fade" id="modalBuktiTf" tabindex="3" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Bukti transfer</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body imgBuktiTf">
        
      </div>
    </div>
  </div>
</div> -->

<!-- Modal -->
<!-- <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Modal title</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div> -->


<div class="modal fade" id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalToggleLabel">Modal 1</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body modal1">
        Show a second modal and hide this one with the button below.
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModalToggle2" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalToggleLabel2">Modal 2</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body modal2">
        Hide this modal and show the first with the button below.
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" data-bs-target="#exampleModalToggle" data-bs-toggle="modal">Back to detail Order</button>
      </div>
    </div>
  </div>
</div>
<!-- <a class="btn btn-primary" data-bs-toggle="modal" href="#exampleModalToggle" role="button">Open first modal</a> -->


<!-- <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> -->
<script>
$(document).ready(function(){
    let imgBuktiTf = ``;
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var table = $("#example").DataTable({
        initComplete: function() {
            var api = this.api();
            $('#mytable_filter input')
            .off('.DT')
            .on('input.DT', function() {
                api.search(this.value).draw();
            });
        },
        oLanguage: {
            sProcessing: "loading..."
        },
        processing: true,
        serverSide: true,
        "autoWidth": false,
        "scrollX": true,
        "scrollY": "300px",
        'ajax': {
            url: "<?= base_url().'orders/getAllOrderJson'?>",
            type: "POST"
        },
        columns: [
        {"data": "id_order"},
        {"data": "id_order"},
        {"data": "datetime_order", render : formatTglOrder},
        {"data": "nama_produk"},
        {"data": "status_order"},
        {"data": "total_order"},
        {"data": "status_pembayaran"},
        {"data": "bukti_transfer", render : buktiTransfer},
        {"data": "id_order", render : orderAction}
        ],
        rowCallback: function(row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            $('td:eq(0)', row).html();
        }
    });

    table.on( 'draw.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
});
    function buktiTransfer(bukti_transfer){
        if(bukti_transfer == ''){
            return `belum melakukan konfirmasi`;
        }else{
            // return bukti_transfer;
            let img = `<img src="<?= base_url().'assets/resize_images/'?>${bukti_transfer}" class="rounded mx-auto d-block" alt="..." style="width:100%;">`;
            $('.modal2').html(img);
            $('#exampleModalToggle2 .modal-footer').hide();
            $('#exampleModalToggle2 .modal-title').text('Bukti transfer');
            
            return `
            <button class="btn btn-xs btn-primary" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal"><i class="mdi mdi-image"> lihat bukti transfer</i></button>
            `;
            // <a href="#" class="nav-link text-secondary" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal">
            //     <i class="mdi mdi-image"> lihat bukti transfer</i>
            // </a>
            
        }
    }

    function orderAction(id_order){
        return `
        <ul class="nav">
            <li class="nav-item ">
                <button type="button" class="btn btn-light btn-xs mr-2 p-0" onclick="detailOrder(${id_order})" data-bs-toggle="modal" href="#exampleModalToggle">
                    <i  class="mdi mdi-eye"></i>
                </button>
            </li>
            <li class="nav-item ">
                <div class="dropdown">
                    <button class="btn btn-secondary btn-xs dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Confirm Order
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="<?= base_url("orders/updateStatusOrder/terima/"); ?>${id_order}" onclick="return confirm('Anda yakin ingin mengubah status order ini menjadi Terima?')">Acc</a>
                        <a class="dropdown-item" href="<?= base_url("orders/updateStatusOrder/reset/"); ?>${id_order}" onclick="return confirm('Anda yakin ingin reset pembayaran order ini?')">Reset pembayaran</a>
                    </div>
                </div>
            </li>
        </ul>
        `;
        // <a class="dropdown-item" href="<?= base_url("orders/updateStatusOrder/cancel/"); ?>${id_order}" onclick="return confirm('Anda yakin ingin mengubah status order ini menjadi Cancel?')">Cancel</a>
    }

    function formatTglOrder(datetime){
        let date = new Date(datetime);

        let day = date.getDate();
        let month = date.toLocaleString('default', { month: 'long' });
        let year = date.getFullYear();
        let hour = date.getHours();
        let minute = date.getMinutes();
        let second = date.getSeconds();
        let datetimeOrder = day + " " + month + " " + year + " " + hour + ":" + minute + ":" + second;
        return datetimeOrder;
    }

    function detailOrder(id_order){
        let url = "<?= base_url("orders/getOrderById/"); ?>"+id_order;
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            // async: true,
            success: function(data) {
                // console.log(data.order.bukti_transfer)
                let buktiTransfer = `
                <button class="btn btn-xs btn-primary" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal"><i class="mdi mdi-image"> lihat bukti transfer</i></button>
                `;
                if( data.order.bukti_transfer === ""){
                    buktiTransfer = `belum melakukan konfirmasi`;
                }

                let html = `
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="wrapper mb-4">
                                    <h6>ID ORDER</h6>
                                    <p>${data.order.id_order}</p>
                                </div>
                                <div class="wrapper mb-4">
                                    <h6>TGL ORDER</h6>
                                    <p>${formatTglOrder(data.order.datetime_order)}</p>
                                </div>
                                <div class="wrapper mb-4">
                                    <h6>TGL EXPIRED</h6>
                                    <p>${formatTglOrder(data.order.datetime_expired)}</p>
                                </div>
                                <div class="wrapper mb-4">
                                    <h6>TGL KONFIRMASI PEMBAYARAN</h6>
                                    <p>${formatTglOrder(data.order.datetime_konfirmasi_pembayaran)}</p>
                                </div>
                                <div class="wrapper mb-4">
                                    <h6>TOTAL ORDER</h6>
                                    <p>${data.order.total_order}</p>
                                </div>
                                <div class="wrapper mb-4">
                                    <h6>KODE UNIK</h6>
                                    <p>${data.order.kode_unik}</p>
                                </div>
                                <div class="wrapper mb-4">
                                    <h6>DISKON</h6>
                                    <p>${data.order.diskon}</p>
                                </div>
                                <div class="wrapper mb-4">
                                    <h6>STATUS CANCEL</h6>
                                    <p>${data.order.status_cancel}</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="wrapper mb-4">
                                    <h6>ID PRODUK</h6>
                                    <p>${data.order.id_produk}</p>
                                </div>
                                <div class="wrapper mb-4">
                                    <h6>NAMA PRODUK</h6>
                                    <p>${data.order.nama_produk}</p>
                                </div>
                                <div class="wrapper mb-4">
                                    <h6>STATUS PEMBAYARAN</h6>
                                    <p>${data.order.status_pembayaran}</p>
                                </div>
                                <div class="wrapper mb-4">
                                    <h6>BUKTI TRANSFER</h6>
                                    ${buktiTransfer}
                                </div>
                                <div class="wrapper mb-4">
                                    <h6>NO REKENING</h6>
                                    <p>${data.order.no_rek}</p>
                                </div>
                                <div class="wrapper mb-4">
                                    <h6>ATAS NAMA</h6>
                                    <p>${data.order.atas_nama}</p>
                                </div>
                                <div class="wrapper mb-4">
                                    <h6>BANK PENGIRIM</h6>
                                    <p>${data.order.dari_bank}</p>
                                </div>
                                <div class="wrapper mb-4">
                                    <h6>BANK PENERIMA</h6>
                                    <p>${data.order.ke_bank}</p>
                                </div>                    
                            </div>
                        </div>
                    </div>
                `;
                $('.modal-title').text('Detail Order');
                $('#exampleModalToggle2 .modal-title').text('Bukti transfer');
                $('#exampleModalToggle2 .modal-footer').show();
                $('.modal1').html(html);
            }
        });
    }
</script>

