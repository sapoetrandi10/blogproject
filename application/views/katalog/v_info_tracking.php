<style>
    .trackOrder{
        margin-top: 70px;
    }
    /* .card-body{
        min-height: 250px;        
    } */
    #content .timeline{
        /* box-sizing: border-box; */
        /* margin-top:10px; */
        max-width: 70%;
        position: relative;
        left: 40px;
        border-left: 3px solid #FF8E00;
        background: rgba(255,159,0,.09);

    }

    .timeline .event:after{
        box-shadow: 0 0 0 3px #FF8E00;
    }
</style>
<div class="container trackOrder">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body text-center">
                    <h4 ><strong>TRACKING ORDER #<?= $dataTracking['id_order'] ?></strong></h4>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-5">
            <div class="card h-100">
                <div class="card-body text-center">
                    <h5 class="border-bottom pb-2"><strong>ORDER STATUS</strong></h5>
                    <div id="content">
                        <ul class="timeline mt-3">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body text-left">
                            <h5 class="border-bottom text-center pb-2 mb-2"><strong>DETAIL PRODUK</strong></h5>
                            <p><strong>Produk : </strong><?= strtoupper($dataTracking['nama_produk']) ?></p>
                            <p><strong>Jumlah Pesanan : </strong> 1</p>
                            <p><strong>Total Harga : </strong>Rp. <?= number_format($dataTracking['total_order']) ?>,-</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card mt-3">
                        <div class="card-body text-left">
                            <h5 class="border-bottom text-center pb-2 mb-2"><strong>DETAIL PEMBAYARAN</strong></h5>
                            <p><strong>Status Pembayaran : </strong><?= strtoupper($dataTracking['status_pembayaran']) ?></p>
                            <p><strong>Dari Bank : </strong><?= strtoupper($dataTracking['dari_bank']) ?></p>
                            <p><strong>Ke Bank : </strong><?= strtoupper($dataTracking['ke_bank']) ?></p>
                            <p><strong>Nominal  : </strong>Rp. <?= number_format($dataTracking['total_order']) ?>,-</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card mt-3   ">
                        <div class="card-body text-left">
                            <h5 class="border-bottom pb-2 text-center mb-2"><strong>DETAIL CUSTOMER</strong></h5>
                            <p><strong>Nama Penerima : </strong><?= strtoupper($dataTracking['nama_pemesan']) ?></p>
                            <p><strong>Nomor Penerima : </strong><?= $dataTracking['no_whatsapp'] ?></p>
                            <p><strong>Alamat Penerima : </strong><?= strtoupper($dataTracking['kota']) ?></p>
                            <p><strong>Email Penerima : </strong><?= $dataTracking['email'] ?></p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<script>
    let status_order = "<?= $dataTracking['status_order'] ?>";
    let timeline = ``;
    if(status_order == 'pending'){
        timeline = `
        <li class="event notifPending"">
            <h3>Belum melakukan pembayaran</h3>
        </li>
        `;
    }else if(status_order == 'process'){
        timeline = `
        <li class="event notifPending"">
            <h3>Belum melakukan pembayaran</h3>
        </li>
        <li class="event notifProses">
            <h3>Diproses</h3>
            <p>Pesanan anda sedang diproses.</p>
        </li>
        `;
    }else if(status_order == 'complete'){
        timeline = `
        <li class="event notifPending"">
            <h3>Belum melakukan pembayaran</h3>
        </li>
        <li class="event notifProses">
            <h3>Diproses</h3>
            <p>Pesanan anda sedang diproses.</p>
        </li>
        <li class="event notifKirim"">
            <h3>Dikirim</h3>
            <p>Pesanan anda sedang dikirim.</p>    
        </li>
        `;
    }else{
        timeline = `
        <li class="event notifPending"">
            <h3>Belum melakukan pembayaran</h3>
        </li>
        <li class="event notifProses">
            <h3>Diproses</h3>
            <p>Pesanan anda sedang diproses.</p>
        </li>
        <li class="event notifKirim">
            <h3>Dikirim</h3>
            <p>Pesanan anda sedang dikirim.</p>    
        </li>
        `;

    }
    $('.timeline').html(timeline);
    $(document).ready(function(){
    })
</script>