<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    .katalog{
        margin-top: 45px;
    }

    .card{
        width: 300px;
    }

    .btnCart{
        width: 100%;
    }
</style>
<div class="container katalog">
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body produkCard">
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let harga = "";
    let harga_coret = "";
    // d-flex flex-row justify-content-start flex-wrap produkContainer
    let url = "<?= storeUrl().'katalog/getAllProduk'?>";
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        success: function(data) {
            let limages = "";
            let thumbnail_produk = "";
            let produkCard = `<div class="text-left d-flex justify-content-start flex-wrap">`;
            if(data.length == 0){
                produkCard = `
                    <div><strong>Produk tidak tersedia!!</strong></div>
                `;
                $("div.produkCard").html(produkCard);
            }else{
                
                $.each(data, function( index, value ) {
                    if(value.thumbnail_produk == ""){
                        limages = `
                            <li data-target="#carousel${value.id_produk}" data-slide-to="0" class="active"></li>
                        `;
                        thumbnail_produk = `
                            <div class="carousel-item active">
                                <img src="<?= base_url(); ?>assets/assets/images/no-image.png" class="d-block w-100" alt="...">
                            </div>
                        `;
                    }else{
                        
                        limages = "";
                        thumbnail_produk = "";
                        let img_thumbnail_produk = JSON.parse(value.thumbnail_produk);
                        $.each(img_thumbnail_produk, function( index, img ) {
                            if(index === 0){
                                limages += `
                                    <li data-target="#carousel${value.id_produk}" data-slide-to="${index}" class="active"></li>
                                `;
                                thumbnail_produk += `
                                    <div class="carousel-item active">
                                        <img src="<?= base_url(); ?>assets/resize_images/${img}" class="d-block w-100" alt="...">
                                    </div>
                                `;

                            }else{
                                limages += `
                                    <li data-target="#carousel${value.id_produk}" data-slide-to="${index}"></li>
                                `;
                                thumbnail_produk += `
                                    <div class="carousel-item">
                                        <img src="<?= base_url(); ?>assets/resize_images/${img}" class="d-block w-100" alt="...">
                                    </div>
                                `;
                            }
                        });
                    }

                    harga =  new Intl.NumberFormat("id-ID", {
                        style: "currency",
                        currency: "IDR"
                    }).format(value.harga);

                    harga_coret =  new Intl.NumberFormat("id-ID", {
                        style: "currency",
                        currency: "IDR"
                    }).format(value.harga_coret);

                    produkCard += `
                    <div class="card mb-3 mr-3">
                        <div id="carousel${value.id_produk}" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                ${limages}
                            </ol>
                            <div class="carousel-inner" style="height: 200px;">
                                ${thumbnail_produk}
                            </div>
                            <a class="carousel-control-prev" href="#carousel${value.id_produk}" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel${value.id_produk}" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title mb-0">${value.nama_produk} <small><i>${value.kategori}</i></small></h5>
                            <small class="text-danger"><i><s>${harga_coret}</s></i></small>
                            <p class="card-text">${harga}</p>
                            <p class="card-text">${value.keterangan_produk}</p>
                            <div class="row">
                                <div class="col-md-12 col-sm-6 col-xs-6 p-1">
                                    <a href="<?= storeUrl("katalog/checkoutPage/"); ?>${value.id_produk}">
                                        <button type="button" class="btn btn-primary" style="width: 100%">
                                            <i class="mdi mdi-cart-plus"> Add to cart</i>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                });
                produkCard += `</div>`;
                $("div.produkCard").html(produkCard);
            }
        }
    });

    $("#search").keyup(function(e){ 
        // komentar = $('#search').val();
        // komentar = $.trim(komentar);

        var code = e.key;
        if(code==="Enter") e.preventDefault();
        if(code==="Enter"){

            let keyword = $('#search').val();
            let url = "<?= storeUrl().'katalog/getProdukByKeyword/'?>";
            console.log(url);
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    keyword : keyword
                },
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    let limages = "";
                    let thumbnail_produk = "";
                    let produkCard = `<div class="text-left d-flex justify-content-start flex-wrap">`;
                    if(data.length == 0){
                        produkCard = `
                            <div><strong>Produk tidak tersedia!!</strong></div>
                        `;
                        $("div.produkCard").html(produkCard);
                    }else{
                        
                        $.each(data, function( index, value ) {
                            if(value.thumbnail_produk == ""){
                                limages = `
                                    <li data-target="#carousel${value.id_produk}" data-slide-to="0" class="active"></li>
                                `;
                                thumbnail_produk = `
                                    <div class="carousel-item active">
                                        <img src="<?= base_url(); ?>assets/assets/images/no-image.png" class="d-block w-100" alt="...">
                                    </div>
                                `;
                            }else{
                                
                                limages = "";
                                thumbnail_produk = "";
                                let img_thumbnail_produk = JSON.parse(value.thumbnail_produk);
                                $.each(img_thumbnail_produk, function( index, img ) {
                                    if(index === 0){
                                        limages += `
                                            <li data-target="#carousel${value.id_produk}" data-slide-to="${index}" class="active"></li>
                                        `;
                                        thumbnail_produk += `
                                            <div class="carousel-item active">
                                                <img src="<?= base_url(); ?>assets/resize_images/${img}" class="d-block w-100" alt="...">
                                            </div>
                                        `;

                                    }else{
                                        limages += `
                                            <li data-target="#carousel${value.id_produk}" data-slide-to="${index}"></li>
                                        `;
                                        thumbnail_produk += `
                                            <div class="carousel-item">
                                                <img src="<?= base_url(); ?>assets/resize_images/${img}" class="d-block w-100" alt="...">
                                            </div>
                                        `;
                                    }
                                });
                            }

                            harga =  new Intl.NumberFormat("id-ID", {
                                style: "currency",
                                currency: "IDR"
                            }).format(value.harga);

                            harga_coret =  new Intl.NumberFormat("id-ID", {
                                style: "currency",
                                currency: "IDR"
                            }).format(value.harga_coret);

                            produkCard += `
                            <div class="card mb-3 mr-3">
                                <div id="carousel${value.id_produk}" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        ${limages}
                                    </ol>
                                    <div class="carousel-inner" style="height: 200px;">
                                        ${thumbnail_produk}
                                    </div>
                                    <a class="carousel-control-prev" href="#carousel${value.id_produk}" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel${value.id_produk}" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title mb-0">${value.nama_produk} <small><i>${value.kategori}</i></small></h5>
                                    <small class="text-danger"><i><s>${harga_coret}</s></i></small>
                                    <p class="card-text">${harga}</p>
                                    <p class="card-text">${value.keterangan_produk}</p>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-6 col-xs-6 p-1">
                                            <a href="<?= storeUrl("katalog/checkoutPage/"); ?>${value.id_produk}">
                                                <button type="button" class="btn btn-primary" style="width: 100%">
                                                    <i class="mdi mdi-cart-plus"> Add to cart</i>
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                        });
                        produkCard += `</div>`;
                        $("div.produkCard").html(produkCard);
                    }
                }
            });
        }
    });

    $('.searchBtn').on('click', function(){
        let keyword = $('#search').val();
        let url = "<?= storeUrl().'katalog/getProdukByKeyword/'?>";
        console.log(url);
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                keyword : keyword
            },
            dataType: 'json',
            success: function(data) {
                console.log(data);
                let limages = "";
                let thumbnail_produk = "";
                let produkCard = `<div class="text-left d-flex justify-content-start flex-wrap">`;
                if(data.length == 0){
                    produkCard = `
                        <div><strong>Produk tidak tersedia!!</strong></div>
                    `;
                    $("div.produkCard").html(produkCard);
                }else{
                    
                    $.each(data, function( index, value ) {
                        if(value.thumbnail_produk == ""){
                            limages = `
                                <li data-target="#carousel${value.id_produk}" data-slide-to="0" class="active"></li>
                            `;
                            thumbnail_produk = `
                                <div class="carousel-item active">
                                    <img src="<?= base_url(); ?>assets/assets/images/no-image.png" class="d-block w-100" alt="...">
                                </div>
                            `;
                        }else{
                            
                            limages = "";
                            thumbnail_produk = "";
                            let img_thumbnail_produk = JSON.parse(value.thumbnail_produk);
                            $.each(img_thumbnail_produk, function( index, img ) {
                                if(index === 0){
                                    limages += `
                                        <li data-target="#carousel${value.id_produk}" data-slide-to="${index}" class="active"></li>
                                    `;
                                    thumbnail_produk += `
                                        <div class="carousel-item active">
                                            <img src="<?= base_url(); ?>assets/resize_images/${img}" class="d-block w-100" alt="...">
                                        </div>
                                    `;

                                }else{
                                    limages += `
                                        <li data-target="#carousel${value.id_produk}" data-slide-to="${index}"></li>
                                    `;
                                    thumbnail_produk += `
                                        <div class="carousel-item">
                                            <img src="<?= base_url(); ?>assets/resize_images/${img}" class="d-block w-100" alt="...">
                                        </div>
                                    `;
                                }
                            });
                        }

                        harga =  new Intl.NumberFormat("id-ID", {
                            style: "currency",
                            currency: "IDR"
                        }).format(value.harga);

                        harga_coret =  new Intl.NumberFormat("id-ID", {
                            style: "currency",
                            currency: "IDR"
                        }).format(value.harga_coret);

                        produkCard += `
                        <div class="card mb-3 mr-3">
                            <div id="carousel${value.id_produk}" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    ${limages}
                                </ol>
                                <div class="carousel-inner" style="height: 200px;">
                                    ${thumbnail_produk}
                                </div>
                                <a class="carousel-control-prev" href="#carousel${value.id_produk}" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carousel${value.id_produk}" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title mb-0">${value.nama_produk} <small><i>${value.kategori}</i></small></h5>
                                <small class="text-danger"><i><s>${harga_coret}</s></i></small>
                                <p class="card-text">${harga}</p>
                                <p class="card-text">${value.keterangan_produk}</p>
                                <div class="row">
                                    <div class="col-md-12 col-sm-6 col-xs-6 p-1">
                                        <a href="<?= storeUrl("katalog/checkoutPage/"); ?>${value.id_produk}">
                                            <button type="button" class="btn btn-primary" style="width: 100%">
                                                <i class="mdi mdi-cart-plus"> Add to cart</i>
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                    });
                    produkCard += `</div>`;
                    $("div.produkCard").html(produkCard);
                }
            }
        });
    })
</script>