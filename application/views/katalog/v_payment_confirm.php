<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    .note{
        font-size: .700rem;
    }

    .checkoutPage {
        width: 75%; 
        margin: auto;
    }

    .select2-selection__arrow b::before{
        display:none !important;
    }

    @media (max-width: 768px) {
        .checkoutPage {
            width: 100%; 
            margin: auto;
        }
    }
</style>
<div class="row checkoutPage">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body text-center">
                <h3 class="mb-3">Konfirmasi pembayaran</h3>
                <h5>Silahkan isi form berikut untuk konfirmasi pembayaran</h5>
                <p>INVOICES #<span class="noInvoice"></span></p>
                <h5>TOTAL INVOICES: <span class="totalInvoice"></span>,-</h5>
                
                <div class="row mt-3">
                    <div class="col-md-8 offset-md-2">
                        <div class="row text-center ">
                            <div class="col-md-12">
                                <?php if ($this->session->flashdata('notif')) {
                                echo $this->session->flashdata('notif');}
                                ?>
                            </div>
                        </div>
                        <form class="forms-sample text-left" id="" method="post" enctype="multipart/form-data" action="<?= storeUrl().'katalog/confirmPayment'?>">
                            <input type="text" class="form-control" id="id_order" name="id_order" autocomplete="off">
                            <div class="form-group">
                                <label for="nama_bank">Nama Bank</label>
                                <input type="text" class="form-control" id="nama_bank" name="nama_bank" autocomplete="off" placeholder="Nama Bank" required>
                            </div>
                            <div class="form-group">
                                <label for="no_rek">No rekening</label>
                                <input type="number" class="form-control" id="no_rek" name="no_rek" min="0" autocomplete="off" placeholder="No rekening" required>
                            </div>
                            <div class="form-group">
                                <label for="atas_nama">Nama pemilik rekening</label>
                                <input type="text" class="form-control" id="atas_nama" name="atas_nama" autocomplete="off" placeholder="Nama pemilik rekening" required>
                            </div>
                            <div class="form-group">
                                <label for="rekening_pembayaran">Rekening Tujuan</label>
                                <select class="js-example-basic-single w-100" id="rekening_pembayaran" name="rekening_pembayaran" required>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="tgl_transfer">Tanggal transfer</label>
                                <div class="input-group date datepicker" id="datePickerExample">
                                    <input type="text" id="tgl_transfer" name="tgl_transfer" class="form-control" required><span class="input-group-addon"><i data-feather="calendar"></i></span>
                                </div>
                            </div>
                            <div class="form-group bukti_pembayaran">
                            <label for="">Upload bukti transfer</label>
                                <input type="file" class="file-upload-default" id="bukti_transfer" name="bukti_transfer" accept=".jpeg, .jpg, .png, .pdf">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                                <p class="note text-danger">*bukti pembayaran wajib di upload</p>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary mt-3 text-center btnConfirm">Konfirmasi pembayaran</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){

    let idOrder = "<?= $idOrder ?>";
    $.ajax({
        url: "<?= storeUrl("katalog/getOrderById/"); ?>"+idOrder,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            if(data.order.status_pembayaran === 'sudah'){
                let decodeIdOrder = "<?= $decodeIdOrder ?>";
                let redirect = `<?= storeUrl("katalog/thankYouPage/?cp="); ?>${decodeIdOrder}`;
                window.location.replace(redirect);
                return false;
            }

            let selectBank = `<option value=''>-- Silahkan pilih rekening --</option>`;
            $.each(data.bank, function( index, value ) {
                selectBank += `
                    <option value="${value.no_rekening}">${value.no_rekening} - ${value.nama_bank} - ${value.nama_rekening}</option>
                `;
            });

            $('#rekening_pembayaran').html(selectBank);
            $('#rekening_pembayaran').select2({
                width: '100%'
            });
            
            $('#id_order').val(data.order.id_order).prop('hidden', true);
            total_order =  new Intl.NumberFormat("id-ID", {
                style: "currency",
                currency: "IDR"
            }).format(data.order.total_order).replace(/(\.|,)00$/g, '');
            $('.noInvoice').html(data.order.id_order);
            $('.totalInvoice').html(total_order);
            if(data.order.bukti_pembayaran == 'on'){
                $('.bukti_pembayaran').show();
                $('#bukti_transfer').attr('required', true);
                // $('.file-upload-info').attr('required', true);
                $('.btnConfirm').on('click', function(e){
                    let file = $('#bukti_transfer').val();
                    if(file === ''){
                        e.preventDefault();
                        alert("Silahkan upload bukti pembayaran terlebih dahulu");
                    }
                })
            }else{
                $('.bukti_pembayaran').hide();
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("Terjadi kesalahan server");
        }
    });

    // $('.btnConfirm').on('click', function(e){
    //     e.preventDefault();
    //     console.log($('bukti_transfer').val());

    // });
});
</script>