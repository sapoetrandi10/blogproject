<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    .textKonfirmasi{
        min-height: 300px;
    }

    .checkoutPage{
        width: 75%; 
        min-height: 600px; 
        margin: auto;
    }
</style>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body checkoutPage text-center ">
                <h3 class="mb-3">Konfirmasi pembayaran berhasil</h3>
                <p>Terimakasih telah melakukan konfirmasi pembayaran</p>
                <h5>INVOICES #<span class="idOrder"><span></h5>
                <div class="card text-dark bg-light my-3">
                    <div class="card-body text-center textKonfirmasi">
                    </div>
                </div>
                <h5>Mau dikabarin lewat email kalau ada info penawaran terbaru? Klik tombol subscribe di bawah ini!</h5>
                <a href="https://m.me/RicoHuangAlona?ref=w10837788" class="btn btn-danger btn-block" style="width: 30%; margin: auto;">
                    <!-- <b> -->
                    <i class="mdi mdi-bell-ring">Subscribe</i> 
                </a>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){

    let idOrder = "<?= $idOrder ?>";
    $.ajax({
        url: "<?= storeUrl("katalog/getOrderById/"); ?>"+idOrder,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('.idOrder').html(data.order.id_order);
            $('.textKonfirmasi').html(data.order.text_konfirmasi_pembayaran);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("Terjadi kesalahan server");
        }
    });
});
</script>