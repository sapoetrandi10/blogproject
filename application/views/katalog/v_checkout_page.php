<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    .checkoutPage{
        width: 50%; 
        margin: auto;
    }
    .carousel{
        max-width: 50%;
        height: 350px;
        margin: auto;

    }
    .checkoutPage{
        width: 75%; 
        margin: auto;
    }
    .btnBeli{
        width: 75%; 
        margin: auto;
    }
    
    .carousel-control-prev-icon, .carousel-control-next-icon{
        color: green;
    }
    .select2-selection__arrow b::before{
        display:none !important;
    }

    @media (max-width: 768px) {
        .checkoutPage{
            width: 100%; 
            margin: auto;
        }

        .carousel-inner{
            min-height: 200px;
        }

        .btnBeli{
            width: 100%; 
            margin: auto;
        }
    }
</style>
<div class="row mt-4">
    <div class="col-md-8 offset-md-2">
        <div class="card mb-4 text-dark">
            <div class="card-header">
                <h4 class="py-3">Produk</h4>
            </div>
            <div class="card-body">
            <div id="carouselExampleIndicators" class="carousel slide mb-5" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="<?= base_url(); ?>assets/assets/images/no-image.png" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="<?= base_url(); ?>assets/assets/images/no-image.png" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="<?= base_url(); ?>assets/assets/images/no-image.png" class="d-block w-100" alt="...">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <h5 class="card-title">Keterangan Produk</h5>
            <p class="card-text keterangan">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8 offset-md-2">
        <div class="card text-dark">
            <div class="card-header">
                <h5>Informasi Pemesanan *</h5>
                <p>silahkan isi data berikut untuk melanjutkan pemesanan.</p>    
            </div>
            <div class="card-body">
                <form class="cmxform" id="checkoutForm" method="post" action="<?= storeUrl().'katalog/createOrder'?>">
                    <fieldset>
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input id="name" class="form-control" name="name" type="text" required>
                        </div>
                        <div class="form-group">
                            <label for="no_whatsapp">No Whatsapp</label>
                            <input id="no_whatsapp" class="form-control" name="no_whatsapp" min="0" type="number" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input id="email" class="form-control" name="email" type="email" required>
                        </div>
                        <div class="form-group">
                            <label for="kota">Kota</label>
                            <select class="js-example-basic-single w-100" id="kota" name="kota" required>
                            </select>
                        </div>
                        <div class="form-group">
                            <!-- <label for="kode_unik">Kode unik</label> -->
                            <input id="kode_unik" class="form-control" name="kode_unik" type="hidden" required>
                            <input id="total_order" class="form-control" name="total_order" type="hidden" required>
                            <input id="nama_produk" class="form-control" name="nama_produk" type="hidden" required>
                            <input id="id_produk" class="form-control" name="id_produk" type="hidden" required>
                        </div>
                    </fieldset>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8 offset-md-2">
    <div class="card text-dark ">
            <div class="card-header">
                <h5>Rincian Pesanan</h5>
            </div>
            <div class="card-body">
                <div class="row mb-3">
                    <div class="col-md-6 col-6 text-left">
                        <h6 class="text-left namaProduk">Produk 1</h6>
                    </div>
                    <div class="col-md-6 col-6 text-right">
                        <p><i><s class="card-text text-danger hargaCoret">Rp. 5454</s></i></p>
                        <p class="card-text harga">Rp. 5454</p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-6 col-6 text-left">
                        <p class="card-text">Kode unik</p>
                        <h6>Total Harga</h6>
                    </div>
                    <div class="col-md-6 col-6 text-right">
                        <p class="card-text kodeUnik">454</p>
                        <h6 class="card-text totalHarga">Rp. 5454</h6>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-warning btnBeli">
                            <i data-feather="shopping-cart"></i> Beli Sekarang
                        </button>
                    </div>
                </div>
                <p class="mt-2 text-center">
                    <svg style="width: 20px;height: auto;position: relative;top: 0px;margin-right: 10px;" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="shield-check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class=""><path style="fill: #2dcb73;" d="M466.5 83.7l-192-80a48.15 48.15 0 0 0-36.9 0l-192 80C27.7 91.1 16 108.6 16 128c0 198.5 114.5 335.7 221.5 380.3 11.8 4.9 25.1 4.9 36.9 0C360.1 472.6 496 349.3 496 128c0-19.4-11.7-36.9-29.5-44.3zm-47.2 114.2l-184 184c-6.2 6.2-16.4 6.2-22.6 0l-104-104c-6.2-6.2-6.2-16.4 0-22.6l22.6-22.6c6.2-6.2 16.4-6.2 22.6 0l70.1 70.1 150.1-150.1c6.2-6.2 16.4-6.2 22.6 0l22.6 22.6c6.3 6.3 6.3 16.4 0 22.6z" class=""></path></svg>
                    Payments are secure and encrypted
                </p>
            </form>
            </div>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-4 offset-md-2 text-right">
        <img src="<?= storeUrl()?>/assets/assets/images/secure/seal_secure_id.png" alt="..." class="img-thumbnail rounded">
    </div>
    <div class="col-md-4">
        <img src="<?= storeUrl()?>/assets/assets/images/secure/seal_satisfaction_id.png" alt="..." class="img-thumbnail rounded">
    </div>
</div>


<script>
$(document).ready(function(){
    let url = "<?= storeUrl().'katalog/getProdukById/'.$produkById?>";
    
    $.ajax({
        url: url,
        // data: {
        // tgl_awal: tgl_awal,
        // tgl_akhir: tgl_akhir,
        // },
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            console.log(data.id_produk)
            let limages = "";
            let thumbnail_produk = "";
            if(data.thumbnail_produk == ""){
                limages = `
                    <li data-target="#carousel${data.id_produk}" data-slide-to="0" class="active"></li>
                `;
                thumbnail_produk = `
                    <div class="carousel-item active">
                        <img src="<?= base_url(); ?>assets/assets/images/no-image.png" class="d-block w-100" alt="...">
                    </div>
                `;
            }else{
                let img_thumbnail_produk = JSON.parse(data.thumbnail_produk);
                $.each(img_thumbnail_produk, function( index, img ) {
                    if(index === 0){
                        limages += `
                            <li data-target="#carousel${data.id_produk}" data-slide-to="${index}" class="active"></li>
                        `;
                        thumbnail_produk += `
                            <div class="carousel-item active">
                                <img src="<?= base_url(); ?>assets/resize_images/${img}" class="d-block w-100" alt="...">
                            </div>
                        `;

                    }else{
                        limages += `
                            <li data-target="#carousel${data.id_produk}" data-slide-to="${index}"></li>
                        `;
                        thumbnail_produk += `
                            <div class="carousel-item">
                                <img src="<?= base_url(); ?>assets/resize_images/${img}" class="d-block w-100" alt="...">
                            </div>
                        `;
                    }
                });
            }

            harga =  new Intl.NumberFormat("id-ID", {
                style: "currency",
                currency: "IDR"
            }).format(data.harga);

            harga_coret =  new Intl.NumberFormat("id-ID", {
                style: "currency",
                currency: "IDR"
            }).format(data.harga_coret);
            let kodeUnik = Math.floor(100 + Math.random() * 900);
            let total_order = parseInt(data.harga) + kodeUnik;
            hargaTotal =  new Intl.NumberFormat("id-ID", {
                style: "currency",
                currency: "IDR"
            }).format(total_order);
            $('.carousel-indicators').html(limages);
            $('.carousel-inner').html(thumbnail_produk);
            $('.keterangan').html(data.keterangan_produk);
            $('.namaProduk').html(data.nama_produk);
            $('.hargaCoret').html(harga_coret);
            $('.harga').html(harga);
            $('.kodeUnik').html(kodeUnik);
            $('#kode_unik').prop('hidden', true);
            $('#kode_unik').val(kodeUnik);
            $('#total_order').prop('hidden', true);
            $('#total_order').val(total_order);
            $('#nama_produk').prop('hidden', true);
            $('#nama_produk').val(data.nama_produk);
            $('#id_produk').prop('hidden', true);
            $('#id_produk').val(data.id_produk);
            $('.totalHarga').html(hargaTotal);

            $('#no_whatsapp').prop('required', true);
            $('#name').prop('required', true);
            $('#email').prop('required', true);
            $('#kota').prop('required', true);
        }
    });

    $.ajax({
        url: "<?= storeUrl("katalog/getAllKota"); ?>",
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('#kota').html(data);
            $('#kota').select2({
                width: '100%'
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("Terjadi kesalahan server");
        }
    });

    // $(".btnBeli").on("click", function(){
    //     $('#checkoutForm').trigger('submit');
    //     console.log("ok");
    // });
});
</script>