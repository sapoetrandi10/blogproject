<style>
    .trackOrder{
        margin-top: 70px;
    }
    .card-body{
        min-height: 250px;        
    }
</style>
<div class="container trackOrder">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-body text-center">
                    <h4 class="card-title m-0">Track Order</h4>
                    <p class="mb-3">Silahkan masukan id order dan no handphone anda</p>
                    <!-- <div class="row text-center ">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata('notif')) { echo $this->session->flashdata('notif'); }?>
                        </div>
                    </div> -->
                    <form class="cmxform mx-5" id="trackingForm" method="post" action="<?= storeUrl("katalog/infoTracking"); ?>">
                    <fieldset>
                        <div class="form-group">
                            <!-- <label for="name">Id order</label> -->
                            <input id="id_order" class="form-control" name="id_order" type="number" placeholder="ID Order" required>
                        </div>
                        <div class="form-group">
                            <!-- <label for="name">No Handphone</label> -->
                            <input id="no_whatsapp" class="form-control" name="no_whatsapp" type="number" placeholder="No Handphone" required>
                        </div>
                        <input class="btn btn-outline-warning w-100" type="submit" value="Submit">
                    </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>