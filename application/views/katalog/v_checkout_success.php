<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    /* .body, .checkoutSuccess{
        background-color: #f8fff0;
    } */
    .btnCopy{
        width: 120px;
    }

    div.checkoutSuccess {
        width: 75%; 
        margin: auto;
    }

    .btnConfirm{
        width: 50%; 
        margin: auto;
    }

    @media (max-width: 768px) {
        div.checkoutSuccess {
            width: 90%; 
            margin: auto;
        }

        .btnConfirm{
            width: 100%; 
            margin: auto;
        }

        .btnCopy{
            width: 100%; 
            margin: auto;
        }
    }
</style>
<!-- number_format($jumlahTransfer) -->
<!-- <div class="row">
    <div class="col-md-12 grid-margin stretch-card"> -->
        <div class="checkoutSuccess text-center">
        <!-- <div class="card text-center checkoutSuccess">
            <div class="card-body" style="width: 75%; margin: auto;"> -->
                <img src="<?= storeUrl()?>/assets/assets/images/succes_page/icon_success.png" height="200px">
                <p>Untuk menyelesaikan proses order, silahkan transfer sejumlah</p>
                <h2><span class="nominalTF"></span>,-</h2>
                <span class="bg-warning rounded"><strong>PENTING !</strong> Mohon transfer ke salah satu rekening dibawah ini sesuai dengan nominal angka diatas!</span>

                <div class="card">
                    <div class="card-body text-left">
                        <div class="row mb-3">
                            <div class="col-md-12">
                                <h6><i class="mdi mdi-google-wallet"></i> Pilih Metode Pembayaran</h6>   
                                <!-- <i class="mdi mdi-google-wallet"></i>  -->
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-12">
                                <div class="bankTransfer">
                                    
                                </div>
                                <div class="paymentGateway">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h6>Klik tombol dibawah ini untuk mengkonfirmasi pembayaran!</h6>
                                <a href="<?= storeUrl("katalog/paymentConfirmation/?cp=$encodeIdOrder"); ?>">
                                    <button type="button" class="btn btn-warning  mt-2 btnConfirm">
                                        Konfirmasi Pembayaran
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                
            <!-- </div>
        </div> -->
    <!-- </div>
</div> -->

<script>
$(document).ready(function(){
    let idOrder = "<?= $idOrder ?>";
    $.ajax({
        url: "<?= storeUrl("katalog/getOrderById/"); ?>"+idOrder,
        type: "GET",
        dataType: "JSON",
        success: function(data) {

            //redirect ke hlaman track order jika status_pembayaran sudah di bayar
            if(data.order.status_pembayaran == 'sudah'){
                window.location.href = "<?= storeUrl('katalog/trackOrder') ?>";
                return false;
            }
                
            //redirect ke hlaman pembayaran ipaymu jika sudah pernah pilih metode pembayaran ipaymu
            if(data.request_payment !== undefined && data.request_payment !== null && data.order.status_pembayaran === "belum") {
                window.location.href = data.request_payment.url;
                return false;
            }
            
            total_order =  new Intl.NumberFormat("id-ID", {
                style: "currency",
                currency: "IDR"
            }).format(data.order.total_order).replace(/(\.|,)00$/g, '');
            $('.nominalTF').html(total_order);

            let bank = ``;
            if(data.order.available_payment.includes('bank')){
                    let logo = "";
                    let no_rekening = "";
                $.each(data.bank, function( index, value ) {
                    // console.log(value.no_rekening);
                    if(value.nama_bank == 'BCA'){
                        logo = "bca-logo";
                    } else if(value.nama_bank == 'BNI'){
                        logo = "bni-logo";
                    }
                    bank += `
                    <div class="row mb-2">
                        <div class="col-md-2 col-sm-12 mb-2 logoBank">
                            <img src="<?= storeUrl()?>/assets/assets/images/succes_page/${logo}.png" height="35px" width="70px">
                        </div>
                        <div class="col-md-6 col-sm-12 mb-2 namaRekTujuan">
                            <h6>Rekening ${value.nama_bank}</h6>
                            <p>Atas nama ${value.nama_rekening.toUpperCase()}</p>
                        </div>
                        <div class="col-md-4 col-sm-12 mb-2 text-right noRekTujuan">   
                            <p>No Rekening Tujuan : </p>
                            <button type="button" class="btn btn-outline-info btn-xs btnCopy" data-toggle="tooltip" data-placement="top" title="Copy to Clipboard"><i class="mdi mdi-content-copy"></i> ${value.no_rekening}</button> 
                        </div>
                    </div>
                    `;
                    no_rekening = value.no_rekening;
                });
                $('.bankTransfer').html(bank);

                $('button.btnCopy').on('click', function(){
                    let no_rekening = $.trim($(this)[0].innerText);
                    copyText(no_rekening);
                })
            }

            if(data.order.available_payment.includes('payment_gateway')){
                let payment_gateway = ``;
                if(data.gateway.layanan.includes('qris')){
                    payment_gateway += `
                    <div class="row mb-2">
                        <div class="col-md-2 col-sm-12 mb-2 logoBank">
                            <img src="<?= storeUrl()?>assets/assets/images/succes_page/qris-logo.png" height="40px" width="90px">
                        </div>
                        <div class="col-md-6 col-sm-12 mb-2 namaRekTujuan">
                            <h6>Pembayaran menggunakan Qris (Ovo, Dana, LinkAja, Gopay)</h6>
                            <p>(biaya 2% dari total transaksi)</p>
                        </div>
                        <div class="col-md-4 col-sm-12 mb-2 text-right noRekTujuan">   
                            <a href="<?= storeUrl("katalog/createPaymentIpaymu/?cp=$encodeIdOrder&pm=qris&pc=qris"); ?>">
                                <button type="button" class="btn btn-outline-info btn-xs btnCopy"><i class="mdi mdi-cash-usd"></i> BAYAR</button> 
                            </a>
                        </div>
                    </div>`;
                }
                if(data.gateway.layanan.includes('alfamart')){
                    payment_gateway += `
                    <div class="row mb-2">
                        <div class="col-md-2 col-sm-12 mb-2">
                            <img src="<?= storeUrl()?>assets/assets/images/succes_page/logo-alfamart.png" height="40px" width="90px">
                        </div>
                        <div class="col-md-6 col-sm-12 mb-2">
                            <h6>Pembayaran menggunakan Alfamart</h6>
                            <p>(biaya Rp. 5.000,-)</p>
                        </div>
                        <div class="col-md-4 col-sm-12 mb-2 text-right"> 
                            <a href="<?= storeUrl("katalog/createPaymentIpaymu/?cp=$encodeIdOrder&pm=cstore&pc=alfamart"); ?>">  
                                <button type="button" class="btn btn-outline-info btn-xs btnCopy"><i class="mdi mdi-cash-usd"></i> BAYAR</button> 
                            </a>
                        </div>
                    </div>`;
                }

                $('.paymentGateway').html(payment_gateway);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("Terjadi kesalahan server");
        }
    });
});

function copyText(params) {
    value = params;
    $(".btnCopy").attr('data-toggle', 'tooltip');
    $(".btnCopy").attr('data-placement', 'top');
    $(".btnCopy").attr('title', 'Copied');
    var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(value).select();
        document.execCommand("copy");
        $temp.remove();
}
</script>