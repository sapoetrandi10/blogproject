<?php

if ( ! function_exists('def_uri')) {
    function def_uri(){
        // return 'test.localhost';
        return 'orderpro.id';
    }
}

// if ( ! function_exists('storeUrl')) {
//     function storeUrl($path = null){
//         $ci =& get_instance();
//         // $urlProduct = 'http://localhost/'.$ci->session->userdata('domain_member').'.'.'blogProject/';
//         $storeUrl = "";
//         if($path != null){
//             $storeUrl = "http://order.blogProject.com/$path";
//         }else{
//             $storeUrl = "http://order.blogProject.com/";
//         }
//         return $storeUrl;
//     }
// }

if ( ! function_exists('storeUrl')) {
    function storeUrl($path = null){
        $ci =& get_instance();
        $whitelist = array(
            '127.0.0.1',
            '::1'
        );
        if (!in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
            // $urlProduct = 'http://localhost/'.$ci->session->userdata('domain_member').'.'.'blogProject/';
            $storeUrl = "";
            if($path != null){
                $storeUrl = "https://blog.dropshipaja.com/$path";
            }else{
                $storeUrl = "https://blog.dropshipaja.com/";
            }
        }else{
            $storeUrl = "";
            if($path != null){
                $storeUrl = "http://order.blogProject.com/$path";
            }else{
                $storeUrl = "http://order.blogProject.com/";
            }
            return $storeUrl;
        }
        return $storeUrl;
    }
}

if (!function_exists('upload')) {
    function upload($allowed_types, $upload_path, $resize_path, $field, $max_size = 5242880){
        $CI = &get_instance();
        $folderUpload = $upload_path;
        $resize_path = $resize_path;
        # periksa apakah folder tersedia
        if (!is_dir($folderUpload)) {
            # jika tidak maka folder harus dibuat terlebih dahulu
            mkdir($folderUpload, 0777, $rekursif = true);
        }
        if (!is_dir($resize_path)) {
            # jika tidak maka folder harus dibuat terlebih dahulu
            mkdir($resize_path, 0777, $rekursif = true);
        }

        $config['upload_path']= $upload_path;
        $config['allowed_types']= $allowed_types;
        $config['encrypt_name'] = TRUE;
        $config['max_size']     = $max_size;
        $CI->upload->initialize($config);
        if ($_FILES[$field]['size'] != 0) {
            if ( !$CI->upload->do_upload($field)){
                $CI->session->set_flashdata('error','Upload failed!');
                $data['error'] = $CI->upload->display_errors();
                $response = [
                    'status' => 'false',
                    'message' => $data['error']
                ];
                return $response;
            } else {
                $uploaded_data = $CI->upload->data();
                $thumbnail = "";
                $imgExt = array('.jpg', '.jpeg', '.png');
                if(in_array($uploaded_data['file_ext'], $imgExt)){
                    resizeImage($uploaded_data['file_name'], $upload_path, $resize_path);
                    $dataimage = $CI->upload->data();
                    $thumbnail = $dataimage['raw_name'].'_thumb'.$dataimage['file_ext'];
                }else{
                    // $thumbnail = $uploaded_data['raw_name'].'_thumb'.$uploaded_data['file_ext'];
                    $thumbnail = $uploaded_data['raw_name'].$uploaded_data['file_ext'];
                }
                
                // return $uploaded_data['file_name'];
                $response = [
                    'status' => 'true',
                    'thumbnail' => $thumbnail
                ];
                return $response;
            }
        }
    }
}

if (!function_exists('resizeImage')) {
    function resizeImage($filename, $upload_path, $resize_path) {
        $CI = &get_instance();
        $source_path = $upload_path ."/" .$filename;
        $target_path = $resize_path ."/";
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => TRUE,
            'create_thumb' => TRUE,
            'thumb_marker' => '_thumb',
            'quality' => '80%',
            'master_dim' => 'auto',
            'width' => '800',
            'height' => '800'
        );
        if (is_object(@$CI->image_lib) ? TRUE : FALSE) {
        $CI->image_lib->initialize($config_manip);      	
        if (!$CI->image_lib->resize()) {
            echo $CI->image_lib->display_errors();
        }
      } else {
        $CI->load->library('image_lib', $config_manip);
          if (!$CI->image_lib->resize()) {
              echo $CI->image_lib->display_errors();
          }
      }
        $CI->image_lib->clear();
        unlink($source_path);
    }
}

if (!function_exists('multipleUploadImage')) {
    function multipleUploadImage($namaField, $upload_path, $resize_path, $allowed_types, $max_size = 5242880) {
        $folderUpload = $upload_path;
        $resize_path = $resize_path;
        # periksa apakah folder tersedia
        if (!is_dir($folderUpload)) {
            # jika tidak maka folder harus dibuat terlebih dahulu
            mkdir($folderUpload, 0777, $rekursif = true);
        }
        if (!is_dir($resize_path)) {
            # jika tidak maka folder harus dibuat terlebih dahulu
            mkdir($resize_path, 0777, $rekursif = true);
        }

        $files = $_FILES;
        if(!empty($files)){
            
            $jumlahFile = count($files[$namaField]['name']);
            $namaFileUpload = [];
            if($files[$namaField]['error'][0] != 4){
                for ($i = 0; $i < $jumlahFile; $i++) {
                    $namaFile = $files[$namaField]['name'][$i];
                    $lokasiTmp = $files[$namaField]['tmp_name'][$i];
                    $errorFile = $files[$namaField]['error'][$i];
                    $size = $files[$namaField]['size'][$i];        
                    $ext = explode("/", $files[$namaField]['type'][$i]);
                    $ext = $ext[count($ext)-1];

                    if(!in_array($ext, $allowed_types)){
                        $response=[
                            'status' => 'false',
                            'message' => "Ekstensi gambar \"$namaFile\" tidak diizinkan silahkan pilih gambar lain!!"
                        ];
                        return $response;
                        break;
                    }
        
                    if($size > $max_size){
                        $response=[
                            'status' => 'false',
                            'message' => "Ukuran gambar \"$namaFile\" terlalu besar silahkan pilih gambar lain!!"
                        ];
                        return $response;
                        break;
                    }

                    if($errorFile != 0){
                        $response=[
                            'status' => 'false',
                            'message' => "Gambar \"$namaFile\" tidak valid silahkan pilih gambar lain!!"
                        ];
                        return $response;
                        break;
                    }

        
                    // count($names)-1
                    # kita tambahkan uniqid() agar nama gambar bersifat unik
                    $namaBaru = uniqid() . '.' . $ext;
        
                    $lokasiBaru = "{$folderUpload}/{$namaBaru}";
                    $prosesUpload = move_uploaded_file($lokasiTmp, $lokasiBaru);
                    resizeImage($namaBaru, $folderUpload, $resize_path);
                    $namaBaru = explode(".", $namaBaru);
                    $namaBaru = $namaBaru[0] ."_thumb.". $namaBaru[1];
                    array_push($namaFileUpload, $namaBaru);
                }
                $response = [
                    'status' => 'true',
                    'image' => $namaFileUpload
                ];
                return $response;
            }
        }else{
            $response=[
                'status' => 'false',
                'message' => 'gambar tidak valid. silahkan pilih gambar lain!'
            ];
            return $response;
        }
    }
}
