<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_cronjob extends CI_Model {
    public function getFollowUpByOrder(){
        $this->db->select('*');
        $this->db->from('tb_orders');
        $this->db->join('tb_produk', 'tb_produk.id_produk = tb_orders.id_produk');
        $this->db->join('tb_woowa_copywriting', 'tb_woowa_copywriting.id_produk = tb_produk.id_produk');
        $this->db->join('tb_woowa', 'tb_woowa.id = tb_woowa_copywriting.woowa_integration');
        // $this->db->where('tb_produk.id_administrator', $this->session->userdata('id_administrator'));
        $result = $this->db->get()->result_array();

        return $result;
    }

    public function updateFollowUpStatus($id_order, $data){

        $this->db->where('id_order', $id_order);
        $result = $this->db->update('tb_orders', $data);

        return $result;
    }
}