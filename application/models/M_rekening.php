<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_rekening extends CI_Model {
    
    function addRekening($data)
	{
		$result = $this->db->insert('tb_rekening', $data);

		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

    function editRekening($id_rekening, $data)
	{
        $this->db->where('id_rekening', $id_rekening);
        $result = $this->db->update('tb_rekening', $data);

        return $result;
		// if($this->db->affected_rows() > 0){
		// 	return true;
		// }else{
		// 	return false;
		// }
	}

    public function getAllRekeningJson(){
        $this->datatables->select('id_rekening, no_rekening, nama_rekening ,nama_bank, bank_id, moota_key');
        $this->datatables->from('tb_rekening');
        return $this->datatables->generate();
    }

    public function getAllRekening(){
        $this->db->select('id_rekening, no_rekening, nama_rekening ,nama_bank, bank_id, moota_key');
        $this->db->from('tb_rekening');
        $result = $this->db->get()->result_array();

        return $result;
    }

	public function getRekeningById($id_rekening){
        $this->db->select('id_rekening, no_rekening, nama_rekening, nama_bank, bank_id, moota_key');
        $this->db->from('tb_rekening');
        $this->db->where('id_rekening', $id_rekening);
        $result = $this->db->get()->row_array();
        
        return $result;
    }

    public function deleteRekening($id_rekening)
    {
        $this->db->where('id_rekening', $id_rekening);
        $result = $this->db->delete('tb_rekening');
        
        if($this->db->affected_rows() > 0){
            return true;
		}else{
            return false;
		}
    }

    public function isRekeningExist($id_rekening)
    {
        $this->db->select('no_rekening');
        $this->db->where('id_rekening', $id_rekening);
        $query = $this->db->get('tb_rekening')->row_array();

        return $query;
    }
}