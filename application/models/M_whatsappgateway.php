<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_whatsappgateway extends CI_Model {
    public function getAllWoowaAccountJson(){
        $this->datatables->select('id, phone_number, woowa_key, id_administrator');
        $this->datatables->from('tb_woowa');
        // $this->datatables->where('id_administrator', $id_administrator);
        return $this->datatables->generate();
    }

    public function deleteWoowaAccount($id)
	{
        $this->db->where('id', $id);
        $result = $this->db->delete('tb_woowa');
        
        if($this->db->affected_rows() > 0){
            return true;
		}else{
            return false;
		}
    }

    public function addWoowaAccount($data)
	{
        $result = $this->db->insert('tb_woowa', $data);
        
        if($this->db->affected_rows() > 0){
            // echo json_encode($result);
            return true;
		}else{
            return false;
		}
    }

    public function addCopywritingFuwa($data)
	{
        $result = $this->db->insert('tb_woowa_copywriting', $data);
        
        if($this->db->affected_rows() > 0){
            // echo json_encode($result);
            return true;
		}else{
            return false;
		}
    }

    public function editWoowaAccount($id, $data)
	{
        $this->db->where('id', $id);
        $result = $this->db->update('tb_woowa', $data);

        return $result;
    }
    
    public function editCopywritingFuwa($id_produk, $data)
	{
        $this->db->where('id_produk', $id_produk);
        $result = $this->db->update('tb_woowa_copywriting', $data);

        return $result;
    }
    
    public function getWoowaById($idWoowa)
	{
		$this->db->select('id, phone_number, woowa_key, id_administrator');
        $this->db->from('tb_woowa');
        $this->db->where('id', $idWoowa);
        $result = $this->db->get()->row_array();
        
        return $result;
	}

    public function getAllProduk()
	{
        $this->datatables->select('id_produk, nama_produk, status_produk, id_administrator');
        $this->datatables->from('tb_produk');
        $this->datatables->where('id_administrator', $this->session->userdata('id_administrator'));
        return $this->datatables->generate();
    }

    public function getWoowaAccount(){
        $this->db->select('id, phone_number, id_administrator');
        $this->db->from('tb_woowa');
        $this->db->where('id_administrator', $this->session->userdata('id_administrator'));
        $result = $this->db->get()->result_array();

        return $result;
    }

    public function getCopywritingFuwaByIdProduk($id_produk)
	{
		$this->db->select('*');
        $this->db->from('tb_woowa_copywriting');
        $this->db->where('id_produk', $id_produk);
        $result = $this->db->get()->row_array();
        
        return $result;
	}
}