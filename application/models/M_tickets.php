<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tickets extends CI_Model {

    public function adminCreateTicket($data){
        $result = $this->db->insert('tb_tickets', $data);

        if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
    }

    public function adminResolvedTicket($id_ticket){
        $this->db->set('status_ticket', 'resolved');
        $this->db->where('id_ticket', $id_ticket);
        $result = $this->db->update('tb_tickets');

        if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
    }

    public function getAllTicketJson(){
        $this->datatables->select('*');
        $this->datatables->from('tb_tickets');
        return $this->datatables->generate();
    }

    public function getAllMember(){
        $this->db->select('id_member, email_member, nama_member, status_member');
        $this->db->from('tb_member');
        $result = $this->db->get()->result_array();

        return $result;
    }

    public function getDetailTicketById($id_ticket){
        $this->db->select('*');
        $this->db->from('tb_tickets');
        // $this->db->join('tb_balasanticket', 'tb_balasanticket.id_ticket = tb_tickets.id_ticket');
        $this->db->join('tb_member', ' tb_member.email_member= tb_tickets.tujuan_pengirim');
        $this->db->where('tb_tickets.id_ticket', $id_ticket);
        $result = $this->db->get()->row_array();

        return $result;
    }

    // public function getDetailTicketById($id_ticket){
    //     $this->db->select('
    //     email_member, nama_member, 
    //     tb_tickets.id_ticket, tb_tickets.isi_ticket, tb_tickets.email_pengirim, tb_tickets.pengirim, tb_tickets.tujuan_pengirim, tb_tickets.subject, tb_tickets.status_ticket, tb_tickets.datetime_ticket, tb_tickets.attachment, 
    //     tb_balasanticket.pengirim, tb_balasanticket.email_pengirim, tb_balasanticket.tujuan_pengirim, tb_balasanticket.komentar, tb_balasanticket.tanggal_post, tb_balasanticket.status_baca, tb_balasanticket.attachment');
    //     $this->db->from('tb_tickets');
    //     $this->db->join('tb_balasanticket', 'tb_balasanticket.id_ticket = tb_tickets.id_ticket');
    //     $this->db->join('tb_member', ' tb_member.email_member= tb_tickets.tujuan_pengirim');
    //     $this->db->where('tb_tickets.id_ticket', $id_ticket);
    //     $result = $this->db->get()->row_array();

    //     return $result;
    // }

    public function getTicketBalasan($id_ticket){
        $this->db->select('*');
        $this->db->from('tb_balasanticket');
        // $this->db->join('tb_balasanticket', 'tb_balasanticket.id_ticket = tb_tickets.id_ticket');
        // $this->db->join('tb_member', ' tb_member.email_member= tb_tickets.tujuan_pengirim');
        $this->db->where('id_ticket', $id_ticket);
        $this->db->order_by('tanggal_post', 'ASC');
        $result = $this->db->get()->result_array();

        // return $result;

        $json = json_encode($result);

        echo $json;
    }

    public function postBalasanAdmin($data){
        $result = $this->db->insert('tb_balasanticket', $data);

        if($this->db->affected_rows() > 0){
            $response = array(
                'status' => 'success'
            );
            $json = json_encode($response);

            echo $json;
		}else{
			$response = array(
                'status' => 'failed'
            );
            $json = json_encode($response);

            echo $json;
		}
    }

    public function updateStatusBacaAdmin($id_ticket){
		
        $this->db->set('status_baca', 'sudah_dibaca');
        $this->db->where('id_ticket', $id_ticket);
        $this->db->where('pengirim', 'customer');
        $result = $this->db->update('tb_balasanticket');
    
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

}