<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_katalog extends CI_Model {
    public function getAllKota(){
        $this->db->select('id_kecamatan , kode_kota, provinsi, kabupaten_kota, nama_kabupaten_kota, kecamatan, kodepos, zona');
        $this->db->from('tb_jne');
        $this->db->group_by("nama_kabupaten_kota");
        $result = $this->db->get()->result_array();

        return $result;
    }
    
    public function getOrderById($id_order){
        $this->db->select('id_order, nama_pemesan, no_whatsapp, email, kota, datetime_order, datetime_expired, tgl_transfer, datetime_konfirmasi_pembayaran, total_order, kode_unik, status_pembayaran, diskon, bukti_transfer, no_rek, no_rek, atas_nama, dari_bank, ke_bank, tb_orders.nama_produk, tb_orders.id_produk, status_cancel, keterangan_produk, tb_payment_gateway.va, tb_payment_gateway.api_key');
        $this->db->from('tb_orders');
        $this->db->join('tb_produk', 'tb_produk.id_produk = tb_orders.id_produk');
        $this->db->join('tb_payment_gateway', 'tb_payment_gateway.id = tb_produk.select_payment_gateway');
        $this->db->where('tb_orders.id_order', $id_order);
        // $result = $this->db->get()->result_array();
        $result = $this->db->get()->row_array();
    
        return $result;
    }

    public function getTrackingOrder($id_order, $no_whatsapp){
        // $this->db->select('id_order, nama_pemesan, no_whatsapp, email, kota, datetime_order, datetime_expired, tgl_transfer, datetime_konfirmasi_pembayaran, total_order, kode_unik, status_pembayaran, diskon, bukti_transfer, no_rek, no_rek, atas_nama, dari_bank, ke_bank, tb_orders.nama_produk, tb_orders.id_produk, status_cancel, keterangan_produk');
        $this->db->select('id_order, nama_pemesan, no_whatsapp, email, kota, datetime_order, datetime_expired, tgl_transfer, datetime_konfirmasi_pembayaran, total_order, kode_unik, status_pembayaran, status_order, diskon, bukti_transfer, no_rek, atas_nama, dari_bank, ke_bank, tb_orders.nama_produk, tb_orders.id_produk, status_cancel');
        $this->db->from('tb_orders');
        $this->db->join('tb_produk', 'tb_produk.id_produk = tb_orders.id_produk');
        $this->db->where('tb_orders.id_order', $id_order);
        $this->db->where('tb_orders.no_whatsapp', $no_whatsapp);
        $result = $this->db->get()->row_array();
    
        return $result;
    }
}