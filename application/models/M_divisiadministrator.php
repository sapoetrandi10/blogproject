<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_divisiadministrator extends CI_Model {
    
    function addDivisiAdmin($data)
	{
		$result = $this->db->insert('tb_divisi_administrator', $data);

		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

    public function getAllDivisiAdminJson(){
        $this->datatables->select('id_divisi, nama_divisi, deskripsi, id_menu');
        $this->datatables->from('tb_divisi_administrator');
        return $this->datatables->generate();
    }

    
    public function getDivisiById($id_divisi){
        $this->db->select('id_divisi, nama_divisi, deskripsi, id_menu');
        $this->db->from('tb_divisi_administrator');
        $this->db->where('id_divisi', $id_divisi);
        $result = $this->db->get()->row_array();
        
        return $result;
    }

    public function editDivisiAdmin($id_divisi, $data){
        $this->db->where('id_divisi', $id_divisi);
        $result = $this->db->update('tb_divisi_administrator', $data);

        return $result;
    }

    public function deleteDivisiAdmin($id_divisi)
    {
        $this->db->where('id_divisi', $id_divisi);
        $result = $this->db->delete('tb_divisi_administrator');
        return $result;
    }

    public function getAllDivisiAdmin(){
        $this->db->select('id_divisi, nama_divisi, deskripsi, id_menu');
        $this->db->from('tb_divisi_administrator');
        $result = $this->db->get()->result_array();

        return $result;
    }
}