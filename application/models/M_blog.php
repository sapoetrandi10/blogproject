<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_blog extends CI_Model {

    public function getAllBlog(){
        $blog_terhapus = 'no';
        $this->db->select('id_blog, judul_blog, status_blog, tb_kategori_blog.id, nama_kategori');
        $this->db->from('tb_blog');
        $this->db->join('tb_kategori_blog', 'tb_kategori_blog.id=tb_blog.id_kategori');
        $this->db->where('blog_terhapus', $blog_terhapus);
        $result = $this->db->get()->result_array();

        return $result;
    }

    public function getBlogById($id_blog){
        $this->db->select('*');
        $this->db->from('tb_blog');
        $this->db->join('tb_kategori_blog', 'tb_kategori_blog.id=tb_blog.id_kategori');
        $this->db->where('id_blog', $id_blog);
        $result = $this->db->get()->row_array();

        return $result;
    }

    public function addBlog($data){
        $result = $this->db->insert('tb_blog', $data);
        return $result;
    }

    public function editBlog($id_blog, $data){;
        $this->db->where('id_blog', $id_blog);
        $result = $this->db->update('tb_blog', $data);

        return $result;
    }

    public function deleteBlog($id_blog)
	{
        $blog_terhapus = 'yes';
        $this->db->set('blog_terhapus', $blog_terhapus  ); 
        $this->db->where('id_blog', $id_blog);
        // $result = $this->db->delete('tb_blog');
        $result = $this->db->update('tb_blog');
        return $result;
    }

    public function getAllBlogJson(){
        $blog_terhapus = 'no';
        $this->datatables->select('id_blog, judul_blog, status_blog, tb_kategori_blog.id AS id_kat, nama_kategori');
        $this->datatables->from('tb_blog');
        $this->datatables->join('tb_kategori_blog', 'tb_kategori_blog.id=tb_blog.id_kategori');
        $this->datatables->where('blog_terhapus', $blog_terhapus);
        return $this->datatables->generate();
    }
}