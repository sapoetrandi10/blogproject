<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kategori extends CI_Model {

    public function getAllKategori(){
        $this->db->select('id, nama_kategori, thumbnail_kategori, status_kategori');
        $this->db->from('tb_kategori_blog');
        $result = $this->db->get()->result_array();

        return $result;
    }

    public function getKategoriById($id_kategori){
        $this->db->select('id, nama_kategori, thumbnail_kategori, status_kategori');
        $this->db->from('tb_kategori_blog');
        $this->db->where('id', $id_kategori);
        $result = $this->db->get()->row_array();

        return $result;
    }

    public function addKategori($data){
        $result = $this->db->insert('tb_kategori_blog', $data);
        return $result;
    }

    public function editKategori($id_kategori, $data){
        $this->db->where('id', $id_kategori);
        $result = $this->db->update('tb_kategori_blog', $data);

        return $result;
    }

    public function deleteKategori($id_kategori)
	{
        $this->db->where('id', $id_kategori);
        $result = $this->db->delete('tb_kategori_blog');
        return $result;
    }

    public function getAllKategoriJson(){
        $this->datatables->select('id, nama_kategori, thumbnail_kategori, status_kategori');
        // $this->datatables->select('nama_kategori, status_kategori');
        $this->datatables->from('tb_kategori_blog');
        return $this->datatables->generate();
    }
}