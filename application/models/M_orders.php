<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_orders extends CI_Model {
    public function getAllOrderJson(){
        $this->datatables->select('id_order, datetime_order, nama_produk, status_cancel, status_order, status_pembayaran, total_order, bukti_transfer');
        $this->datatables->from('tb_orders');
        $this->datatables->where('status_order', 'pending');
        return $this->datatables->generate();
    }

    public function getProgressOrderJson(){
        $this->datatables->select('id_order, datetime_order, nama_produk, status_cancel, status_order, status_pembayaran, total_order, bukti_transfer');
        $this->datatables->from('tb_orders');
        $this->datatables->where('status_order !=', 'pending');
        return $this->datatables->generate();
    }

    public function updateStatusOrder($status_order, $id_order){
        $data = array();

        if($status_order == "terima"){
            $data = array(
                'status_pembayaran' => 'sudah',
                'status_order' => 'process',
            );
        }else if($status_order == "reset"){
            $data = array(
                'bukti_transfer' => "",
                'no_rek' => "",
                'atas_nama' => "",
                'dari_bank' => "",
                'ke_bank' => "",
                'status_pembayaran' => "belum",
                'status_cancel' => "yes",
            );
        }else if($status_order == "pending"){
            $data = array(
                'status_order' => 'pending'
            );

        }else if($status_order == "process"){
            $data = array(
                'status_order' => 'process'
            );

        }else if($status_order == "complete"){
            $data = array(
                'status_order' => 'complete'
            );

        }else if($status_order == "refund"){
            $data = array(
                'status_order' => 'refund'
            );

        }else if($status_order == "cancel"){
            $data = array(
                'status_order' => 'cancel',
                'status_cancel' => "yes"
            );

        }
        
        $this->db->where('id_order', $id_order);
        $result = $this->db->update('tb_orders', $data);

        if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
    }

    public function setPaymentSuccess($id_order, $data){

        $this->db->where('id_order', $id_order);
        $result = $this->db->update('tb_orders', $data);

        if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
    }

    public function updateLogRequest($sid, $data){

        $this->db->where('session_id', $sid);
        $result = $this->db->update('log_request', $data);

        if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
    }
    
    public function getOrderById($id_order){
        // $this->db->select('id_produk , nama_produk, kategori, harga, harga_coret, thumbnail_produk, url, keterangan_produk, text_button_beli, subtext_button_beli, warna_button_beli, rekening_pembayaran, bukti_pembayaran, text_konfirmasi_pembayaran, id_rekening, no_rekening, nama_rekening, nama_bank, bank_id, moota_key');
        $result = array();
        $this->db->select('*');
        $this->db->from('tb_orders');
        $this->db->join('tb_produk', '.tb_produk.id_produk = tb_orders.id_produk');
        // $this->db->join('tb_payment_gateway', 'tb_payment_gateway.id = tb_produk.select_payment_gateway');
        $this->db->where('id_order', $id_order);
        $order = $this->db->get()->row_array();

        
        

        $rekening_pembayaran = json_decode($order['rekening_pembayaran']);
        
        $this->db->select('*');
        $this->db->from('tb_rekening');
        $this->db->where_in('no_rekening', $rekening_pembayaran);
        $bank = $this->db->get()->result_array();
        
        
        $this->db->select('*');
        $this->db->from('tb_payment_gateway');
        if($order['select_payment_gateway'] == 'default'){
            $this->db->where('status_level', 'primary');
        }else{
            $this->db->where('id', $order['select_payment_gateway']);
            // $this->db->where('status_level', 'optional');
        }
        $gateway = $this->db->get()->row_array();

        $response = array();
        if($order){
            $result['order'] = $order;
        }else{
            $err = [
                "status" => "order not found!"
            ];
            $response = json_encode($err);
        }

        if($bank){
            $result['bank'] = $bank;
        }else{
            $err = [
                "status" => "bank not found!"
            ];
            $response = json_encode($err);
        }

        if($gateway){
            $result['gateway'] = $gateway;
        }else{
            $err = [
                "status" => "payment gateway not found!"
            ];
            $response = json_encode($err);
        }

        $this->db->select('id, id_order, session_id, url');
        $this->db->from('log_request');
        $this->db->where_in('id_order', $id_order);
        $request_payment = $this->db->get()->row_array();

        if($request_payment){
            $result['request_payment'] = $request_payment;
        }
        // else{
        //     $err = [
        //         "status" => "payment gateway not found!"
        //     ];
        //     $response = json_encode($err);
        // }

        if($order && $bank && $gateway){
            $result = json_encode($result);
			return $result;
		}else{
			return $response;
		}
    }

    public function createOrder($data){

        $result = $this->db->insert('tb_orders', $data);
    
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function confirmPayment($id_order, $data){
        
        $this->db->where('id_order', $id_order);
        $result = $this->db->update('tb_orders', $data);
    
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }
}