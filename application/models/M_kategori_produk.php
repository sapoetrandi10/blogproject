<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kategori_produk extends CI_Model {

    public function getAllKategoriproduk(){
        $this->db->select('id_kategori_produk, nama_kategori_produk, thumbnail_kategori');
        $this->db->from('tb_kategori_produk');
        $result = $this->db->get()->result_array();

        return $result;
    }

    public function getKategoriprodukById($id_kategori){
        $this->db->select('id_kategori_produk, nama_kategori_produk, thumbnail_kategori');
        $this->db->from('tb_kategori_produk');
        $this->db->where('id_kategori_produk', $id_kategori);
        $result = $this->db->get()->row_array();

        return $result;
    }

    public function addKategoriproduk($data){
        $result = $this->db->insert('tb_kategori_produk', $data);
        return $result;
    }

    public function editKategoriproduk($id_kategori, $data){
        $this->db->where('id_kategori_produk', $id_kategori);
        $result = $this->db->update('tb_kategori_produk', $data);

        return $result;
    }

    public function deleteKategoriproduk($id_kategori)
	{
        $this->db->where('id_kategori_produk', $id_kategori);
        $result = $this->db->delete('tb_kategori_produk');
        return $result;
    }

    public function getAllKategoriprodukJson(){
        $this->datatables->select('id_kategori_produk, nama_kategori_produk, thumbnail_kategori');
        $this->datatables->from('tb_kategori_produk');
        return $this->datatables->generate();
    }
}