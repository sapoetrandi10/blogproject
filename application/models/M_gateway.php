<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_gateway extends CI_Model {
    public function addPaymentGateway($data){
        $result = $this->db->insert('tb_payment_gateway', $data);
        
		if($this->db->affected_rows() > 0){
            return true;
		}else{
            return false;
		}
    }

    public function editPaymentGateway($id, $data){
        $this->db->where('id', $id);
        $this->db->where('id_administrator', $this->session->userdata('id_administrator'));
        $result = $this->db->update('tb_payment_gateway', $data);
        
        return $result;
    }

    public function deletePaymentGateway($id)
	{
        $this->db->where('id', $id);
        $result = $this->db->delete('tb_payment_gateway');
        
        if($this->db->affected_rows() > 0){
            return true;
		}else{
            return false;
		}
    }

    public function getAllPaymentGatewayJson(){
        $this->datatables->select('id, nama, api_key, status, status_level, layanan, payment_gateway');
        $this->datatables->from('tb_payment_gateway');
        return $this->datatables->generate();
    }

    public function getAllPaymentGateway(){
        $this->db->select('id, nama, api_key, status, status_level, layanan, payment_gateway');
        $this->db->from('tb_payment_gateway');
        return $this->db->get()->result_array();
    }
    
    public function getPaymentGatewayById($id_gateway){
        $this->db->select('id, nama, va, api_key, status, status_level, layanan, payment_gateway');
        $this->db->from('tb_payment_gateway');
        $this->db->where('id', $id_gateway);
        $result = $this->db->get()->row_array();
        
        return $result;
    }

    public function insertRequestIpaymu($data){
        $result = $this->db->insert('log_request', $data);
        
		if($this->db->affected_rows() > 0){
            return true;
		}else{
            return false;
		}
    }
    public function insertLogIpaymu($data){
        $result = $this->db->insert('log_ipaymu', $data);
        
		if($this->db->affected_rows() > 0){
            return true;
		}else{
            return false;
		}
    }
}