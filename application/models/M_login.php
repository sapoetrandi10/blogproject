<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model {

    function checkLogin($email, $password)
	{
		// $passwordHash = md5(md5($password));

		$this->db->select('*');
		$this->db->from('tb_administrator');
		$this->db->where('email_administrator', $email);
		$this->db->where('password_administrator', $password);
		// $this->db->where('status_member', 'AKTIF');
		$query = $this->db->get()->row_array();

		if ($query) {
            return $query;
        } else {
			return FALSE;
		}
	}

    public function lastlogin($email_administrator, $currentDateTime){
        $this->db->set('last_login', $currentDateTime);
        $this->db->where('email_administrator', $email_administrator);
        return $this->db->update('tb_administrator');
    }

	function register($data)
	{
		$result = $this->db->insert('tb_administrator', $data);

		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

}