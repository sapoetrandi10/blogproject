<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_administrator extends CI_Model {
    
    function addAdmin($data)
	{
		$result = $this->db->insert('tb_administrator', $data);

		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

    public function getAdminById($id_administrator){
        $this->db->select('id_administrator, email_administrator, nama_administrator, telp_administrator, status_administrator, role_administrator');
        $this->db->from('tb_administrator');
        $this->db->where('id_administrator', $id_administrator);
        $result = $this->db->get()->row_array();

        return $result;
    }

    public function editAdmin($id_administrator, $data){;
        $this->db->where('id_administrator', $id_administrator);
        $result = $this->db->update('tb_administrator', $data);

        return $result;
    }

    public function getAllAdminJson(){
        $this->datatables->select('id_administrator, nama_administrator, email_administrator, telp_administrator, datetime_join, status_administrator, role_administrator');
        $this->datatables->from('tb_administrator');
        return $this->datatables->generate();
    }

    public function deleteAdmin($id_administrator)
	{
        $this->db->where('id_administrator', $id_administrator);
        $result = $this->db->delete('tb_administrator');
        return $result;
    }

    public function isEmailExist($id_administrator)
    {
        $this->db->select('email_administrator');
        $this->db->where('id_administrator', $id_administrator);
        $query = $this->db->get('tb_administrator')->row_array();

        return $query;
    }
}
