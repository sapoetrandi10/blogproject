<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_studycaseapi extends CI_Model {
    public function getBarang()
	{
		$this->db->select('nama_kategori, SUM(stock) AS stock_per_kategori');
        $this->db->from('tb_barang');
        $this->db->join('tb_kategoribarang', 'tb_kategoribarang.id_kategori=tb_barang.kategori_barang');
        // $this->db->where('id_blog', $id_blog);
        $this->db->group_by('kategori_barang');
        $this->db->limit(50);
        $result = $this->db->get()->result_array();

        $json = json_encode($result);

        echo $json;
	}
}