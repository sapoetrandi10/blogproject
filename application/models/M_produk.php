<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_produk extends CI_Model {
    function addProduk($data)
	{
		$result = $this->db->insert('tb_produk', $data);

		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

    public function getAllProduk(){
        $this->db->select('id_produk , nama_produk, kategori, harga, harga_coret, thumbnail_produk, url, keterangan_produk, text_button_beli, subtext_button_beli, warna_button_beli, rekening_pembayaran, bukti_pembayaran, text_konfirmasi_pembayaran');
        $this->db->from('tb_produk');
        if($this->session->userdata('id_administrator') !== null){
            $this->db->where('id_administrator', $this->session->userdata('id_administrator'));
        }
        $result = $this->db->get()->result_array();

        return $result;
    }
    
    public function getProdukByKeyword($keyword){
        $this->db->select('id_produk , nama_produk, kategori, harga, harga_coret, thumbnail_produk, url, keterangan_produk, text_button_beli, subtext_button_beli, warna_button_beli, rekening_pembayaran, bukti_pembayaran, text_konfirmasi_pembayaran');
        $this->db->from('tb_produk');
        $this->db->or_like('nama_produk', $keyword);
        $this->db->or_like('kategori', $keyword);
        $this->db->or_like('harga', $keyword);
        $this->db->or_like('keterangan_produk', $keyword);
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function getProdukById($id_produk){
        $this->db->select('id_produk , nama_produk, kategori, harga, harga_coret, thumbnail_produk, url, keterangan_produk, text_button_beli, subtext_button_beli, warna_button_beli, rekening_pembayaran, select_payment_gateway, bukti_pembayaran, available_payment, text_konfirmasi_pembayaran');
        $this->db->from('tb_produk');
        $this->db->where('id_produk', $id_produk);
        $result = $this->db->get()->row_array();

        return $result;
    }

    public function editProduk($id_produk, $data)
	{
        $this->db->where('id_produk', $id_produk);
        $result = $this->db->update('tb_produk', $data);

        return $result;
    }

    public function deleteProduk($id_produk)
	{
        $this->db->where('id_produk', $id_produk);
        $result = $this->db->delete('tb_produk');

        if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
    }

    public function searchProduk($keyword)
	{
        $this->db->select('id_produk , nama_produk, kategori, harga, harga_coret, thumbnail_produk, url, keterangan_produk, text_button_beli, subtext_button_beli, warna_button_beli, rekening_pembayaran, bukti_pembayaran, text_konfirmasi_pembayaran');
        $this->db->from('tb_produk');
        $this->db->or_like('id_produk', $keyword);
        $this->db->or_like('kategori', $keyword);
        $this->db->or_like('harga', $keyword);
        $this->db->or_like('url', $keyword);
        $this->db->or_like('keterangan_produk', $keyword);
        $this->db->or_like('rekening_pembayaran', $keyword);
        $this->db->or_like('bukti_pembayaran', $keyword);
        $result = $this->db->get()->result_array();

        return $result;
	}
}